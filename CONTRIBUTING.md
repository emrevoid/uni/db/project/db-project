# Contributing

## Branching model

[The branching model by Bitbucket](https://confluence.atlassian.com/bitbucket/branching-a-repository-223217999.html#BranchingaRepository-branching_modelThebranchingmodel)

## Workflow

Project management is done using issues on GitLab boards.
