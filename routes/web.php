<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Event;
use Illuminate\Support\Facades\Auth;

Route::get('/', 'EventController@index')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/administrators/panel', 'AdministratorController@panel');

Route::resources([
    'macrocategories' => 'MacrocategoryController',
    'categories' => 'CategoryController',
    'customers' => 'CustomerController',
    'organizers' => 'OrganizerController',
    'administrators' => 'AdministratorController',
    'events' => 'EventController',
    'tickets' => 'TicketController',
    'places' => 'PlaceController',
    'guests' => 'GuestController',
    'partecipates' => 'PartecipateController',
    'sponsors' => 'SponsorController',
    'sponsor_ranges' => 'SponsorRangeController',
    'funds' => 'FundController',
    'ticket_rate' => 'TicketRateController',
    'seat_types' => 'SeatTypeController',
    'seats' => 'SeatController',
    'statistics' => 'StatisticController',
]);

Route::get('/search', 'EventController@search');

Route::get('/cart/{id}/add', 'CartController@add_ticket')->name('add_to_cart');

Route::get('/cart/pay', 'CartController@pay')->name('cart.pay');

Route::resource('cart', 'CartController')->only('index', 'update', 'destroy');

Route::resource('orders', 'OrderController')->except([
    'edit', 'update', 'create', 'store'
]);

Route::get('/notifications', 'NotificationController@notifications');

Route::get('/favorites/{id}/remove', 'FavoriteController@remove')->name('remove_favorite');

Route::get('/favorites/{id}/add', 'FavoriteController@add')->name('add_favorite');

Route::resource('favorites', 'FavoriteController')->only(['store', 'destroy']);

Route::get('/myevents', 'MyEventsController@index');

Route::name('statistics.')->group(static function () {
    Route::get('category-age-average', 'StatisticController@categoryAgeAverage')->name('category-age-average');
    Route::get('category-ranking', 'StatisticController@categoryRanking')->name('category-ranking');
    Route::get('customer-spending-average', 'StatisticController@customerSpendingAverage')->name('customer-spending-average');
    Route::get('event-profit', 'StatisticController@eventProfit')->name('event-profit');
    Route::get('place-age-average', 'StatisticController@placeAgeAverage')->name('place-age-average');
    Route::get('place-profit', 'StatisticController@placeProfit')->name('place-profit');
    Route::get('sponsor-ranking', 'StatisticController@sponsorRanking')->name('sponsor-ranking');
    Route::get('ticket-sold-guest', 'StatisticController@ticketSoldGuest')->name('ticket-sold-guest');
});
