<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('quantity');
            $table->unsignedDecimal('unit_price', 8, 2);
            $table->unsignedBigInteger('event_id')->nullable();
            $table->unsignedBigInteger('seat_type_id')->nullable();
            $table->timestamps();

            $table->foreign('event_id')
                    ->references('id')
                    ->on('events');
            $table->foreign('seat_type_id')
                    ->references('id')
                    ->on('seat_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seats');
    }
}
