<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedDecimal('amount');
            $table->unsignedBigInteger('sponsor_id')->nullable();
            $table->unsignedBigInteger('event_id')->nullable();
            $table->unsignedBigInteger('sponsor_range_id')->nullable();
            $table->timestamps();

            $table->unique(['event_id', 'sponsor_id']);
            $table->foreign('event_id')->references('id')->on('events');
            $table->foreign('sponsor_id')->references('id')->on('sponsors');
            $table->foreign('sponsor_range_id')->references('id')->on('sponsor_ranges');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funds');
    }
}
