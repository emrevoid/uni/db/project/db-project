<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['customer_id', 'paid', 'last_cart_update'];

    public function user()
    {
        return $this->belongsTo("App\User", "customer_id", "id");
    }

    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }
}
