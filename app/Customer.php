<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Customer
 * @package App
 */
class Customer extends User
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'sesso', 'dataNascita', 'residenza', 'user_id'];

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->morphOne('App\User', 'userable');
    }
}
