<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SponsorRange extends Model
{
    protected $fillable = ['name', 'amount'];
}
