<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Partecipate
 * @package App
 */
class Partecipate extends Model
{
    protected $fillable = ['event_id', 'guest_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo('App\Event')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function guest()
    {
        return $this->belongsTo('App\Guest');
    }
}
