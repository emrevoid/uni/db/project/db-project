<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'macrocategory_id'];

    public function macrocategory()
    {
        return $this->belongsTo('App\Macrocategory');
    }
}
