<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

/**
 * Class User
 * @package App
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname','email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return mixed
     */
    public function userable()
    {
        return $this->morphTo();
    }

    /**
     * Finds all orders a user has done.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany Order::class
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * Return customers' favorites events.
     */
    public function favorites()
    {
        if(Gate::allows('isCustomer')) {
            return $this->hasMany(\App\Favorite::class, 'customer_id');
        }
    }
}
