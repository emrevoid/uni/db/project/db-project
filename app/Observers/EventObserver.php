<?php

namespace App\Observers;

use App\Event;
use App\Notifications\EventUpdated;
use App\Notifications\SoldOut;
use Ds\Set;
use Illuminate\Support\Facades\Notification;

class EventObserver
{
    /**
     * Handle the event "created" event.
     *
     * @param  \App\Event  $event
     * @return void
     */
    public function created(Event $event)
    {
        //
    }

    /**
     * Handle the event "updated" event.
     *
     * @param  \App\Event  $event
     * @return void
     */
    public function updated(Event $event)
    {
        $event_fields = ['name', 'description', 'guest', 'place', 'price',
                         'start_date', 'end_date', 'start_time', 'end_time'];
        if ($event->wasChanged($event_fields)) {
            Notification::send($event->paid_orders_users, new EventUpdated($event));
            Notification::send($event->favorites_users, new EventUpdated($event));
        }
    }

    /**
     * Handle the event "deleted" event.
     *
     * @param  \App\Event  $event
     * @return void
     */
    public function deleted(Event $event)
    {
        //
    }

    /**
     * Handle the event "restored" event.
     *
     * @param  \App\Event  $event
     * @return void
     */
    public function restored(Event $event)
    {
        //
    }

    /**
     * Handle the event "force deleted" event.
     *
     * @param  \App\Event  $event
     * @return void
     */
    public function forceDeleted(Event $event)
    {
        //
    }
}
