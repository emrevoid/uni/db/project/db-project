<?php

namespace App\Observers;

use App\Notifications\OrderCompleted;
use App\Order;
use App\Notifications\SoldOut;
use Illuminate\Support\Facades\Notification;

class OrderObserver
{
    /**
     * Handle the order "updated" event.
     *
     * @param Order $order
     * @return void
     */
    public function updated(Order $order)
    {
        if ($order->wasChanged('paid')) {
            Notification::send($order->user, new OrderCompleted($order));

            $tickets = $order->tickets;
            foreach ($tickets as $ticket) {
                $event = $ticket->event;
                if ($event->effectively_sold_ticket_number >= $event->n_ticket) {
                    Notification::send($event->paid_orders_users, new SoldOut($event));
                    Notification::send($event->favorites_users, new SoldOut($event));
                    Notification::send($event->organizer, new SoldOut($event));
                }
            }
        }
    }
}
