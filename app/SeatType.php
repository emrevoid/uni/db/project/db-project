<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SeatType extends Model
{
    use SoftDeletes;

    protected $fillable = ['name'];

    public function seats()
    {
        return $this->hasMany(\App\Seat::class);
    }

    public function tickets()
    {
        return $this->hasMany(\App\Ticket::class);
    }
}
