<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Sitemap\SitemapGenerator;
use Illuminate\Support\Facades\Storage;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the website sitemap.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $folder = 'public/sitemap';
        Storage::disk('local')->makeDirectory($folder);
        $sitemap_path = base_path('storage/app/' . $folder) .  '/sitemap.xml';
        SitemapGenerator::create('https://emre-db.herokuapp.com')
            ->writeToFile($sitemap_path);
    }
}
