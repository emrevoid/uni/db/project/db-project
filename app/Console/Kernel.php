<?php

namespace App\Console;

use App\Http\Controllers\OrderController;
use App\Order;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $time_delta = \Carbon\Carbon::now()->subMinutes(30);

            $cart_items_not_paid = Cart::select('cart.*')
                                        ->join('tickets', 'cart.ticket_id', '=', 'tickets.id')
                                        ->whereNull('tickets.order_id')
                                        ->where('tickets.created_at', '<', $time_delta->format('Y-m-d H:i:s'))
                                        ->get();

            foreach ($cart_items_not_paid as $cart_item_not_paid) {
                $event = $cart_item_not_paid->ticket->event;
                ++$event->n_ticket_aval;
                $event->save();
                $ticket = $cart_item_not_paid->ticket;
                $cart_item_not_paid->delete();
                $ticket->delete();
            }
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
