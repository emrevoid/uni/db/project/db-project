<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Cart extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['customer_id', 'ticket_id'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cart';

    public function user()
    {
        return $this->belongsTo(\App\User::class, "customer_id");
    }

    /**
     * Return tickets added to cart for the current user
     * @return array
     */
    public static function getCartItems()
    {
        $current_user = User::findOrFail(Auth::id());
        $userCart = Cart::where('customer_id', $current_user->id)->get();
        $tickets = [];

        if (isset($userCart)) {
            foreach ($userCart as $itemInCart) {
                $tickets[] = Ticket::where('id', $itemInCart->ticket->id)->first();
            }
        }

        return $tickets;
    }

    public function ticket()
    {
        return $this->belongsTo(\App\Ticket::class);
    }
}
