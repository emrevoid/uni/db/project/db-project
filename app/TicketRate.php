<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketRate extends Model
{
    protected $fillable = ['name', 'amount'];
}
