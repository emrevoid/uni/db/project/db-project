<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    protected $fillable = ['name', 'description'];

    public function funds()
    {
        return $this->hasMany(\App\Fund::class);
    }
}
