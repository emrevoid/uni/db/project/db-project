<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Organizer
 * @package App
 */
class Organizer extends User
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'ragSociale', 'descrizione', 'numTelefono' ];

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->morphOne('App\User', 'userable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany(\App\Event::class);
    }
}
