<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Administrator
 * @package App
 */
class Administrator extends User
{

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->morphOne('App\User', 'userable');
    }
}
