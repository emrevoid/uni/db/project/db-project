<?php

namespace App\Providers;

use App\Event;
use App\Observers\EventObserver;
use App\Observers\OrderObserver;
use App\Order;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        if ($this->app->environment() === 'production') {
            \URL::forceScheme('https');
        }
        Event::observe(EventObserver::class);
        Order::observe(OrderObserver::class);
    }
}
