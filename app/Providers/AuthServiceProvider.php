<?php

namespace App\Providers;

use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isCustomer', function ($user) {
            return $user->userable_type === "App\Customer";
        });

        Gate::define('isOrganizer', function ($user) {
            return $user->userable_type === "App\Organizer";
        });

        Gate::define('isAdmin', function ($user) {
            return $user->userable_type === "App\Administrator";
        });

        Gate::define('isCustomerOwner', function ($user, $current) {
            return $user->id === $current->id;
        });

        Gate::define('isOrganizerOwner', function ($user, $current) {
            return $user->id === $current->id;
        });

        Gate::define('isAdminOwner', function ($user, $current) {
            return $user->id === $current->id;
        });
    }
}
