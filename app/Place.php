<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address','phoneNum', 'description',
    ];

    public function event()
    {
        return $this->hasMany('App\Event');
    }

}
