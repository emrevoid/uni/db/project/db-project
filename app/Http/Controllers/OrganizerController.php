<?php

namespace App\Http\Controllers;

use App\User;
use App\Organizer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * Class OrganizerController
 * @package App\Http\Controllers
 */
class OrganizerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::where('userable_type', 'like', '%Organizer')->get();

        if (Gate::allows('isAdmin') || Gate::allows('isCustomer')) {
            return view('organizers.index', compact('users'));
        }

        abort(404);
    }


    /**
     * @param $id
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('organizers.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'ragSociale' => ['required', 'string', 'max:255'],
            'numTelefono' => ['required', 'string', 'max:255'],
            'descrizione' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $new_user = User::create([
            'name' => $request['name'],
            'surname' => $request['surname'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        $new_organizer = Organizer::create([
            'ragSociale' => $request['ragSociale'],
            'descrizione' => $request['descrizione'],
            'numTelefono' => $request['numTelefono'],
        ]);

        $new_organizer->user()->save($new_user);
        $new_user->userable()->associate($new_organizer)->save();

        if (Gate::allows('isAdmin')) {
            return redirect('/organizers')->with('success', 'Organizzatore creato con successo.');
        } else {
            return redirect('/login')->with('success', 'Organizzatore creato con successo.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $current_user = User::findOrFail($id);
        $organizer = Organizer::findOrFail($current_user->userable_id);

        if (Gate::allows('isOrganizerOwner', $current_user) || Gate::allows('isAdmin')) {
            return view('organizers.edit', compact('organizer', 'current_user'));
        }
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $current_user = User::findOrFail($id);

        if (Gate::allows('isOrganizerOwner', $current_user) || Gate::allows('isAdmin')) {
            $user_data = $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'surname' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            ]);
            User::whereId($id)->update($user_data);

            $organizer_data = $request->validate([
                'ragSociale' => ['required', 'string', 'max:255'],
                'descrizione' => ['required', 'string', 'max:255'],
                'numTelefono' => ['required', 'string', 'max:255']
            ]);
            Organizer::whereId($current_user->userable_id)->update($organizer_data);

            return redirect('/organizers/'.$id.'/edit')->with('success', 'Profilo aggiornato con successo.');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $organizer = Organizer::findOrFail($user->userable_id);

        if (Gate::allows('isOrganizerOwner', $user) || Gate::allows('isAdmin')) {
            $organizer->delete();
            $user->delete();
        }

        if (Gate::allows('isAdmin')) {
            return redirect(route('organizers.index'))->with('success', 'Organizzatore cancellato con successo.');
        }

        if (Gate::allows('isOrganizerOwner', $user)) {
            return redirect('/login')->with('success', 'Organizzatore cancellato con successo.');
        }

        abort(404);
    }
}
