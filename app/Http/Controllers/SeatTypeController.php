<?php

namespace App\Http\Controllers;

use App\Event;
use App\Seat;
use App\SeatType;
use App\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class SeatTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Gate::allows('isAdmin')) {
            $seat_types = SeatType::all();
            return view('seat_types.index', compact('seat_types'));
        }

        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        if (Gate::allows('isAdmin')) {
            return view('seat_types.create');
        }

        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (Gate::allows('isAdmin')) {
            $validData = $request->validate([
                'name' => 'required|unique:seat_types,name|string|max:255',
            ]);
            $seat_type = SeatType::create($validData);

            return redirect(route('seat_types.index'))->with('success', 'Tipo posto '. $seat_type->name .' aggiunto!');
        }

        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SeatType  $seatType
     * @return \Illuminate\Http\Response
     */
    public function show(SeatType $seatType)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SeatType  $seatType
     * @return \Illuminate\Http\Response
     */
    public function edit(SeatType $seatType)
    {
        if (Gate::allows('isAdmin')) {
            return view('seat_types.edit', compact('seatType'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SeatType  $seatType
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, SeatType $seatType)
    {
        if (Gate::allows('isAdmin')) {
            $validData = $request->validate([
                'name' => ['required', 'string', 'max:255',
                    Rule::unique('seat_types', 'name')->ignore($seatType),
                ],
            ]);
            SeatType::whereId($seatType->id)->update($validData);


            return redirect(route('seat_types.index'))
                ->with('success', 'Tipo posto ' . $seatType->name . ' aggiornato con successo!');
        }

        abort(404);
    }

    /**
     * Remove a seat type.
     * While removing, it also removes seats assigned for an event, but only the ones
     * which still was not bought by customers.
     * @param  \App\SeatType  $seatType
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(SeatType $seatType)
    {
        if (Gate::allows('isAdmin')) {
            /**
             * OLD:
             * - decrease n_ticket in event, for all events using that seat_type
             *   - do not include bought tickets in count
             * - decrease n_ticket_available in event, for all events using that seat_type
             * --------------------------------------------------------------------------
             * NEW:
             * - remove seat (soft delete)
             * - remove seat_type (soft delete)
             * - re-count n_available_ticket (**for all events**)
             *      = sum(quantity) from seats group by event_id
             * - re-count n_ticket (**for all events**)
             *      = n_available_ticket + sold_tickets
             *   - così includiamo anche i posti venduti ma che sono stati cancellati
             */
            Seat::where('seat_type_id', $seatType->id)->delete();
            SeatType::whereId($seatType->id)->delete();


            $events_to_update = DB::table('events')
                                    ->select('events.*')
                                    ->join('seats', 'seats.event_id', '=', 'events.id')
                                    ->where('seats.seat_type_id', '=', $seatType->id)
                                    ->whereNull('events.deleted_at');

            foreach ($events_to_update as $event_to_update) {
                if(isset($event_to_update)) {
                    SeatController::recalculateNAvailTickets($event_to_update);
                }
            }

            /*
             * TODO
             * move to SeatController making static call

            $events_tickets_available = DB::table('seats')
                ->select('event_id', DB::raw('sum(quantity) as n_tickets'))
                ->groupBy('event_id')
                ->get();

            foreach ($events_tickets_available as $event_ticket_available) {
                Event::where('id', $event_ticket_available->event_id)
                        ->update([
                            'n_ticket_aval' => $event_ticket_available->quantity
                        ]);
            }
            */

            /*
             * We need to reiterate for all tickets:
             * calculating n_ticket starting from seats
             * is wrong because an event could have had
             * multiple removed seats in past.
             */

            foreach ($events_to_update as $event_to_update) {
                $event_to_update->n_ticket = $event_to_update->n_ticket_aval + TicketController::getNSoldTicketsForEvent($event_to_update);
                $event_to_update->save();
            }

            /*
            $events_tickets_sold = DB::table('tickets')
                ->select('event_id', DB::raw('count(*) as ticket_count'))
                ->whereNotNull('order_id')
                ->groupBy('event_id')
                ->get();

            foreach ($events_tickets_sold as $event_tickets_sold) {
                $event = Event::where('id', $event_tickets_sold->event_id)->get();
                $event->n_ticket = $event->n_ticket_aval + $event_tickets_sold->ticket_count;
                $event->save();
            }
            */

            return redirect(route('seat_types.index'))
                ->with('success', 'Tipo posto ' . $seatType->name . ' eliminato con successo!');
        }

        abort(404);
    }

    /**
     * TODO
     * check:
     * - delete:
     *   - se qualcuno ha comprato un biglietto per questa tipologia di posto
     *     - soft delete
     *     - oppure impossibilità di cancellare
     * - soft deletes ovunque
     * se nessuno ha comprato posti ricalcolo per tutti gli eventi
     * le disponibilità e i campi ridondanti
     *
     * disponibilità:
     * in insert, update, delete always recalculate
     * - n_total_tickets
     * - n_available_tickets
     *
     * sempre tenere conto che devo bloccare la possibilità di
     * togliere più posti di quanti biglietti ne sono stati acquistati
     */
}
