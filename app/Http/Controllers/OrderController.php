<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function index()
    {
        $current_user = User::findOrFail(Auth::id());

        if (Gate::allows('isAdmin')) {
            $orders = Order::all();
            return view('orders.index', compact('orders'));
        } elseif (Gate::allows('isCustomer')) {
            $orders = Order::where('customer_id', $current_user->id)->get();
            return view('orders.userindex', compact('orders'));
        }

        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);

        if (Gate::allows('isAdmin') || Gate::allows('isCustomerOwner', $order->user)) {
            $total_price = null;
            if (!empty($order->tickets)) {
                $total_price = CartController::calc_total_price($order->tickets);
            }
            return view('orders.show', compact('order', 'total_price'));
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        $tickets = $order->tickets;
        $userino = $order->user;

        if (Gate::allows('isAdmin') || Gate::allows('isCustomerOwner', $userino)) {
            foreach ($tickets as $ticket) {
                $event = $ticket->event;
                $event->n_ticket_aval = (int)$event->n_ticket_aval - (int)$ticket->quantity;
                $event->save();
                $ticket->delete();
            }
            $order->delete();

            if (Gate::allows('isAdmin')) {
                return redirect('/orders')->with('success', 'Ordine ' . $order->id . ' eliminato!');
            } elseif (Gate::allows('isCustomerOwner', $userino)) {
                return redirect('/cart');
            }
        }

        abort(404);
    }
}
