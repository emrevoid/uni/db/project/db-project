<?php

namespace App\Http\Controllers;

use App\Sponsor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class SponsorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Gate::allows('isAdmin')) {
            $sponsors = Sponsor::all();
            return view('sponsors.index', compact('sponsors'));
        }

        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('isAdmin')) {
            return view('sponsors.create');
        }

        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (Gate::allows('isAdmin')) {
            $validData = $request->validate([
                'name' => 'required|alpha_dash|max:255',
                'description' => 'required|max:500'
            ]);
            $sponsor = Sponsor::create($validData);

            return redirect(route('sponsors.index'))->with('success', 'Sponsor '. $sponsor->name .' creato!');
        }

        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sponsor  $sponsor
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Sponsor $sponsor)
    {
        return view('sponsors.show', compact('sponsor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function edit(Sponsor $sponsor)
    {
        if (Gate::allows('isAdmin')) {
            return view('sponsors.edit', compact('sponsor'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sponsor  $sponsor
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Sponsor $sponsor)
    {
        if (Gate::allows('isAdmin')) {
            $data = $request->validate([
                'name' => 'required|alpha_dash|max:255',
                'description' => 'required|max:500'
            ]);
            Sponsor::whereId($sponsor->id)->update($data);

            return redirect(route('sponsors.index'))
                ->with('success', 'Sponsor ' . $sponsor->name . ' aggiornato con successo!');
        }

        abort(404);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sponsor  $sponsor
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Sponsor $sponsor)
    {
        if (Gate::allows('isAdmin')) {
            Sponsor::whereId($sponsor->id)->delete();

            return redirect(route('sponsors.index'))
                ->with('success', 'Sponsor ' . $sponsor->name . ' eliminato con successo!');
        }

        abort(404);
    }
}
