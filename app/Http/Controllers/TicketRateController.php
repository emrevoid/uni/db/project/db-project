<?php

namespace App\Http\Controllers;

use App\TicketRate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class TicketRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Gate::allows('isAdmin')) {
            $ticketRates = TicketRate::all();
            return view('ticket_rates.index', compact('ticketRates'));
        }

        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        if (Gate::allows('isAdmin')) {
            return view('ticket_rates.create');
        }

        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        if (Gate::allows('isAdmin')) {
            $validData = $request->validate([
                'name' => ['required', 'alpha_dash', 'max:255',  'unique:ticket_rates,name'],
                'amount' => ['required', 'numeric', 'min:0.00', 'max:1.00']
            ]);
            $ticket_rate = TicketRate::create($validData);

            return redirect(route('ticket_rate.index'))->with('success', 'Tariffa '. $ticket_rate->name .' aggiunta!');
        }

        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TicketRate  $ticketRate
     * @return \Illuminate\Http\Response
     */
    public function show(TicketRate $ticketRate)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TicketRate  $ticketRate
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(TicketRate $ticketRate)
    {
        if (Gate::allows('isAdmin')) {
            return view('ticket_rates.edit', compact('ticketRate'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TicketRate  $ticketRate
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, TicketRate $ticketRate)
    {
        if (Gate::allows('isAdmin')) {
            $data = $request->validate([
                'name' => ['required', 'alpha_dash', 'max:255',
                    Rule::unique('ticket_rates', 'name')->ignore($ticketRate)
                ],
                'amount' => 'required|numeric|min:0.00|max:1.00'
            ]);
            TicketRate::whereId($ticketRate->id)->update($data);

            return redirect(route('ticket_rate.index'))
                    ->with('success', 'Tariffa ' . $ticketRate->name . ' aggiornata con successo!');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TicketRate  $ticketRate
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function destroy(TicketRate $ticketRate)
    {
        if (Gate::allows('isAdmin')) {
            TicketRate::whereId($ticketRate->id)->delete();

            return redirect(route('ticket_rate.index'))
                ->with('success', 'Tariffa ' . $ticketRate->name . ' eliminata con successo!');
        }

        abort(404);
    }
}
