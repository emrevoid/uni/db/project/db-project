<?php

namespace App\Http\Controllers;

use App\Event;
use App\Guest;
use App\Partecipate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class PartecipateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Gate::allows('isAdmin')) {
            $partecipates = Partecipate::select('partecipates.*')
                                        ->join('events', 'events.id', '=', 'partecipates.event_id')
                                        ->whereNull('events.deleted_at')
                                        ->get();
        } elseif (Gate::allows('isOrganizer')) {
            $partecipates = Partecipate::select('partecipates.*')
                                        ->join('events', 'events.id', '=', 'partecipates.event_id')
                                        ->whereNull('events.deleted_at')
                                        ->where('events.organizer_id', Auth::id())
                                        ->get();
        } else {
            abort(404);
        }

        return view('partecipates.index', compact('partecipates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        if (Gate::allows('isAdmin')) {
            $events = Event::all();
        } elseif (Gate::allows('isOrganizer')) {
            $events = Event::where('organizer_id', Auth::id())->get();
        } else {
            abort(404);
        }
        $guests = Guest::all();

        return view('partecipates.create', compact('events', 'guests'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validated_data = $request->validate([
            'event_id' => 'required',
            'guest_id' => ['required',
                Rule::unique('partecipates')->where(function ($query) use ($request) {
                    return $query->where('event_id', $request->get('event_id'));
                })]
        ]);

        Partecipate::create($validated_data);

        return redirect('/partecipates/create')->with('success', 'Ospite aggiunto con successo all\'evento');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Partecipate  $partecipate
     * @return \Illuminate\Http\Response
     */
    public function show(Partecipate $partecipate)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Partecipate  $partecipate
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        if (Gate::allows('isAdmin')) {
            $events = Event::all();
        } elseif (Gate::allows('isOrganizer')) {
            $events = Event::where('organizer_id', Auth::id())->get();
        } else {
            abort(404);
        }

        $partecipate = Partecipate::findOrFail($id);
        $guests = Guest::all();

        return view('partecipates.edit', compact('events', 'guests', 'partecipate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Partecipate  $partecipate
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $validated_data = $request->validate([
            'event_id' => 'required',
            'guest_id' => 'required',
        ]);

        $new_data = $request->validate([
            'event_id' => 'required',
            'guest_id' => 'required',
        ]);

        $partecipate = Partecipate::findOrFail($id);

        $new_data['guest_id'] = $validated_data['new_guest_id'];
        $partecipate->update($new_data);

        return redirect('/partecipates/'.$id.'/edit')->with('success', 'Ospite modificato con successo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Partecipate  $partecipate
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $partecipate = Partecipate::findOrFail($id);

        $partecipate->delete();

        return redirect('/partecipates/create')->with('success', 'Ospite eliminato con successo');
    }
}
