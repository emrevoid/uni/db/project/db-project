<?php

namespace App\Http\Controllers;

use App\Statistic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $profit_all = DB::table('tickets')
            ->select(DB::raw('round(sum(tickets.final_price),2) as total_profit'))
            ->get();

        return view('statistics.index', compact('profit_all'));
    }

    public static function categoryRanking()
    {
        $category_ranking = DB::table('macrocategories')
            ->join('categories', 'macrocategories.id', '=', 'categories.macrocategory_id')
            ->join('events', 'categories.id', '=', 'events.category_id')
            ->join('tickets', 'events.id', '=', 'tickets.event_id')
            ->select('categories.id', 'categories.name as subcat', 'macrocategories.name as macrocat', DB::raw('round(sum(tickets.final_price),2) as total_profit'))
            ->groupBy('categories.id')
            ->orderByDesc('total_profit')
            ->get();

        return view('statistics.category-ranking', compact('category_ranking'));
    }

    public static function categoryAgeAverage()
    {
        $category_age_average = DB::table('macrocategories')
            ->join('categories', 'macrocategories.id', '=', 'categories.macrocategory_id')
            ->join('events', 'categories.id', '=', 'events.category_id')
            ->join('tickets', 'events.id', '=', 'tickets.event_id')
            ->join('orders', 'tickets.order_id', '=', 'orders.id')
            ->join('users', 'orders.customer_id', '=', 'users.id')
            ->join('customers', 'users.userable_id', '=', 'customers.id')
            ->select('categories.id', 'categories.name as subcat', 'macrocategories.name as macrocat', DB::raw('round(avg(year(now()) - year(customers.dataNascita)),0) as age'))
            ->groupBy('categories.id')
            ->orderByDesc('age')
            ->get();

        return view('statistics.category-age-average', compact('category_age_average'));
    }

    public static function customerSpendingAverage()
    {
        $customer_spending_average = DB::table('tickets')
            ->join('orders', 'tickets.order_id', '=', 'orders.id')
            ->join('users', 'orders.customer_id', '=', 'users.id')
            ->select('users.id', 'users.email as u_email', DB::raw('round(avg(tickets.final_price),2) as f_price'))
            ->groupBy('users.id')
            ->orderByDesc('f_price')
            ->get();

        return view('statistics.customer-spending-average', compact('customer_spending_average'));
    }

    public static function eventProfit()
    {
        $event_profit = DB::table('events')
            ->join('tickets', 'events.id', '=', 'tickets.event_id')
            ->select('events.id', 'events.name as e_name', DB::raw('sum(tickets.final_price) as f_price'))
            ->groupBy('events.id')
            ->orderByDesc('f_price')
            ->get();

        return view('statistics.event-profit', compact('event_profit'));
    }

    public static function placeProfit()
    {
        $place_profit = DB::table('places')
            ->join('events', 'places.id', '=', 'events.place_id')
            ->join('tickets', 'events.id', '=', 'tickets.event_id')
            ->select('places.id', 'places.name as p_name', DB::raw('sum(tickets.final_price) as f_price'))
            ->groupBy('places.id')
            ->orderByDesc('f_price')
            ->get();

        return view('statistics.place-profit', compact('place_profit'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function placeAgeAverage()
    {
        $place_age_average = DB::table('places')
            ->join('events', 'places.id', '=', 'events.place_id')
            ->join('tickets', 'events.id', '=', 'tickets.event_id')
            ->join('orders', 'tickets.order_id', '=', 'orders.id')
            ->join('users', 'orders.customer_id', '=', 'users.id')
            ->join('customers', 'users.userable_id', '=', 'customers.id')
            ->select('places.id', 'places.name as p_name', DB::raw('round(avg(year(now()) - year(customers.dataNascita)),0) as age'))
            ->groupBy('places.id')
            ->orderByDesc('age')
            ->get();

        return view('statistics.place-age-average', compact('place_age_average'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function sponsorRanking()
    {
        $sponsor_ranking = DB::table('funds')
            ->join('sponsors', 'funds.sponsor_id', '=', 'sponsors.id')
            ->select('funds.sponsor_id', 'sponsors.name as spo_name', DB::raw('sum(funds.amount) as total_funds'))
            ->groupBy('sponsor_id')
            ->orderByDesc('total_funds')
            ->get();

        return view('statistics.sponsor-ranking', compact('sponsor_ranking'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Statistic  $statistic
     * @return \Illuminate\Http\Response
     */
    public function show(Statistic $statistic)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Statistic  $statistic
     * @return \Illuminate\Http\Response
     */
    public function edit(Statistic $statistic)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Statistic  $statistic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Statistic $statistic)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Statistic  $statistic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Statistic $statistic)
    {
        abort(404);
    }
}
