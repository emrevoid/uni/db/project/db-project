<?php

namespace App\Http\Controllers;

use App\Event;
use App\Fund;
use App\SponsorRange;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class SponsorRangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Gate::allows('isAdmin')) {
            $sponsor_ranges = SponsorRange::all();
            return view('sponsor_ranges.index', compact('sponsor_ranges'));
        }

        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        if (Gate::allows('isAdmin')) {
            return view('sponsor_ranges.create');
        }

        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (Gate::allows('isAdmin')) {
            $validData = $request->validate([
                'name' => 'required|unique:sponsor_ranges,name|alpha_dash|max:255',
                'amount' => 'required|unique:sponsor_ranges,amount|numeric|min:0.00|max:1.00'
            ]);
            $sponsor_range = SponsorRange::create($validData);
            FundController::recalculateAllFunds();

            return redirect(route('sponsor_ranges.index'))->with('success', 'Fascia sponsor '. $sponsor_range->name .' creata!');
        }

        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SponsorRange  $sponsorRange
     * @return \Illuminate\Http\Response
     */
    public function show(SponsorRange $sponsorRange)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SponsorRange  $sponsorRange
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(SponsorRange $sponsorRange)
    {
        if (Gate::allows('isAdmin')) {
            return view('sponsor_ranges.edit', compact('sponsorRange'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SponsorRange  $sponsorRange
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, SponsorRange $sponsorRange)
    {
        if (Gate::allows('isAdmin')) {
            /*
            $data = $request->validate([
                'name' => 'required|alpha_dash|max:255',
                'amount' => 'required|numeric|min:0.00|max:1.00'
            ]);
            */
            $data = $request->validate([
                'name' => ['required', Rule::unique('sponsor_ranges', 'name')->ignore($sponsorRange), 'alpha_dash', 'max:255'],
                'amount' => ['required', Rule::unique('sponsor_ranges', 'amount')->ignore($sponsorRange), 'numeric', 'min:0.00', 'max:1.00'],
            ]);
            SponsorRange::whereId($sponsorRange->id)->update($data);
            FundController::recalculateAllFunds();

            return redirect(route('sponsor_ranges.index'))
                ->with('success', 'Fascia sponsor ' . $sponsorRange->name . ' aggiornata con successo!');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SponsorRange  $sponsorRange
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(SponsorRange $sponsorRange)
    {
        if (Gate::allows('isAdmin')) {
            Fund::where('sponsor_range_id', $sponsorRange->id)
                  ->update(['sponsor_range_id' => null]);
            SponsorRange::whereId($sponsorRange->id)->delete();
            FundController::recalculateAllFunds();

            return redirect(route('sponsor_ranges.index'))
                ->with('success', 'Fascia sponsor ' . $sponsorRange->name . ' eliminata con successo!');
        }

        abort(404);
    }
}
