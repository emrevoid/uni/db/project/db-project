<?php

namespace App\Http\Controllers;

use App\Order;
use App\Ticket;
use App\Event;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

/**
 * Class TicketController
 * @package App\Http\Controllers
 */
class TicketController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Gate::allows('isAdmin')) {
            $tickets = Ticket::all();
            return view('tickets.index', compact('tickets'));
        }

        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        if (Gate::allows('isAdmin')) {
            $events = Event::all();
            $orders = Order::all();
            return view('tickets.create', compact('orders', 'events'));
        }

        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (Gate::allows('isAdmin')) {
            return $this->store_admin($request);
        } elseif (Gate::allows('isCustomer')) {
            return $this->store_customer($request);
        }

        abort(404);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function store_customer(Request $request)
    {
        if (Gate::allows('isCustomer')) {
            $validatedData = $request->validate([
                'event_id' => 'required'
            ]);
            $event = Event::findOrFail($request['event_id']);

            if (isset($event->n_ticket) && $event->n_ticket - $event->n_ticket_aval <= 0) {
                return redirect('/')->with('error', 'L\'evento ' .
                    $event->name . ' non ha più biglietti acquistabili.');
            }

            if (strtotime($event->end_date) < time()) {
                return redirect('/')->with('error', 'L\'evento ' . $event->name . ' si è già concluso.');
            }

            $order = Order::where(function ($query) {
                $query->where('customer_id', Auth::id())
                    ->where('paid', false);
            })->first();

            if (!isset($order)) {
                $order = Order::create(array('customer_id' => Auth::id()));
            } else {
                $ticket_exists = $order->tickets->where('event_id', $event->id)->first();
                if (isset($ticket_exists)) {
                    return redirect('/')->with('error', 'Hai già biglietti relativi a ' .
                        $event->name . ' nel carrello.');
                }
            }
            $validatedData['order_id'] = $order->id;
            $validatedData['quantity'] = 1;

            $ticket = Ticket::create($validatedData);
            $event->n_ticket_aval++;
            $event->save();
            $this->update_cart_time($ticket);
            return redirect('/')->with('success', 'Biglietto ' . $event->name . ' aggiunto!');
        }

        abort(404);
    }

    public function show($id)
    {
        abort(404);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function store_admin(Request $request)
    {
        if (Gate::allows('isAdmin')) {
            $validatedData = $request->validate([
                'event_id' => 'required',
                'order_id' => 'required',
                'quantity' => 'required|gte:1|lte:5'
            ]);
            $event = Event::findOrFail($request['event_id']);
            $order = Order::findOrFail($request['order_id']);

            if (isset($event->n_ticket) &&
                $event->n_ticket - $event->n_ticket_aval - (int)$validatedData['quantity'] < 0) {
                return redirect('/tickets')->with('error', 'L\'evento ' .
                    $event->name . ' non ha più biglietti acquistabili o la quantità richesta è troppo elevata.');
            }

            if (strtotime($event->end_date) < time()) {
                return redirect('/tickets')->with('error', 'L\'evento ' . $event->name . ' si è già concluso.');
            }

            $ticket_exists = $order->tickets->where('event_id', $event->id)->first();
            if (isset($ticket_exists)) {
                return redirect('/tickets')->with('error', 'Ci sono già biglietti relativi a ' .
                    $event->name . ' in quest\'ordine.');
            }

            $ticket = Ticket::create($validatedData);
            $event->n_ticket_aval += (int)$validatedData['quantity'];
            $event->save();
            $this->update_cart_time($ticket);
            return redirect('/tickets')->with('success', 'Biglietto ' . $ticket->id . ' aggiunto!');
        }

        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $ticket = Ticket::findOrFail($id);

        if (Gate::allows('isAdmin')) {
            return view('tickets.edit', compact('ticket'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        if (Gate::allows('isAdmin')) {
            $validatedData = $request->validate([
                'quantity' => 'required|gt:0'
            ]);
            $ticket = Ticket::findOrFail($id);
            $delta = $validatedData['quantity'] - $ticket->quantity;
            $event = $ticket->event;

            if ($delta === 0) {
                return redirect('/tickets')->with('success', 'Biglietto ' . $ticket->id . ' non aggiornato.');
            }

            if (isset($event->n_ticket)) {
                $ticket_remaining = $event->n_ticket - $event->n_ticket_aval;
                if ($ticket_remaining - $delta >= 0) {
                    $event->n_ticket_aval += $delta;
                    $event->save();
                    $ticket->update($validatedData);
                    $this->update_cart_time($ticket);
                    return redirect('/tickets')->with('success', 'Biglietto ' . $ticket->id . ' aggiornato.');
                } else {
                    return redirect('/tickets')->with('error', 'Biglietti rimasti non sufficienti.');
                }
            } else {
                $event->n_ticket_aval += $delta;
                $event->save();
                $ticket->update($validatedData);
                $this->update_cart_time($ticket);
                return redirect('/tickets')->with('success', 'Biglietto ' . $ticket->id . ' aggiornato.');
            }
        }
        abort(404);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $ticket = Ticket::findOrFail($id);
        $current_user = $ticket->order->user;

        if (Gate::allows('isAdmin') || Gate::allows('isCustomerOwner', $current_user)) {
            $event = $ticket->event;
            ++$event->n_ticket_aval;
            $event->save();
            $ticket->delete();

            $order = $ticket->order;
            if (\count($order->tickets) === 0) {
                $order->delete();
            }
        }

        if (Gate::allows('isAdmin')) {
            return redirect('/tickets')->with('success', 'Il biglietto ' . $ticket->id . ' è stato cancellato!');
        } elseif (Gate::allows('isCustomerOwner', $current_user)) {
            return redirect(route('orders.index'));
        }

        abort(404);
    }

    public static function getNSoldTicketsForEvent(Event $event)
    {
        return DB::table('tickets')
                    ->whereNotNull('order_id')
                    ->where('event_id', '=', $event->id)
                    ->count();
    }
}
