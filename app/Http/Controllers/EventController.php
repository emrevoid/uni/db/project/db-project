<?php

namespace App\Http\Controllers;

use App\Category;
use App\Event;
use App\Favorite;
use App\Fund;
use App\Guest;
use App\Macrocategory;
use App\Organizer;
use App\Place;
use App\Partecipate;
use App\Seat;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

/**
 * Class EventController
 * @package App\Http\Controllers
 */
class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show', 'search']);
    }

    /**
     * @param Request $request
     */
    public function validateDateTime(Request $request)
    {
        $request->validate([
            'start_date' => 'required|after_or_equal:now',
            'end_date' => 'required|after_or_equal:start_date',
            'start_time' => 'required',
            'end_time' => 'required'
        ], [
            'start_date.after_or_equal' => 'Il campo :attribute deve essere una data successiva o uguale a oggi.'
        ]);

        $request['start_datetime'] = $request['start_date'] . ' ' . $request['start_time'];
        $request['end_datetime'] = $request['end_date'] . ' ' . $request['end_time'];

        $request->validate([
            'start_datetime' => 'required|after_or_equal:now',
            'end_datetime' => 'required|after_or_equal:start_datetime'
        ], [
            'start_datetime.after_or_equal' => 'Il campo :attribute deve essere una data successiva o uguale a oggi.'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $events = Event::where(function ($query) {
            $query->whereDate('end_date', '>=', date('Y-m-d'))
                ->where('n_ticket', '>', 0)
                ->where('n_ticket_aval', '>', 0);
        })->paginate(6);

        if (Auth::check()) {
            $current_user = User::findOrFail(Auth::id());

            if (Gate::allows('isAdmin')) {
                $events = Event::all();
                return view('events.index', compact('events'));
            }

            if (Gate::allows('isOrganizer')) {
                $events = Event::where('organizer_id', $current_user->id)->get();
                return view('events.organizer-index', compact('events'));
            }

            if (Gate::allows('isCustomer')) {
                $macrocategories = Macrocategory::all();

                $favorites = Favorite::where('customer_id', $current_user->id)->get();
                $favorites_events_ids = [];
                foreach ($favorites as $favorite) {
                    $favorites_events_ids[] = $favorite->event_id;
                }

                foreach ($events as $event) {
                    $event['price'] = Seat::where('event_id', $event->id)
                                            ->min('unit_price');
                }
                return view('index', compact('events', 'macrocategories', 'favorites_events_ids'));
            }
        }

        $macrocategories = Macrocategory::all();

        return view('index', compact('events', 'macrocategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        if (Gate::allows('isAdmin') || Gate::allows('isOrganizer')) {
            $macrocategories = Macrocategory::all();
            $organizers = User::where('userable_type', 'App\Organizer')->get();
            $current_user = User::findOrFail(Auth::id());
            $places = Place::all();
            $guests = Guest::all();

            return view('events.create', compact('macrocategories', 'organizers', 'current_user', 'guests', 'places'));
        }

        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (Gate::allows('isAdmin') || Gate::allows('isOrganizer')) {
            $this->validateDateTime($request);

            $validated_data = $request->validate([
                'name' => 'required|max:50|unique:events,name',
                'description' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
                'start_date' => 'required',
                'end_date' => 'required',
                'start_time' => 'required',
                'end_time' => 'required',
                'category_id' => 'required|exists:categories,id',
                'place_id' => 'required|exists:places,id'
            ]);

            $validated_data['n_ticket'] = 0;
            $validated_data['n_ticket_aval'] = 0;

            $current_user = User::findOrFail(Auth::id());

            if ($current_user->userable_type === 'App\Organizer') {
                $validated_data['organizer_id'] = Auth::id();
            } else {
                $request->validate(['organizer_id' => 'required|exists:users,id']);
                $organizer = User::findOrFail($request['organizer_id']);
                if ($organizer->userable_type !== 'App\Organizer') {
                    abort(404);
                }
                $validated_data['organizer_id'] = $request['organizer_id'];
            }

            $event = Event::create($validated_data);

            $image = $request->file('image');
            $image_name = Str::slug($request->input('name')) . '_' .
                time() . '.' . $image->getClientOriginalExtension();
            $folder = 'uploads/images/';
            $image->storePubliclyAs($folder, $image_name, 's3');
            $event->image = $folder . $image_name;

            $event->save();
            return redirect(route('seats.create'))->with('success', 'Evento ' . $event->name . ' aggiunto! Ora assegna dei posti all\'evento.');
        }

        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);
        $organizer = User::findOrFail($event->organizer_id)->userable;
        $category = Category::findOrFail($event->category_id);
        $current_user = User::findOrFail(Auth::id());
        $place = Place::findOrFail($event->place_id);

        $event['price'] = Seat::where('event_id', $event->id)
                                ->min('unit_price');

        $fav_events_ids = [];
        if (Gate::allows('isCustomer')) {
            $favorites = Favorite::where('customer_id', $current_user->id)->get();
            foreach ($favorites as $favorite) {
                $fav_events_ids[] = $favorite->event_id;
            }
        }

        if ($current_user !== null) {
            return view('events.show', compact(
                'event',
                'organizer',
                'category',
                'current_user',
                'fav_events_ids',
                'place'
            ));
        }

        return view('events.show', compact('event', 'organizer', 'category', 'place'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);
        $user_to_check = User::findOrFail($event->organizer_id);
        $current_user = User::findOrFail(Auth::id());

        if (Gate::allows('isAdmin') || Gate::allows('isOrganizerOwner', $user_to_check)) {
            $event_start_datetime = $event->start_date . ' ' . $event->start_time;

            if (strtotime($event_start_datetime) < strtotime('now') &&
                $current_user->userable_type !== 'App\Administrator') {
                return redirect('/events')->with(
                    'error',
                    'L\'evento selezionato non può essere modificato in quanto già iniziato/finito.'
                );
            }

            $places = Place::all();
            $guests = Guest::all();
            $macrocategories = Macrocategory::all();
            return view('events.edit', compact('event', 'macrocategories', 'guests', 'places'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $event = Event::findOrFail($id);
        $user_to_check = User::findOrFail($event->organizer_id);

        if (Gate::allows('isOrganizerOwner', $user_to_check) || Gate::allows('isAdmin')) {
            $this->validateDateTime($request);

            $validated_data = $request->validate([
                'name' => ['required', 'max:50',
                    Rule::unique('events', 'name')->ignore($event)
                ],
                'description' => 'required',
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
                'start_date' => 'required',
                'end_date' => 'required',
                'start_time' => 'required',
                'end_time' => 'required',
                'category_id' => 'required|exists:categories,id',
                'place_id' => 'required|exists:places,id'
            ]);

            /*
            if ($request['name'] !== $event->name) {
                $request->validate(['name' => 'unique:events']);
            }
            */
            /*
            if ($request->has('n_ticket_remaining')) {
                $request->validate(['n_ticket_remaining' => 'required|gte:0']);
                $validated_data['n_ticket'] = (int)$request['n_ticket_remaining'] + (int)$event->n_ticket_aval;
                unset($validated_data['n_ticket_remaining']);
            }
            if (isset($event->price)) {
                $validated_data['price'] = $event->price;
            } else {
                $validated_data['price'] = null;
            }
            */

            if ($request->has('image')) {
                Storage::disk('s3')->delete($event->image);
                $image = $request->file('image');
                $image_name = Str::slug($request->input('name')) . '_' .
                    time() . '.' . $image->getClientOriginalExtension();
                $folder = 'uploads/images/';
                $image->storePubliclyAs($folder, $image_name, 's3');
                Storage::disk('s3')->delete($event->image);
                $validated_data['image'] = $folder . $image_name;
            } else {
                $validated_data['image'] = $event->image;
            }

            $event->update($validated_data);

            return redirect('/events')->with('success', 'Evento ' . $event->name . ' aggiornato!');
        }

        abort(404);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function search(Request $request)
    {
        $macrocategories = Macrocategory::all();
//        $places = Place::all();

        if (isset($request['searchterm']) && $request['searchterm'] !== null) {
            $searchterm = $request['searchterm'];
            $organizers = DB::table('users')
                ->join('organizers', 'users.userable_id', '=', 'organizers.id')
                ->where('organizers.ragSociale', 'like', '%' . $searchterm . '%')
                ->select('users.id')
                ->get();
            $organizers_ids = [];
            foreach ($organizers as $organizer) {
                $organizers_ids[] = $organizer->id;
            }
            if (isset($request['category_id']) && $request['category_id'] !== null) {
                $category_id = $request['category_id'];
                $events = Event::where(function ($query) use ($searchterm, $category_id, $organizers_ids) {
                    $query->whereDate('end_date', '>=', date('Y-m-d'))
                        ->where('category_id', $category_id)
                        ->where(function ($query) use ($searchterm, $organizers_ids) {
                            $query->WhereIn('organizer_id', $organizers_ids)
                                ->orWhere('name', 'like', '%' . $searchterm . '%')
                                ->orWhere('guest', 'like', '%' . $searchterm . '%')
                                ->orWhere('place', 'like', '%' . $searchterm . '%');
                        });
                })->paginate(6);
                return view('index', compact('events', 'macrocategories', 'searchterm', 'category_id'));
            } else {
                $events = Event::where(function ($query) use ($searchterm, $organizers_ids) {
                    $query->whereDate('end_date', '>=', date('Y-m-d'))
                        ->where(function ($query) use ($searchterm, $organizers_ids) {
                            $query->WhereIn('organizer_id', $organizers_ids)
                                ->orWhere('name', 'like', '%' . $searchterm . '%')
                                ->orWhere('guest', 'like', '%' . $searchterm . '%')
                                ->orWhere('place', 'like', '%' . $searchterm . '%');
                        });
                })->paginate(6);
            }
            return view('index', compact('events', 'macrocategories', 'searchterm'));
        } else {
            if (isset($request['category_id']) && $request['category_id'] !== null) {
                $category_id = $request['category_id'];
                $events = Event::where(function ($query) use ($category_id) {
                    $query->whereDate('end_date', '>=', date('Y-m-d'))
                        ->where('category_id', $category_id);
                })->paginate(6);
                return view('index', compact('events', 'macrocategories', 'category_id'));
            } else {
                $events = Event::paginate(6);
            }
        }

        return view('index', compact('events', 'macrocategories'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        $user_to_check = User::findOrFail($event->organizer_id);

        if (Gate::allows('isOrganizerOwner', $user_to_check) || Gate::allows('isAdmin')) {
            if ($event->start_date < date('Y-m-d')) {
                return redirect('/events')->with('error', 'L\'evento ' .
                    $event->name . ' non può più essere cancellato/modificato in quanto è già iniziato.');
            }

            /*
            $event->favorites->delete();
            $event->tickets->delete();
            $event->seats->delete();
            $event->funds->delete(); /* also recalcolate funds: put business logic in observer ?
            $event->partecipates->delete();
            */

            Favorite::destroy(Favorite::where('event_id', $id)->get());
            Ticket::destroy(Ticket::where('event_id', $id)->get());
            Seat::destroy(Seat::where('event_id', $id)->get());
            Fund::destroy(Fund::where('event_id', $id)->get());
            Partecipate::destroy(Partecipate::where('event_id', $id)->get());

            /*
            Favorite::destroy(Favorite::select('id')->where('event_id', $id)->get());
            Ticket::destroy(Ticket::select('id')->where('event_id', $id)->get());
            Seat::destroy(Seat::select('id')->where('event_id', $id)->get());
            Fund::destroy(Fund::select('id')->where('event_id', $id)->get());
            Partecipate::destroy(Partecipate::select('id')->where('event_id', $id)->get());
            */

            /*
            Favorite::where('event_id', $event->id)->delete();

            Ticket::where('event_id', $event->id)->delete();
            Seat::where('event_id', $event->id)->delete();
            Fund::where('event_id', $event->id)->delete();
            Partecipate::where('event_id', $event->id)->delete();
            */

            // Storage::disk('s3')->delete($event->image);

            $event->delete();

            return redirect('/events')->with('success', 'Evento ' . $event->name . ' cancellato!');
        }

        abort(404);
    }
}
