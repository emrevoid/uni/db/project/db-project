<?php

namespace App\Http\Controllers;

use App\User;
use App\Customer;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;

/**
 * Class CustomerController
 * @package App\Http\Controllers
 */
class CustomerController extends Controller
{
    protected $redirectTo = '/home';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::where('userable_type', 'like', '%Customer')->get();

        if (Gate::allows('isAdmin')) {
            return view('customers.index', compact('users'));
        }

        abort(404);
    }

    /**
     * @param $id
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * @param $request
     */
    public function store(Request $request)
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $current_user = User::findOrFail($id);
        $customer = Customer::findOrFail($current_user->userable_id);

        if (Gate::allows('isCustomerOwner', $current_user) || Gate::allows('isAdmin')) {
            return view('customers.edit', compact('customer', 'current_user'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $current_user = User::findOrFail($id);

        if (Gate::allows('isCustomerOwner', $current_user) || Gate::allows('isAdmin')) {
            $user_data = $request->validate([
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'name' => ['required', 'string', 'max:255'],
                'surname' => ['required', 'string', 'max:255'],
            ]);
            User::whereId($id)->update($user_data);

            $customer_data = $request->validate([
                'sesso' => ['required', 'string', 'max:255'],
                'dataNascita' => ['required', 'date'],
                'residenza' => ['required', 'string', 'max:255']
            ]);
            Customer::whereId($current_user->userable_id)->update($customer_data);

            return redirect('/customers/'.$id.'/edit')->with('success', 'Profilo aggiornato con successo!');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $customer = Customer::findOrFail($user->userable_id);

        if (Gate::allows('isCustomerOwner', $user) || Gate::allows('isAdmin')) {
            $customer->delete();
            $user->delete();

        }
        if (Gate::allows('isAdmin')) {
            return redirect('/customers')->with('success', 'Cliente cancellato con successo!');
        }

        if (Gate::allows('isCustomerOwner', $user)) {
            return redirect('/register')->with('success', 'Cliente cancellato con successo!');
        }

        abort(404);
    }
}
