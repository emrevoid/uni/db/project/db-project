<?php

namespace App\Http\Controllers;

use App\Category;
use App\Macrocategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

/**
 * Class CategoryController
 * @package App\Http\Controllers
 */
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index()
    {
        if (Gate::allows('isAdmin')) {
            return redirect()->action('MacrocategoryController@index');
        }

        abort(404);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $macrocategories = Macrocategory::all();

        if (Gate::allows('isAdmin')) {
            return view('categories.create', compact('macrocategories'));
        }

        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (Gate::allows('isAdmin')) {
            $data = $request->validate([
                'name' => ['required', 'max:255',
                    Rule::unique('categories')->where(function ($query) use ($request) {
                        return $query->where('macrocategory_id', $request->get('macrocategory_id'));
                    })],
                'macrocategory_id' => 'required'
            ]);
            $category = Category::create($data);

            return redirect('/macrocategories')->with('success', 'Categoria '. $category->name .' aggiunta!');
        }

        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        $current_macrocategory = $category->macrocategory;
        $macrocategories = Macrocategory::all();

        if (Gate::allows('isAdmin')) {
            return view('categories.edit', compact('category', 'current_macrocategory', 'macrocategories'));
        }

        abort(404);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        if (Gate::allows('isAdmin')) {
            $category = Category::findOrFail($id);
            $data = $request->validate([
                'name' => ['required', 'max:255',
                    Rule::unique('categories')->where(function ($query) use ($request) {
                        return $query->where('macrocategory_id', $request->get('macrocategory_id'));
                    })],
                'macrocategory_id' => 'required'
            ]);
            Category::whereId($id)->update($data);

            return redirect('/macrocategories')
                ->with('success', 'Categoria ' . $category->name . ' aggiornata con successo!');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);

        if (Gate::allows('isAdmin')) {
            $category->delete();

            return redirect('/macrocategories')
            ->with('success', 'Categoria ' . $category->name . ' eliminata con successo!');
        }

        abort(404);
    }
}
