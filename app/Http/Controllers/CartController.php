<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Event;
use App\Seat;
use App\SeatType;
use App\Ticket;
use App\TicketRate;
use App\User;
use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

/**
 * Class CartController
 * @package App\Http\Controllers
 */
class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Gate::allows('isCustomer')) {
            $tickets = Cart::getCartItems();

            $total_price = null;
            if (!empty($tickets)) {
                $total_price = static::calc_total_price($tickets);
            }

            $ticket_rates = TicketRate::all();
            $seat_types = SeatType::all();

            return view('cart.index', compact('tickets', 'total_price', 'ticket_rates', 'seat_types'));
        }

        abort(404);
    }

    /**
     * Create a new Ticket for a given Event and add it to the Cart.
     * - checks if event has ended
     * - checks if there are more than N tickets for the same event in cart
     * - checks if there are still seats available for the event
     * - create ticket
     * - add ticket to cart
     * @param $event_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function add_ticket($event_id)
    {
        if (Gate::allows('isCustomer')) {
            $current_user = User::findOrFail(Auth::id());
            $event = Event::findOrFail($event_id);

            if (strtotime($event->end_date) < time()) {
                return response('err_time', 400);
            }

            $tickets_count = DB::table('cart')
                ->join('tickets', 'cart.ticket_id', '=', 'tickets.id')
                ->join('events', 'tickets.event_id', '=', 'events.id')
                ->where('cart.customer_id', $current_user->id)
                ->where('events.id', $event->id)
                ->count();

            if ($tickets_count >= 5) {
                return response('err_max_tickets', 400);
            }

            if (isset($event->n_ticket_aval) && $event->n_ticket_aval <= 0) {
                return response('err_n_ticket', 400);
            }

            $event->n_ticket_aval--;
            $event->save();

            $newTicket = Ticket::create([
                'event_id' => $event_id,
            ]);

            Cart::create([
                'customer_id' => $current_user->id,
                'ticket_id' => $newTicket->id
            ]);

            return response('success', 200);
        }

        abort(404);
    }

    /**
     * Pay the order:
     * - checks if all tickets in cart have seat_type_id and ticket_rate_id set
     * - remove tickets from favorites
     * - create a new order
     * - link tickets to order
     * - remove tickets from cart
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function pay()
    {
        $current_user = User::findOrFail(Auth::id());

        $itemsInCart = Cart::getCartItems();

        $userCart = Cart::where('ticket_id', $itemsInCart[0]->id)->first();

        if (!empty($itemsInCart) && Gate::allows('isCustomerOwner', $userCart->user)) {
            foreach ($itemsInCart as $itemInCart) {
                if (!isset($itemInCart->seat_type, $itemInCart->ticket_rate)) {
                    return redirect(route('cart.index'))
                        ->withErrors('Seleziona un posto e una tariffa per tutti i biglietti.');
                }
            }

            $order = Order::create([
                'customer_id' => $current_user->id
            ]);

            /**
             * TODO
             * a query is better?
             */
            foreach ($itemsInCart as $itemInCart) {
                foreach ($current_user->favorites as $favorite) {
                    if ($itemInCart->event->id === $favorite->event->id) {
                        $favorite->delete();
                    }
                }
                $itemInCart->order_id = $order->id;
                $itemInCart->save();
                Cart::where('ticket_id', $itemInCart->id)->first()->delete();
            }
            return redirect(route('orders.show', $order->id))
                    ->with('success', 'Hai appena effettuato l\'ordine numero '.$order->id.'.');
        }

        abort(404);
    }

    /**
     * Update tickets' in cart:
     * - checks if sent tickets are in cart
     * - checks if seat_type is set and is valid (seat->quantity - sold tickets for that quantity)
     * - checks if ticket_rate is set and is valid
     * - calculates and sets final price for ticket
     * - set ticket_rate to 0 (if available) if seat_type is 0
     * TODO COMPLETE
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $ticket_id)
    {
        if (!Gate::allows('isCustomer')) {
            abort(404);
        }

        $current_user = User::findOrFail(Auth::id());

        if (Gate::allows('isCustomerOwner', $current_user)) {
            $ticket = Ticket::findOrFail($ticket_id);

            $cart_item = Cart::where('customer_id', $current_user->id)
                ->where('ticket_id', $ticket->id)
                ->first();

            if (!isset($cart_item)) {
                return redirect(route('cart.index'))->withErrors('Il biglietto non è presente nel tuo carrello.');
            }

            $validData = $request->validate([
                'seat_type_id' => 'required|exists:seat_types,id',
                'ticket_rate_id' => 'required|exists:ticket_rates,id'
            ]);

            $seat = Seat::where('seat_type_id', $request['seat_type_id'])
                        ->where('event_id', $ticket->event->id)
                        ->first();

            $tickets_sold_seat_type = Ticket::whereNotNull('order_id')
                                            ->where('event_id', $ticket->event->id)
                                            ->where('seat_type_id', $request['seat_type_id'])
                                            ->count();

            if ($seat->quantity > $tickets_sold_seat_type) {
                $ticket->seat_type_id = $validData['seat_type_id'];
            } else {
                return redirect(route('cart.index'))
                        ->withErrors('Non sono più disponibili biglietti per l\'evento '.$ticket->event->name .
                            ' con posto ' . SeatType::findOrFail($request['seat_type_id'])->name);
            }

            $ticket->ticket_rate_id = $validData['ticket_rate_id'];

            $ticket_rate_zero = TicketRate::where('amount', 0.0)->first();
            if ((double)$seat->unit_price === 0.0 &&
                isset($ticket_rate_zero->amount) &&
                (double)$ticket_rate_zero->amount === 0.0) {
                $ticket->ticket_rate_id = $ticket_rate_zero->id;
            }

            $ticket->final_price = $seat->unit_price * (1 - TicketRate::findOrFail($ticket->ticket_rate_id)->amount);
            $ticket->save();

            return redirect(route('cart.index'));
        }
    }

    /**
     * Remove a ticket from cart, aka
     * - delete cart entry
     * - delete ticket entry
     * - increment n_ticket_aval
     * @param Cart $cart
     */
    public function destroy($ticket_id)
    {
        $cart = Cart::where('ticket_id', $ticket_id)->first();

        if (Gate::allows('isCustomerOwner', $cart->user)) {
            $event = $cart->ticket->event;
            ++$event->n_ticket_aval;
            $event->save();
            $ticket = $cart->ticket;
            $cart->delete();
            $ticket->delete();
            return redirect(route('cart.index'));
        }

        abort(404);
    }

    /**
     * @param $tickets
     * @return float|int
     */
    public static function calc_total_price($tickets)
    {
        $total_price = 0;
        foreach ($tickets as $ticket) {
            if (isset($ticket->final_price)) {
                $total_price += $ticket->final_price;
            }
        }
        return $total_price;
    }
}
