<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class MyEventsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Gate::allows('isCustomer')) {
            $current_user = User::findOrFail(Auth::id());
            $favorites = FavoriteController::index();

            $orders = Order::where(function ($query) use ($current_user) {
                $query->where('customer_id', $current_user->id);
//                    ->where('paid', true);
            })->get();
            $next_events = [];
            $past_events = [];
            foreach ($orders as $order) {
                $tickets = $order->tickets;
                foreach ($tickets as $ticket) {
                    $event = $ticket->event;
                    if (!\in_array($event, $next_events, true) && !\in_array($event, $past_events, true)) {
                        if (strtotime($event->end_date) < time()) {
                            $past_events[] = $event;
                        } else {
                            $next_events[] = $event;
                        }
                    }
                }
            }

            return view('myevents.index', compact('favorites', 'next_events', 'past_events'));
        }

        abort(404);
    }
}
