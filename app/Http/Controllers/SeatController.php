<?php

namespace App\Http\Controllers;

use App\Event;
use App\Seat;
use App\SeatType;
use App\Ticket;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class SeatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Gate::allows('isAdmin')) {
            $seats = Seat::select('seats.*')
                            ->join('events', 'events.id', '=', 'seats.event_id')
                            ->whereNull('events.deleted_at')
                            ->get();
        } elseif (Gate::allows('isOrganizer')) {
            $seats = Seat::select('seats.*')
                            ->join('events', 'events.id', '=', 'seats.event_id')
                            ->join('users', 'events.organizer_id', '=', 'users.id')
                            ->where('users.id', '=', Auth::id())
                            ->whereNull('events.deleted_at')
                            ->get();
        } else {
            abort(404);
        }

        return view('seats.index', compact('seats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $seat_types = SeatType::all();
        if (Gate::allows('isAdmin')) {
            $events = Event::all();
        } elseif (Gate::allows('isOrganizer')) {
            $events = Event::where('organizer_id', Auth::id())->get();
        } else {
            abort(404);
        }

        return view('seats.create', compact('seat_types', 'events'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (Gate::allows('isAdmin')) {
            $validData = $request->validate([
                'quantity' => 'required|integer|min:1',
                'unit_price' => 'required|min:1',
                'event_id' => ['required', 'exists:events,id', Rule::unique('seats')->where(function ($query) use ($request) {
                    $query->where('seat_type_id', $request->get('seat_type_id'))
                            ->whereNull('deleted_at');
                })],
                'seat_type_id' => ['required', 'exists:seat_types,id', Rule::unique('seats')->where(function ($query) use ($request) {
                    $query->where('event_id', $request->get('event_id'))
                        ->whereNull('deleted_at');
                })],
            ]);
        } elseif (Gate::allows('isOrganizer')) {
            $validData = $request->validate([
                'quantity' => 'required|integer|min:1',
                'unit_price' => 'required|min:1',
                'event_id' => ['required', Rule::exists('events', 'id')->where(function ($query) use ($request) {
                    $query->where('organizer_id', Auth::id());
                }),  Rule::unique('seats')->where(function ($query) use ($request) {
                    $query->where('seat_type_id', $request->get('seat_type_id'))
                        ->whereNull('deleted_at');
                })],
                'seat_type_id' => ['required', 'exists:seat_types,id', Rule::unique('seats')->where(function ($query) use ($request) {
                    $query->where('event_id', $request->get('event_id'))
                        ->whereNull('deleted_at');
                })]
            ]);
        } else {
            abort(404);
        }

        $seat = Seat::create($validData);
        self::recalculateNAvailTickets(Event::findOrFail($validData['event_id']));
        $event = Event::whereId($validData['event_id'])->first();
        $event->n_ticket += $validData['quantity'];
        $event->save();
        return redirect(route('seats.index'))->with('success', 'Posto '. $seat->id .' aggiunto!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Seat  $seat
     * @return \Illuminate\Http\Response
     */
    public function show(Seat $seat)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Seat  $seat
     * @return \Illuminate\Http\Response
     */
    public function edit(Seat $seat)
    {
        $seat_types = SeatType::all();
        if (Gate::allows('isAdmin')) {
            $events = Event::all();
        } elseif (Gate::allows('isOrganizer')) {
            $events = Event::where('organizer_id', Auth::id())->get();
        } else {
            abort(404);
        }

        return view('seats.edit', compact('seat', 'seat_types', 'events'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Seat  $seat
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Seat $seat)
    {
        if (Gate::allows('isAdmin')) {
            $validData = $request->validate([
                'quantity' => 'required|integer|min:1',
                'unit_price' => 'required|min:1',
                'event_id' => ['required', 'exists:events,id', Rule::unique('seats', 'event_id')->where(function ($query) use ($request) {
                    $query->where('seat_type_id', $request->get('seat_type_id'))
                        ->whereNull('deleted_at');
                })->ignore($seat)],
                'seat_type_id' => ['required', 'exists:seat_types,id', Rule::unique('seats', 'seat_type_id')->where(function ($query) use ($request) {
                    $query->where('event_id', $request->get('event_id'))
                        ->whereNull('deleted_at');
                })->ignore($seat)]
            ]);
        } elseif (Gate::allows('isOrganizer')) {
            $validData = $request->validate([
                'quantity' => 'required|integer|min:1',
                'unit_price' => 'required|min:1',
                'event_id' => ['required', Rule::exists('events', 'id')->where(function ($query) use ($request) {
                    $query->where('organizer_id', Auth::id());
                }),  Rule::unique('seats', 'event_id')->where(function ($query) use ($request) {
                    $query->where('seat_type_id', $request->get('seat_type_id'))
                        ->whereNull('deleted_at');
                })->ignore($seat)],
                'seat_type_id' => ['required', 'exists:seat_types,id', Rule::unique('seats', 'seat_type_id')->where(function ($query) use ($request) {
                    $query->where('event_id', $request->get('event_id'))
                        ->whereNull('deleted_at');
                })->ignore($seat)]
            ]);
        } else {
            abort(404);
        }

        /*
         * Check if user is asking to update seats quantity setting its value
         * less than tickets sold for that seat.
         */
        $sold_tickets = Ticket::whereNotNull('order_id')
                ->where('event_id', $seat->event->id)
                ->where('seat_type_id', $seat->seat_type->id)
                ->count();

        if ($request['quantity'] < $sold_tickets) {
            return redirect(route('seats.index'))->withErrors('Non puoi inserire meno posti di quanti ne sono stati acquistati.');
        }

        $seat->update($validData);
        self::recalculateNAvailTickets(Event::findOrFail($validData['event_id']));
        $event = Event::whereId($validData['event_id'])->first();
        $event->n_ticket += abs($validData['quantity'] - $seat->quantity);
        $event->save();
        return redirect(route('seats.index'))->with('success', 'Posto '. $seat->id .' aggiornato!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Seat  $seat
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Seat $seat)
    {
        if (Gate::allows('isAdmin') || Gate::allows('isOrganizer')) {
            $seat->delete();

            self::recalculateNAvailTickets(Event::findOrFail($seat->event_id));
            $event = Event::whereId($seat->event_id)->first();
            $event->n_ticket -= $seat->quantity;
            $event->save();

            return redirect((route('seats.index')))->with('success', 'Posto eliminato!');
        }

        abort(404);
    }

    public static function recalculateNAvailTickets(Event $event)
    {
        $event->n_ticket_aval = self::getTicketsQuantityForEvent($event);
        $event->save();
    }

    public static function getTicketsQuantityForEvent(Event $event)
    {
        return DB::table('seats')
            ->where('event_id', $event->id)
            ->whereNull('deleted_at')
            ->sum('quantity');
    }
}
