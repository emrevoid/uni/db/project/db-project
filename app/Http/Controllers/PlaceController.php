<?php

namespace App\Http\Controllers;

use App\Event;
use App\Place;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $places = Place::all();

        return view('places.index', compact('places'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        if (Gate::allows('isAdmin')) {
            return view('places.create');
        }

        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (Gate::allows('isAdmin')) {
            $new_place = Place::create([
                'name' => $request['name'],
                'address' => $request['address'],
                'description' => $request['description'],
                'phoneNum' => $request['phoneNum'],
            ]);

            $new_place->save();

            return redirect('/places')->with('success', "Luogo creato con successo");
        }

        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $place = Place::findOrFail($id);
        $events = Event::all()->where('places_id', $place->id);

        return view('places.show', compact('place', 'events'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $place = Place::findOrFail($id);

        if (Gate::allows('isAdmin')) {
            return view('places.edit', compact('place'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $place = Place::findOrFail($id);

        if (Gate::allows('isAdmin')) {
            $place_data = $request->validate([
                'name' => $request['name'],
                'address' => $request['address'],
                'description' => $request['description'],
                'phoneNum' => $request['phoneNum'],
            ]);
            Place::whereId($place->id)->update($place_data);

            return redirect('/places'.$id.'/edit')->with('success', "Luogo aggiornato correttamente");
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $place = Place::findOrFail($id);

        if (Gate::allows('isAdmin')) {
            $place->delete();
            return redirect('/places')->with('success', "Luogo eliminato correttamente");
        }

        abort(404);
    }
}
