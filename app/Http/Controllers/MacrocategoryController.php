<?php

namespace App\Http\Controllers;

use App\Category;
use Carbon\Traits\Macro;
use Illuminate\Http\Request;
use App\Macrocategory;
use Illuminate\Support\Facades\Gate;

/**
 * Class MacrocategoryController
 * @package App\Http\Controllers
 */
class MacrocategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $macrocategories = Macrocategory::all();
        $categories = Category::all();

        if (Gate::allows('isAdmin')) {
            return view('macrocategories.index', compact('macrocategories', 'categories'));
        }

        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        if (Gate::allows('isAdmin')) {
            return view('macrocategories.create');
        }

        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (Gate::allows('isAdmin')) {
            $data = $request->validate([
                'name' => 'required|unique:macrocategories,name|max:255'
            ]);
            $macrocategory = Macrocategory::create($data);

            return redirect('/macrocategories')
                ->with('success', 'Macrocategoria '. $macrocategory->name .' aggiunta con successo!');
        }

        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $macrocategory = Macrocategory::findOrFail($id);

        if (Gate::allows('isAdmin')) {
            return view('macrocategories.edit', compact('macrocategory'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        if (Gate::allows('isAdmin')) {
            $macrocategory = Macrocategory::findOrFail($id);

            $data = $request->validate([
                'name' => 'required|unique:macrocategories,name|max:255'
            ]);

            Macrocategory::whereId($id)->update($data);
            return redirect('/macrocategories')
                ->with('success', 'Macrocategoria ' . $macrocategory->name . ' aggiornata con successo!');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $macrocategory = Macrocategory::findOrFail($id);
        if (Gate::allows('isAdmin')) {
            $macrocategory->delete();

            return redirect('/macrocategories')
                ->with('success', 'Macrocategoria ' . $macrocategory->name . ' eliminata con successo!');
        }

        abort(404);
    }
}
