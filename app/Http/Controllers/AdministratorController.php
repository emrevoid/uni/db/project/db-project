<?php

namespace App\Http\Controllers;

use App\Administrator;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * Class AdministratorController
 * @package App\Http\Controllers
 */
class AdministratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::where('userable_type', 'like', '%Administrator')->get();

        if (Gate::allows('isAdmin')) {
            return view('administrators.index', compact('users'));
        }

        abort(404);
    }

    /**
     * @param $id
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        if (Gate::allows('isAdmin')) {
            return view('administrators.register');
        }

        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (Gate::allows('isAdmin')) {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'surname' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);

            $new_user = User::create([
                'name' => $request['name'],
                'surname' => $request['surname'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
            ]);

            $new_admin = Administrator::create();

            $new_admin->user()->save($new_user);
            $new_user->userable()->associate($new_admin)->save();

            return redirect('/administrators')->with('success', 'Amministratore creato con successo');
        }

        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $current_user = User::findOrFail($id);
        $admin = Administrator::findOrFail($current_user->userable_id);

        if (Gate::allows('isAdminOwner', $current_user) || Gate::allows('isAdmin')) {
            return view('administrators.edit', compact('admin', 'current_user'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $current_user = User::findOrFail($id);

        if (Gate::allows('isAdminOwner', $current_user) || Gate::allows('isAdmin')) {
            $user_data = $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'surname' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            ]);
            User::whereId($id)->update($user_data);

            return redirect('/administrators/'.$id.'/edit')
                ->with('success', 'Il profilo è stato aggiornato con successo.');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $admin = Administrator::findOrFail($user->userable_id);

        if (Gate::allows('isAdminOwner', $user) || Gate::allows('isAdmin')) {
            $admin->delete();
            $user->delete();
            return redirect(route('administrators.index'))->with('success', 'Amministratore cancellato con successo.');
        }

        abort(404);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function panel()
    {
        if (Gate::allows('isAdmin')) {
            return view('administrators.panel');
        }

        abort(404);
    }
}
