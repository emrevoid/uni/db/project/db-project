<?php

namespace App\Http\Controllers;

use App\Event;
use App\Favorite;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class FavoriteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function index()
    {
        $current_user = User::findOrFail(Auth::id());

        $favorites = Favorite::where(function ($query) use ($current_user) {
            $query->where('customer_id', $current_user->id);
        })->get();

        return $favorites;
    }

    /**
     * @param $event_id
     * @param $user_id
     * @return bool
     */
    private static function event_bought($event_id, $user_id)
    {
        $orders = Order::where('customer_id', $user_id)->get();
        foreach ($orders as $order) {
            $tickets = $order->tickets;
            foreach ($tickets as $ticket) {
                if ($ticket->event_id === $event_id) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (Gate::allows('isCustomer')) {
            $current_user = User::findOrFail(Auth::id());
            $validated_data = $request->validate([
                'event_id' => 'required'
            ]);

            $event = Event::findOrFail($validated_data['event_id']);

            $favorite_exists = Favorite::where(function ($query) use ($current_user, $event) {
                $query->where('customer_id', $current_user->id)
                    ->where('event_id', $event->id);
            })->get();

            if (!$favorite_exists->isEmpty()) {
                return redirect('/myevents')->with('error', 'Evento ' .
                    $event->name . ' già presente nella lista dei preferiti.');
            }

            if ($this->event_bought($event->id, $current_user->id)) {
                return redirect('/myevents')->with('error', 'Evento ' .
                    $event->name . ' già presente fra i tuoi acquisti.');
            }

            Favorite::create(['event_id' => $event->id, 'customer_id' => $current_user->id]);
            return redirect('/myevents')->with('success', 'Evento ' . $event->name . ' aggiunto ai preferiti!');
        }

        abort(404);
    }

    /**
     * @param $event_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function add($event_id)
    {
        if (Gate::allows('isCustomer')) {
            $current_user = User::findOrFail(Auth::id());
            $event = Event::findOrFail($event_id);

            $favorite_exists = Favorite::where(function ($query) use ($current_user, $event) {
                $query->where('customer_id', $current_user->id)
                    ->where('event_id', $event->id);
            })->get();

            if (!$favorite_exists->isEmpty()) {
                return response('err_already_exists', 400);
            }

            if ($this->event_bought($event->id, $current_user->id)) {
                return response('err_already_bought', 400);
            }

            Favorite::create(['event_id' => $event->id, 'customer_id' => $current_user->id]);

            return response('success', 200);
        }

        abort(404);
    }

    /**
     * @param $event_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function remove($event_id)
    {
        if (Gate::allows('isCustomer')) {
            $current_user = User::findOrFail(Auth::id());

            $event = Event::findOrFail($event_id);

            $favorite_exists = Favorite::where(function ($query) use ($current_user, $event) {
                $query->where('customer_id', $current_user->id)
                    ->where('event_id', $event->id);
            })->get();
            if ($favorite_exists->isEmpty()) {
                return response('err_not_exists', 400);
            }

            $favorite_exists[0]->delete();

            return response('success', 200);
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $favorite = Favorite::findOrFail($id);
        $user_to_check = User::findOrFail($favorite->customer_id);

        if (Gate::allows('isCustomerOwner', $user_to_check)) {
            $favorite->delete();

            return redirect('/myevents')->with('success', 'Evento rimosso dai preferiti!');
        }

        abort(404);
    }
}
