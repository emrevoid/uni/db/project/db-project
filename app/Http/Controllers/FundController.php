<?php

namespace App\Http\Controllers;

use App\Event;
use App\Fund;
use App\Sponsor;
use App\SponsorRange;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class FundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Gate::allows('isAdmin')) {
            $funds = Fund::all();
            return view('funds.index', compact('funds'));
        }

        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        if (Gate::allows('isAdmin')) {
            $sponsors = \App\Sponsor::all();
            $events = \App\Event::all();
            return view('funds.create', compact('sponsors', 'events'));
        }

        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (Gate::allows('isAdmin')) {
            /**
             * TODO check if drops error on duplicates
             */
            Validator::make($request->all(), [
                'event_id' => 'required|exists:events,id',
                'sponsor_id' => [ 'required', 'exists:sponsors,id',
                    Rule::unique('funds')->where(function ($query) use ($request) {
                        return $query->where('event_id', $request->get('event_id'));
                    })],
                'amount' => 'required|numeric|min:1'
            ])->validate();

            Fund::create($request->all());

            /**
             * TODO
             * - identify sponsor range
             * - recalculate sponsor range for all sponsor for that event
             * - incapsulate code in function which will be called on edit and on destroy
             */
            static::recalculateFunds($request->get('event_id'));

            return redirect(route('funds.index'))->with('success', 'Donazione aggiunta!');
        }

        abort(404);
    }

    /**
     * Calculate sponsor ranges for all funds for a certain event.
     * @param $event_id
     */
    public static function recalculateFunds($event_id)
    {
        $total_funds = Fund::where('event_id', $event_id)
                            ->sum('amount');
        $funds = Fund::where('event_id', $event_id)->get();

        foreach ($funds as $fund) {
            $percentage_amount = 0;

            if ($total_funds !== 0) {
                $percentage_amount = round($fund->amount / $total_funds, 2);
            }

            // Identify sponsor range
            $max_range = SponsorRange::orderBy('amount', 'desc')->first();
            $ranges = SponsorRange::orderBy('amount', 'asc')->get();

            /**
             * Set the maximum range, then iterate all ranges
             * from bottom: if a range is less than the current
             * range amount, set it and exit from loop.
             */
            if (isset($max_range)) {
                $fund->sponsor_range_id = $max_range->id;
                foreach ($ranges as $range) {
                    if ($percentage_amount < $range->amount) {
                        $fund->sponsor_range_id = $range->id;
                        break;
                    }
                }
            }

            $fund->save();
        }
    }

    public static function recalculateAllFunds()
    {
        $events = Event::all();
        foreach ($events as $event) {
            static::recalculateFunds($event->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fund  $fund
     * @return \Illuminate\Http\Response
     */
    public function show(Fund $fund)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fund  $fund
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Fund $fund)
    {
        if (Gate::allows('isAdmin')) {
            $events = Event::all();
            $sponsors = Sponsor::all();
            return view('funds.edit', compact('fund', 'events', 'sponsors'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fund  $fund
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Fund $fund)
    {
        if (Gate::allows('isAdmin')) {
            $validData = $request->validate([
                'event_id' => ['required', 'exists:events,id',
                    Rule::unique('funds', 'event_id')->where(function ($query) use ($request) {
                        return $query->where('sponsor_id', $request->get('sponsor_id'));
                    })->ignore($fund)],
                'sponsor_id' => [ 'required', 'exists:sponsors,id',
                    Rule::unique('funds', 'sponsor_id')->where(function ($query) use ($request) {
                        return $query->where('event_id', $request->get('event_id'));
                    })->ignore($fund)],
                'amount' => 'required|numeric|min:1'
            ]);

            /*
             * TODO
             * ignore $this->request->get('event_id');
             * Rule::unique('funds')->where(function ($query) use ($fund) {
                        return $query->where('event_id', $fund->event_id);
                    })],
             */

            Fund::whereId($fund->id)->update($validData);

            static::recalculateFunds($validData['event_id']);
            static::recalculateFunds($fund->event->id);

            return redirect(route('funds.index'))
                ->with('success', 'Donazione aggiornata!');
        }

        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fund  $fund
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Fund $fund)
    {
        if (Gate::allows('isAdmin')) {
            Fund::whereId($fund->id)->delete();
            static::recalculateFunds($fund->event->id);

            return redirect(route('funds.index'))
                ->with('success', 'Donazione ' . $fund->name . ' rimossa.');
        }

        abort(404);
    }
}
