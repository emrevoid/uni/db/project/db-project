<?php

namespace App;

use Ds\Set;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Event extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'description', 'image',
        'n_ticket', 'n_ticket_aval', 'price',
        'start_date', 'end_date', 'start_time', 'end_time',
        'category_id', 'organizer_id', 'place_id'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function organizer()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function partecipates()
    {
        return $this->hasMany('App\Partecipate');
    }

    public function place()
    {
        return $this->belongsTo('App\Place');
    }

    public function image()
    {
        return $this->image;
    }

    public function funds()
    {
        return $this->hasMany(\App\Fund::class);
    }

    public function seats()
    {
        return $this->hasMany(\App\Seat::class);
    }

    public function getPaidOrdersUsersAttribute()
    {
        $tickets = Ticket::where('event_id', $this->id)->get();
        $customer_ids = new Set();
        foreach ($tickets as $ticket) {
            if ($ticket->order->paid) {
                $customer_ids->add($ticket->order->user->id);
            }
        }

        return User::find($customer_ids->toArray());
    }

    public function getFavoritesUsersAttribute()
    {
        $users = DB::table('users')
            ->join('favorites', 'users.id', '=', 'favorites.customer_id')
            ->where('favorites.event_id', $this->id)
            ->select('users.id')
            ->get();
        $usersIds = [];
        foreach ($users as $user) {
            $usersIds[] = $user->id;
        }

        return User::find($usersIds);
    }

    public function getEffectivelySoldTicketNumberAttribute()
    {
        $sold = DB::table('tickets')
            ->join('orders', 'orders.id', '=', 'tickets.order_id')
            ->where('orders.paid', true)
            ->where('tickets.event_id', $this->id)
            ->sum('tickets.quantity');
        return (int)$sold;
    }
}
