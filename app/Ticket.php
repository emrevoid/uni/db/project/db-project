<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = ['event_id', 'order_id', 'quantity', 'ticket_rate_id', 'seat_type_id'];

    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function ticket_rate()
    {
        return $this->belongsTo(\App\TicketRate::class);
    }

    public function seat_type()
    {
        return $this->belongsTo(\App\SeatType::class);
    }
}
