# Developing guidelines

## Stack

- Laradock, it runs in containers:
  - Laravel 6
  - MySQL 5.7 (not 8 because of [that](https://dev.to/highcenburg/php-artisan-migrate-3l3l))
- Testing and CI
- Deployment (CD)

## Prerequisites

- docker
- docker-compose

## Folder structure

```
starting-folder/
├── laradock/
│   └── ...
└── db-project/
    └── ...
```

## Laradock

1. Clone this repo and `git checkout develop`
2. Clone laradock:
   ```bash
   $ git clone -n https://github.com/laradock/laradock.git \
     && cd laradock \
     && git checkout 4f7ab34a6bbe45f0da8d0cd5d12ae62609a5f028
   ```
3. Then:
   ```
   cp env-example .env
   sed -i "s/APP_CODE_PATH_HOST=..\//APP_CODE_PATH_HOST=..\/db-project\//" .env
   sed -i "s/COMPOSE_PROJECT_NAME=laradock/COMPOSE_PROJECT_NAME=eventnation_db/" .env
   sed -i "s/WORKSPACE_INSTALL_LARAVEL_INSTALLER=false/WORKSPACE_INSTALL_LARAVEL_INSTALLER=true/" .env
   sed -i "s/WORKSPACE_INSTALL_YARN=false/WORKSPACE_INSTALL_YARN=true/" .env
   sed -i "s/MYSQL_VERSION=latest/MYSQL_VERSION=5.7/" .env
   ```
4. Start docker (`systemctl start docker`)
5. `docker-compose up -d nginx mysql` (takes some time!)
6. `docker-compose exec -u laradock workspace bash`
7. `composer install`
8. Get a working `.env` file in the project repo
   - if you want to work with a local DB use `DB_HOST=mysql` or the local mysql docker container docker IP (`docker inspect -f '{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq)`, [source](https://laracasts.com/discuss/channels/laravel/dock-laravel-sqlstatehy000-2002-connection-refused?page=1#reply=517024)
9. Restart the stack (`docker-compose restart`)
10. Go to `http://localhost`

## Upgrading stack

See https://gitlab.com/emrevoid/uni/web/web-project/issues/5.

## IDE

PHPStorm with following plugins:
- Laravel
- PHP Code Sniffer
- PHP Inspections (EA Extended)
