@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="card-panel red lighten-1">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif
    <div class="row">
        <div class="col s12 m4 l3"></div>
        <div class="col s12 m4 l6">
            <h1>{{ $guest->name }}</h1>
            <div class="card hoverable">
                <div class="card-content">
                    <p><i class="tiny material-icons">message</i>{{ $guest->description }}</p>
                </div>
            </div>
            <h4>Lista delle Partecipazioni</h4>
            <table class="striped responsive-table">
                <thead>
                <tr>
                    <th>Data</th>
                    <th>Evento</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                @foreach ($partecipates as $partecipate)
                    <tr>
                        <td><a href="/events/{{ $partecipate->event->id }}" aria-label="Link all'evento {{$partecipate->event->name}}">{{ $partecipate->event->name }}</a></td>
                        <td>{{ $partecipate->event->start_date }}</td>
                        @can('isAdmin', auth()->user())
                        <td><a href="{{ route('partecipates.edit', $partecipate->id) }}"><i class="material-icons">edit</i></a></td>
                        <td><a href="#seat-delete-modal" onclick="delete_seat({{ $partecipate->id }})" class="waves-effect waves-light modal-trigger"><i class="material-icons red-text">delete</i></a></td>
                        @endcan
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="col s12 m4 l3"></div>
    </div>
@endsection
