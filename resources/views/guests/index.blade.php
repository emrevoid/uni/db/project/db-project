@extends('layouts.app')

@section('content')
    <h4>Lista degli Ospiti</h4>
    <table class="striped responsive-table">
        <thead>
        <tr>
            <th>Nome</th>
            <th>Descrizione</th>
            <th></th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        @foreach($guests as $guest)
            <tr>
                <td><a href="/guests/{{ $guest->id }}" aria-label="Link all'ospite {{$guest->name}}">{{ $guest->name }}</a></td>
                <td>{{ $guest->description }}</td>
                @can('isAdmin', auth()->user())
                    <td>
                    <a href="{{ route('guests.edit', $guest->id) }}">
                        <i class="material-icons">edit</i>
                    </a>
                </td>
                <td>
                    <a href="#guest-delete-modal" onclick="delete_guest({{ $guest->id }})" class="waves-effect waves-light modal-trigger">
                        <i class="material-icons red-text">delete</i>
                    </a>
                </td>
                @endcan
            </tr>
        @endforeach
        </tbody>
    </table>
    @can('isAdmin', Auth::user())
        @if(Route::has('guests.create'))
            <div class="fixed-action-btn">
                <a class="btn-floating btn-large red" href="{{ route('guests.create') }}">
                    <i class="large material-icons">add</i>
                </a>
            </div>
        @endif
    @endcan

    <div id="guest-delete-modal" class="modal">
        <form id="guest-delete-form" action="" method="post">
            <div class="modal-content">
                @csrf
                @method('DELETE')
                <h4>Conferma</h4>
                <p>Sei sicuro di voler cancellare questo ospite?</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="btn btn-flat modal-close red">No</a>
                <button type="submit" name="" class="btn btn-flat green" onclick="guest_delete_submit()">Si</button>
            </div>
        </form>
    </div>

    <script>
        function delete_guest(id)
        {
            let url = '{{ route("guests.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#guest-delete-form").attr('action', url);
        }

        function guest_delete_submit()
        {
            $("#guest-delete-form").submit();
        }
    </script>

@endsection
