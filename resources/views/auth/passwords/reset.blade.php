@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col s12 m4 l4"></div>
    <div class="col s12 m4 l4">
        <h1 class="center-align">{{__('Reset Password') }}</h1>
        <div class="card">
                <div class="card-content">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">email</i>
                                <input id="email" type="email" class="validate @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                                <label for="email">{{ __('Indirizzo E-Mail') }}</label>
                                @error('email')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                            <i class="material-icons prefix">lock</i>
                            <input id="password" type="password" class="validate @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            <label for="password">{{ __('Password') }}</label>
                            @error('password')
                            <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                            @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">lock</i>
                                <input id="password" type="password" class="validate @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                <label for="password">{{ __('Password') }}</label>
                                @error('password')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">lock</i>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                <label for="password-confirm">{{ __('Conferma Password') }}</label>
                            </div>
                        </div>

                        <div class="card-action center-align">
                            <button type="submit" class="btn-flat">
                                {{ __('Resetta password') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
