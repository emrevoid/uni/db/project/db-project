@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col s12 m4 l4"></div>
    <div class="col s12 m4 l4">
        <h1 class="center-align">{{__('Reset Password') }}</h1>
        <div class="card">
            <div class="card-content">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="row">

                            <div class="input-field">
                                <i class="material-icons prefix">email</i>
                                <input id="email" type="email" class="validate @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                <label for="email">{{ __('Indirizzo E-Mail') }}</label>
                                @error('email')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>

                            <div class="card-action center-align">
                                <button type="submit" class="btn-flat">
                                    {{ __('Manda il link per il reset della password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
