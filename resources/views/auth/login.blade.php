@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col s12 m4 l4"></div>
        <div class="col s12 m4 l4">
            <h1 class="center-align">{{__('Login') }}</h1>
            <div class="card">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="card-content">
                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">email</i>
                                <input id="email" type="email" class="validate @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                <label for="email">{{ __('Indirizzo E-Mail') }}</label>
                                @error('email')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">lock</i>
                                <input id="password" type="password" class="validate @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                <label for="password">{{ __('Password') }}</label>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                @if (Route::has('password.request'))
                                <div class="right-align">
                                    <a href="{{ route('password.request') }}">
                                        <button type="button" class="btn-flat">
                                            {{ __('Password dimenticata?') }}
                                        </button>
                                    </a>
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <p>
                                <label>
                                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
                                    <span> {{ __('Ricordami') }}</span>
                                </label>
                            </p>
                        </div>
                    </div>
                    <div class="card-action center-align">
                        <button type="submit" class="btn-flat">
                            {{ __('Login') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col s12 m4 l4"></div>
    </div>
@endsection
