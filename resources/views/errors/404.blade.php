@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h3 class="center-align">
                    404 | Pagina non trovata
                </h3>
            </div>
        </div>
    </div>
@endsection

