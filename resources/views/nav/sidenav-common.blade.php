@guest
<ul id="slide-out" class="sidenav">
@else
@if(Auth::user()->userable_type === 'App\Organizer' || Auth::user()->userable_type === 'App\Administrator')
<ul id="slide-out" class="sidenav sidenav-fixed">
@else
<ul id="slide-out" class="sidenav">
@endif
@endguest
    @guest
        <li>
            <a href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
        @if (Route::has('register'))
            <li>
                <a href="{{ route('register') }}">{{ __('Registrati') }}</a>
            </li>
        @endif
    @else
        <li>
            <div class="user-view">
                <div class="background"></div>
                <a href="#user"><i class="large material-icons">person</i></a>
                <a href="#name">
                    <span class="white-text name">{{ Auth::user()->name }}</span>
                </a>
                <a href="#email"><span class="white-text email">{{ Auth::user()->email }}</span></a>
            </div>
        </li>
    @if(Auth::user()->userable_type === 'App\Customer')
        @include('nav.sidenav-customer')
    @elseif(Auth::user()->userable_type === 'App\Organizer')
        @include('nav.sidenav-organizer')
    @elseif(Auth::user()->userable_type === 'App\Administrator')
        @include('nav.sidenav-administrator')
    @endif
        <li>
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <i class="material-icons">exit_to_app</i> {{ __('Logout') }}
            </a>
        </li>
    @endguest
</ul>
