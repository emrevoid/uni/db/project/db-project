<div class="navbar-fixed">
    <nav>
        <div class="nav-wrapper container">
            <a href="#" data-target="slide-out" class="left sidenav-trigger hide-on-large-only"><i class="material-icons">menu</i></a>
            <a class="brand-logo" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>

            <ul class="right">
                @guest
                    <li class="hide-on-med-and-down">
                        <a href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="hide-on-med-and-down">
                            <a href="{{ route('register') }}">{{ __('Registrati') }}</a>
                        </li>
                    @endif
                @else
                    <li>
                        <a class='dropdown-trigger' href='#' data-target='dropdown-notification'><i class="material-icons">notifications</i></a>
                    </li>
                    @if(Auth::user()->userable_type === 'App\Customer')
                    <li class="hide-on-med-and-down">
                        <a href="{{ route('cart.index') }}"><i class="material-icons">shopping_cart</i></a>
                    </li>
                    <li class="hide-on-med-and-down">
                        <a class="dropdown-trigger" href="#" data-target="dropdown-user">
                            <i class="material-icons left">person</i>
                            {{ Auth::user()->name }}
                            <i class="material-icons right">arrow_drop_down</i>
                        </a>
                    </li>
                    @endif
                @endguest
            </ul>
        </div>
    </nav>
</div>
