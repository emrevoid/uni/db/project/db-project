<li>
    <a href="{{ route('customers.edit', Auth::user(), Auth::user()->userable_id) }}">
        <i class="material-icons">person</i>Profilo
    </a>
</li>
<li>
    <a href="/myevents"><i class="material-icons">confirmation_number</i> Miei eventi</a>
</li>
<li>
    <a href="{{ route('cart.index') }}"><i class="material-icons">shopping_cart</i> Carrello</a>
</li>
<li>
    <a href="{{ route('orders.index') }}"><i class="material-icons">receipt</i> Ordini</a>
</li>
