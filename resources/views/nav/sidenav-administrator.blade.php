<li>
    <a href="{{ route('administrators.edit', Auth::user(), Auth::user()->userable_id) }}">
        <i class="material-icons">person</i>Profilo
    </a>
</li>
<li class="no-padding">
    <ul class="collapsible collapsible-accordion">
        <li>
            <a class="collapsible-header" style="padding: 0px 32px;"><i class="material-icons">people</i>Utenti <i class="material-icons right">arrow_drop_down</i> </a>
            <div class="collapsible-body">
                <ul >
                    <li><a href="{{ route('customers.index') }}"><i class="material-icons">person</i> Clienti</a></li>
                    <li><a href="{{ route('organizers.index') }}"><i class="material-icons">business_center</i> Organizzatori</a></li>
                    <li><a href="{{ route('administrators.index') }}"><i class="material-icons">assignment_ind</i> Amministratori</a></li>
                    <li><div class="divider"></div></li>
                </ul>
            </div>
        </li>
    </ul>
</li>

<li>
    <a href="{{ route('macrocategories.index') }}"><i class="material-icons">folder</i> Categorie</a>
</li>
<li>
    <a href="{{ route('events.index') }}"><i class="material-icons">event</i> Eventi</a>
</li>
<li>
    <a href="{{ route('seat_types.index') }}"><i class="material-icons">event_seat</i> Tipi posti</a>
</li>
@include('nav.menu-items.seats')
<li>
    <a href="{{ route('orders.index') }}"><i class="material-icons">receipt</i> Ordini</a>
</li>
<li class="no-padding">
    <ul class="collapsible collapsible-accordion">
        <li>
            <a class="collapsible-header" style="padding: 0px 32px;"><i class="material-icons">people</i>Sponsorizzazioni <i class="material-icons right">arrow_drop_down</i> </a>
            <div class="collapsible-body">
                <ul>
                    <li><a href="{{ route('sponsors.index') }}"><i class="material-icons">business</i> Sponsors</a></li>
                    <li><a href="{{ route('sponsor_ranges.index') }}"><i class="material-icons">equalizer</i> Fasce sponsor</a></li>
                    <li><a href="{{ route('funds.index') }}"><i class="material-icons">card_giftcard</i> Donazioni</a></li>
                    <li><div class="divider"></div></li>
                </ul>
            </div>
        </li>
    </ul>
</li>
@include('nav.menu-items.guests')
<li>
    <a href="{{ route('places.index') }}"><i class="material-icons">place</i> Luoghi</a>
<li>
    <a href="{{ route('ticket_rate.index') }}"><i class="material-icons">attach_money</i> Tariffe</a>
</li>
