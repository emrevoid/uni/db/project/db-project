<li>
    <a href="{{ route('organizers.edit', Auth::user(), Auth::user()->id) }}">
        <i class="material-icons">person</i>Profilo
    </a>
</li>
<li><a href="{{ route('events.index') }}"><i class="material-icons">confirmation_number</i>Miei eventi</a></li>
@include('nav.menu-items.guests')
@include('nav.menu-items.seats')
