<li class="no-padding">
    <ul class="collapsible collapsible-accordion">
        <li>
            <a class="collapsible-header" style="padding: 0px 32px;"><i class="material-icons">record_voice_over</i>Ospiti<i class="material-icons right">arrow_drop_down</i> </a>
            <div class="collapsible-body">
                <ul>
                    <li><a href="{{ route('guests.index') }}"><i class="material-icons">people</i>Lista Ospiti</a></li>
                    <li><a href="{{ route('partecipates.index') }}"><i class="material-icons">person_pin</i> Partecipazioni</a></li>
                    <li><div class="divider"></div></li>
                </ul>
            </div>
        </li>
    </ul>
</li>
