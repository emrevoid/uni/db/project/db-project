@extends('layouts.app')

@section('content')
@canany(['isAdmin','isCustomerOwner'], auth()->user())

    <h4>Ordine numero {{$order->id}}</h4>
    @can('isAdmin', auth()->user())
    <div class="row">
        Eseguito da:
        <a href="{{ route('customers.edit', $order->user->id) }}">{{ $order->user->email }}</a>
    </div>
    @endcan
    <div class="row">
        @foreach( $order->tickets as $ticket )
            @include('layouts.event-card.order', compact($ticket, $total_price))
        @endforeach
    </div>
    <h5 class="right">Prezzo totale: {{ number_format($total_price, 2) }} €</h5>
@endcanany
@endsection
