@extends('layouts.app')

@section('content')
@can('isCustomerOwner', auth()->user())
    <ul class="collection with-header">
        <li class="collection-header"><h4>I tuoi ordini</h4></li>
        @foreach($orders as $order)
            <li class="collection-item">
                <div>
                    {{ $order->created_at }}
                    <a href="{{route('orders.show', $order->id)}}" class="secondary-content">
                        <i class="material-icons">info</i>
                    </a>
                    @if($order->paid === false)
                        <a href="{{route('orders.cart')}}" class="secondary-content">
                            <i class="material-icons">payment</i>
                        </a>
                    @endif
                </div>
            </li>
        @endforeach
    </ul>
@endcan
@endsection
