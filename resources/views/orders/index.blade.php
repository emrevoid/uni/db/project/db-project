@extends('layouts.app')

@section('content')
@can('isAdmin', auth()->user())
    <ul class="collection with-header">
        <li class="collection-header"><h4>Ordini</h4></li>
        @foreach($orders as $order)
            <li class="collection-item">
                <div>
                    {{ $order->id }}
                    <a href="#order-delete-modal" onclick="delete_order({{$order->id}})" class="waves-effect waves-light modal-trigger secondary-content">
                        <i class="material-icons red-text">delete</i>
                    </a>
                    <a href="{{route('orders.show', $order->id)}}" class="secondary-content">
                        <i class="material-icons">info</i>
                    </a>
                </div>
            </li>
        @endforeach
    </ul>

    @if(Route::has('orders.create'))
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href="{{ route('orders.create') }}">
                <i class="large material-icons">add</i>
            </a>
        </div>
    @endif

    <div id="order-delete-modal" class="modal">
        <form id="order-delete-form" action="" method="post">
            <div class="modal-content">
                @csrf
                @method('DELETE')
                <h4>Conferma</h4>
                <p>Sei sicuro di voler cancellare l'ordine?</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="btn btn-flat modal-close red">No</a>
                <button type="submit" name="" class="btn btn-flat green" onclick="order_delete_submit()">Si</button>
            </div>
        </form>
    </div>

    <script>
        function delete_order(id)
        {
            let url = '{{ route("orders.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#order-delete-form").attr('action', url);
        }

        function order_delete_submit()
        {
            $("#order-delete-form").submit();
        }
    </script>
@endcan
@endsection
