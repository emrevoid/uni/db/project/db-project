@extends('layouts.app')

@section('content')
    <h4>Categorie più redditizie</h4>
    <table class="striped responsive-table">
        <thead>
        <tr>
            <th>Macrocategoria</th>
            <th>Categoria</th>
            <th>Incasso Totale</th>
        </tr>
        </thead>

        <tbody>
        @foreach($category_ranking as $cr)
            <tr>
                <td>{{ $cr->macrocat }}</td>
                <td>{{ $cr->subcat }}</td>
                <td>{{ $cr->total_profit .' €' }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
