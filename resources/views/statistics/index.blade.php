@extends('layouts.app')

@section('content')
    <h4>Statistiche</h4>
{{--    <table class="striped responsive-table">--}}
{{--        <thead>--}}
{{--        <tr>--}}
{{--            <th>Dato</th>--}}
{{--            <th>Risultato</th>--}}
{{--        </tr>--}}
{{--        </thead>--}}

{{--        <tbody>--}}
{{--            <tr>--}}
{{--                <td>{{ 'Guadagno Totale' }}</td>--}}
{{--                <td>{{ $profit_all->total_profit }}</td>--}}
{{--            </tr>--}}
{{--        </tbody>--}}
{{--    </table>--}}
    <ul class="collection with-header">
        <li class="collection-item"><div><a href="{{ route('statistics.category-age-average') }}">{{ 'Età Media per Categoria' }}</a></div></li>
        <li class="collection-item"><div><a href="{{ route('statistics.category-ranking') }}">{{ 'Categorie più redditizie' }}</a></div></li>
        <li class="collection-item"><div><a href="{{ route('statistics.customer-spending-average') }}">{{ 'Spesa media per Cliente' }}</a></div></li>
        <li class="collection-item"><div><a href="{{ route('statistics.event-profit') }}">{{ 'Profitto per Evento' }}</a></div></li>
        <li class="collection-item"><div><a href="{{ route('statistics.place-age-average') }}">{{ 'Età Media per Luogo' }}</a></div></li>
        <li class="collection-item"><div><a href="{{ route('statistics.place-profit') }}">{{ 'Profitto per Luogo' }}</a></div></li>
        <li class="collection-item"><div><a href="{{ route('statistics.sponsor-ranking') }}">{{ 'Classifica Sponsor' }}</a></div></li>
    </ul>
@endsection
