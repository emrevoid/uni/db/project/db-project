@extends('layouts.app')

@section('content')
    <h4>Spesa Media per Cliente</h4>
    <table class="striped responsive-table">
        <thead>
        <tr>
            <th>Cliente</th>
            <th>Spesa Media</th>
        </tr>
        </thead>

        <tbody>
        @foreach($customer_spending_average as $csa)
            <tr>
                <td>{{ $csa->u_email }}</td>
                <td>{{ $csa->f_price .' €'}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
