@extends('layouts.app')

@section('content')
    <h4>Età Media per Categoria</h4>
    <table class="striped responsive-table">
        <thead>
        <tr>
            <th>Macrocategoria</th>
            <th>Categoria</th>
            <th>Età Media</th>
        </tr>
        </thead>

        <tbody>
        @foreach($category_age_average as $caa)
            <tr>
                <td>{{ $caa->macrocat }}</td>
                <td>{{ $caa->subcat }}</td>
                <td>{{ $caa->age }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
