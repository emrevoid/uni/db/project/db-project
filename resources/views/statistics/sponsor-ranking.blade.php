@extends('layouts.app')

@section('content')
    <h4>Classifica Sponsor</h4>
    <table class="striped responsive-table">
        <thead>
        <tr>
            <th>Nome Sponsor</th>
            <th>Donazioni totali</th>
        </tr>
        </thead>

        <tbody>
        @foreach($sponsor_ranking as $sr)
            <tr>
                <td>{{ $sr->spo_name }}</td>
                <td>{{ $sr->total_funds .' €'}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
