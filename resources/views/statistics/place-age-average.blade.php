@extends('layouts.app')

@section('content')
    <h4>Età Media per Luogo</h4>
    <table class="striped responsive-table">
        <thead>
        <tr>
            <th>Luogo</th>
            <th>Età Media</th>
        </tr>
        </thead>

        <tbody>
        @foreach($place_age_average as $paa)
            <tr>
                <td>{{ $paa->p_name }}</td>
                <td>{{ $paa->age }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
