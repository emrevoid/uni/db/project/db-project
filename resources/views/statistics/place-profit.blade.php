@extends('layouts.app')

@section('content')
    <h4>Guadagni per Luogo</h4>
    <table class="striped responsive-table">
        <thead>
        <tr>
            <th>Luogo</th>
            <th>Guadagno totale</th>
        </tr>
        </thead>

        <tbody>
        @foreach($place_profit as $pp)
            <tr>
                <td>{{ $pp->p_name }}</td>
                <td>{{ $pp->f_price .' €'}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
