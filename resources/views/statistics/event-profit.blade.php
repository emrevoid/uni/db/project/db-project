@extends('layouts.app')

@section('content')
    <h4>Guadagni per Evento</h4>
    <table class="striped responsive-table">
        <thead>
        <tr>
            <th>Evento</th>
            <th>Guadagno</th>
        </tr>
        </thead>

        <tbody>
        @foreach($event_profit as $ep)
            <tr>
                <td>{{ $ep->e_name }}</td>
                <td>{{ $ep->f_price .' €'}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
