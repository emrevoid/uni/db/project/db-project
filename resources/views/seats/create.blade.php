@extends('layouts.app')

@section('content')
    @canany(['isAdmin', 'isOrganizer'], auth()->user())
        <div class="row">
            <div class="col s12 m4 l4"></div>
            <div class="col s12 m4 l4">
                <div class="card">
                    <form method="POST" action="{{ route('seats.store') }}">
                        @csrf
                        <div class="card-content">
                            @if ($errors->any())
                                <div class="card-panel red lighten-1">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div><br />
                            @endif
                            <span class="card-title">Crea un posto per un evento</span>
                            <div class="row">
                                <div class="input-field col s12">
                                    <select name="event_id">
                                        <option value="" disabled selected>Scegli un evento</option>
                                        @foreach($events as $event)
                                            <option value="{{ $event->id }}">{{ $event->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <select name="seat_type_id">
                                        <option value="" disabled selected>Scegli un tipo posto</option>
                                        @foreach($seat_types as $seat_type)
                                            <option value="{{ $seat_type->id }}">{{ $seat_type->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="quantity" name="quantity" type="number" min="1" class="validate">
                                    <label for="quantity">Quantità</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="unit_price" name="unit_price" type="number" min="0" class="validate">
                                    <label for="unit_price">Prezzo unitario</label>
                                </div>
                            </div>
                        </div>

                        <div class="card-action">
                            <button class="btn-flat"><a href="{{route('seats.index')}}">Indietro</a></button>
                            <button type="submit" class="btn-flat">Crea posto</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col s12 m4 l4"></div>
        </div>
    @endcan
@endsection
