@extends('layouts.app')

@section('content')
    @canany(['isAdmin', 'isOrganizer'], auth()->user())
        <h4>Posti eventi</h4>
                <table class="striped responsive-table">
                    <thead>
                    <tr>
                        <th>Evento</th>
                        <th>Tipo posto</th>
                        <th>Quantità</th>
                        <th>Prezzo unitario</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($seats as $seat)
                    <tr>
                        <td>{{ $seat->event->name }}</td>
                        <td>{{ $seat->seat_type->name }}</td>
                        <td>{{ $seat->quantity }}</td>
                        <td>{{ $seat->unit_price }}</td>
                        <td>
                            <a href="{{ route('seats.edit', $seat->id) }}">
                                <i class="material-icons">edit</i>
                            </a>
                        </td>
                        <td>
                            <a href="#seat-delete-modal" onclick="delete_seat({{ $seat->id }})" class="waves-effect waves-light modal-trigger">
                                <i class="material-icons red-text">delete</i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
        </ul>

        @if(Route::has('seats.create'))
            <div class="fixed-action-btn">
                <a class="btn-floating btn-large red" href="{{ route('seats.create') }}">
                    <i class="large material-icons">add</i>
                </a>
            </div>
        @endif

        <div id="seat-delete-modal" class="modal">
            <form id="seat-delete-form" action="" method="post">
                <div class="modal-content">
                    @csrf
                    @method('DELETE')
                    <h4>Conferma</h4>
                    <p>Sei sicuro di voler cancellare il posto?</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="btn btn-flat modal-close red">No</a>
                    <button type="submit" name="" class="btn btn-flat green" onclick="seat_delete_submit()">Si</button>
                </div>
            </form>
        </div>

        <script>
            function delete_seat(id)
            {
                let url = '{{ route("seats.destroy", ":id") }}';
                url = url.replace(':id', id);
                $("#seat-delete-form").attr('action', url);
            }

            function seat_delete_submit()
            {
                $("#seat-delete-form").submit();
            }
        </script>
    @endcan
@endsection
