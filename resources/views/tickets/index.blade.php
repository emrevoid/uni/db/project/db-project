@extends('layouts.app')

@section('content')
@can('isAdmin', auth()->user())
    <ul class="collection with-header">
        <li class="collection-header"><h4>Biglietti</h4></li>
        @foreach($tickets as $ticket)
            <li class="collection-item">
                <div>
                    Biglietto {{ $ticket->id }} - Ordine {{ $ticket->order->id }} - Evento: {{ $ticket->event->name }} - Quantità: {{ $ticket->quantity }}

                    <a href="#ticket-delete-modal" onclick="delete_ticket({{$ticket->id}})" class="waves-effect waves-light modal-trigger secondary-content">
                        <i class="material-icons red-text">delete</i>
                    </a>
                    <a href="{{ route('tickets.edit', $ticket->id) }}" class="secondary-content">
                        <i class="material-icons">edit</i>
                    </a>
                </div>
            </li>
        @endforeach
    </ul>

    @if(Route::has('tickets.create'))
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href="{{ route('tickets.create') }}">
                <i class="large material-icons">add</i>
            </a>
        </div>
    @endif

    <div id="ticket-delete-modal" class="modal">
        <form id="ticket-delete-form" action="" method="post">
            <div class="modal-content">
                @csrf
                @method('DELETE')
                <h4>Conferma</h4>
                <p>Sei sicuro di voler cancellare il biglietto?</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="btn btn-flat modal-close red">No</a>
                <button type="submit" name="" class="btn btn-flat green" onclick="ticket_delete_submit()">Si</button>
            </div>
        </form>
    </div>

    <script>
        function delete_ticket(id)
        {
            let url = '{{ route("tickets.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#ticket-delete-form").attr('action', url);
        }

        function ticket_delete_submit()
        {
            $("#ticket-delete-form").submit();
        }
    </script>

@endcan
@endsection
