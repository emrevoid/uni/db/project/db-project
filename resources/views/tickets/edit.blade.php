@extends('layouts.app')

@section('content')
@can('isAdmin', auth()->user())
    <div class="row">
        <div class="col s12 m4 l4"></div>
        <div class="col s12 m4 l4">
            <div class="card">
                <form method="POST" action="{{ route('tickets.update', $ticket->id) }}">
                    @csrf
                    @method('PATCH')
                    <div class="card-content">
                        @if ($errors->any())
                            <div class="card-panel red lighten-1">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        <span class="card-title">Modifica il biglietto {{ $ticket->id }}</span>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="quantity" name="quantity" type="number" placeholder="{{ $ticket->quantity }}" class="validate" min="1">
                                <label for="quantity">Quantità</label>
                            </div>
                        </div>
                    </div>

                    <div class="card-action">
                        <button class="btn-flat"><a href="{{route('tickets.index')}}">Indietro</a></button>
                        <button type="submit" class="btn-flat">Modifica biglietto</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col s12 m4 l4"></div>
    </div>
@endcan
@endsection
