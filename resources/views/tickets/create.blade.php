@extends('layouts.app')

@section('content')
@can('isAdmin', auth()->user())
    <div class="row">
        <div class="col s12 m4 l4"></div>
        <div class="col s12 m4 l4">
            <div class="card">
                <form method="POST" action="{{ route('tickets.store') }}">
                    @csrf
                    <div class="card-content">
                        @if ($errors->any())
                            <div class="card-panel red lighten-1">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        <span class="card-title">Crea un biglietto</span>
                        <div class="row">
                            <div class="input-field col s12">
                                <select name="event_id">
                                    <option value="" disabled selected>Scegli un evento</option>
                                    @foreach($events as $event)
                                        <option value="{{ $event->id }}">{{ $event->name }}</option>
                                    @endforeach
                                </select>
                                <label>Evento</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <select name="order_id">
                                    <option value="" disabled selected>Scegli un ordine</option>
                                    @foreach($orders as $order)
                                        <option value="{{ $order->id }}">{{ $order->id }}</option>
                                    @endforeach
                                </select>
                                <label>Ordine</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="quantity" value="1" name="quantity" type="number" class="validate" min="1">
                                <label for="quantity">Quantità</label>
                            </div>
                        </div>
                    </div>

                    <div class="card-action">
                        <button class="btn-flat"><a href="{{route('tickets.index')}}">Indietro</a></button>
                        <button type="submit" class="btn-flat">Crea biglietto</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col s12 m4 l4"></div>
    </div>
@endcan
@endsection
