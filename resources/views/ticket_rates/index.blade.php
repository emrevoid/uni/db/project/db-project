@extends('layouts.app')

@section('content')
    @can('isAdmin', auth()->user())
        <ul class="collection with-header">
            <li class="collection-header"><h4>Tariffe</h4></li>
            @foreach($ticketRates as $ticketRate)
                <li class="collection-item">
                    <div>
                        {{ $ticketRate->name }} (sconto {{ $ticketRate->amount * 100 }}%)

                        <a href="#rate-delete-modal" onclick="delete_rate({{ $ticketRate->id }})" class="waves-effect waves-light modal-trigger secondary-content">
                            <i class="material-icons red-text">delete</i>
                        </a>
                        <a href="{{ route('ticket_rate.edit', $ticketRate->id) }}" class="secondary-content">
                            <i class="material-icons">edit</i>
                        </a>
                    </div>
                </li>
            @endforeach
        </ul>

        @if(Route::has('ticket_rate.create'))
            <div class="fixed-action-btn">
                <a class="btn-floating btn-large red" href="{{ route('ticket_rate.create') }}">
                    <i class="large material-icons">add</i>
                </a>
            </div>
        @endif

        <div id="rate-delete-modal" class="modal">
            <form id="rate-delete-form" action="" method="post">
                <div class="modal-content">
                    @csrf
                    @method('DELETE')
                    <h4>Conferma</h4>
                    <p>Sei sicuro di voler cancellare la tariffa?</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="btn btn-flat modal-close red">No</a>
                    <button type="submit" name="" class="btn btn-flat green" onclick="rate_delete_submit()">Si</button>
                </div>
            </form>
        </div>

        <script>
            function delete_rate(tr)
            {
                let url = '{{ route("ticket_rate.destroy", ":id") }}';
                url = url.replace(':id', tr);
                $("#rate-delete-form").attr('action', url);
            }

            function rate_delete_submit()
            {
                $("#rate-delete-form").submit();
            }
        </script>
    @endcan
@endsection
