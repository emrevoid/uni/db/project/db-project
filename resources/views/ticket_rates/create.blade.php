@extends('layouts.app')

@section('content')
@can('isAdmin', auth()->user())
    <div class="row">
        <div class="col s12 m4 l4"></div>
        <div class="col s12 m4 l4">
            <div class="card">
                <form method="POST" action="{{ route('ticket_rate.store') }}">
                    @csrf
                    <div class="card-content">
                        @if ($errors->any())
                            <div class="card-panel red lighten-1">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        <span class="card-title">Crea una tariffa</span>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="rate_name" name="name" type="text" class="validate">
                                <label for="rate_name">Nome tariffa</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="rate_amount" name="amount" type="number" min="0.00" max="1.00" step="0.01" class="validate">
                                <label for="rate_amount">Percentuale di sconto (decimale)</label>
                            </div>
                        </div>
                    </div>

                    <div class="card-action">
                        <button class="btn-flat"><a href="{{route('ticket_rate.index')}}">Indietro</a></button>
                        <button type="submit" class="btn-flat">Crea tariffa</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col s12 m4 l4"></div>
    </div>
@endcan
@endsection
