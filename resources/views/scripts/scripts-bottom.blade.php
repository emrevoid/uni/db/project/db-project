<!--Compiled and minified Materialize JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js" integrity="sha256-T6iGJTr91LvmSE0OD+GJ79ArVNS0JClRUKJuTPDr0ac=" crossorigin="anonymous"></script>

@include('scripts.notifications')
@include('scripts.favorites')
@include('scripts.cart')

<script>
    $( document ).ready(function () {
        if ($(window).width() < 992) {
            $(".dropdown-trigger").dropdown({
                hover: false,
                constrainWidth: false,
                coverTrigger: false,
                alignment: 'right'
            });
        } else {
            $(".dropdown-trigger").dropdown({
                hover: true,
                constrainWidth: false,
                coverTrigger: false,
                alignment: 'right'
            });
        }
        $(".sidenav").sidenav();
        $(".fixed-action-btn").floatingActionButton();
        $("select").formSelect();
        $('.collapsible').collapsible({
            accordion: false
        });
        $('.modal').modal();
        $('.tabs').tabs({
            swipeable: true
        });

        if(Laravel.userId) {
            $.get('/notifications', function (data) {
                addNotifications(data, notificationsWrapper);
            });

            window.Echo.private(`App.User.${Laravel.userId}`)
                .notification((notification) => {
                    M.toast({html : 'Hai una nuova notifica!'});
                    addNotifications([notification], notificationsWrapper);
                });
        }
    });
</script>
