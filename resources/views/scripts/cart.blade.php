<script>
    function add_ticket(event_id) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState == 4) {
                switch (xmlHttp.responseText) {
                    case 'err_time':
                        send_toast('L\'evento si è già concluso.', xmlHttp.status);
                        break;
                    case 'err_already_exists':
                        send_toast('Hai già biglietti per questo evento nel carrello.', 200);
                        break;
                    case 'err_n_ticket':
                        send_toast('L\'evento selezionato non ha più biglietti acquistabili.', xmlHttp.status);
                        break;
                    case 'err_max_tickets':
                        send_toast('Hai raggiunto il numero massimo di biglietti<br>acquistabili per questo evento', xmlHttp.status);
                        break;
                    case 'success':
                        send_toast('Biglietto aggiunto al carrello!', xmlHttp.status);
                        break;
                    default:
                        send_toast('Richiesta illegale.', 404);
                        break;
                }
            }
        }
        xmlHttp.open("GET", '/cart/' + event_id + '/add', true);
        xmlHttp.send();
    }
</script>
