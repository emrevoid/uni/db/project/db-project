<script>
    function add_favorite(_this, event_id) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4) {
                switch (xmlHttp.responseText) {
                    case 'err_already_exists':
                        send_toast('L\'evento è già nei tuoi preferiti!', xmlHttp.status);
                        break;
                    case 'err_already_bought':
                        send_toast('Hai già un biglietto per questo evento, non puoi aggiungerlo alla lista dei preferiti.', xmlHttp.status);
                        break;
                    case 'success':
                        send_toast('Evento aggiunto ai preferiti!', xmlHttp.status);
                        _this.getElementsByClassName("material-icons")[0].textContent = "favorite";
                        _this.onclick = function () {
                            remove_favorite(this, event_id);
                        };
                        break;
                    default:
                        send_toast('Errore nella richiesta.', 404);
                        break;
                }
            }
        };
        xmlHttp.open("GET", '/favorites/' + event_id + '/add', true);
        xmlHttp.send();
    }

    function remove_favorite(_this, event_id) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4) {
                switch (xmlHttp.responseText) {
                    case 'err_not_exists':
                        send_toast('L\'evento non è presente fra i tuoi preferiti!', xmlHttp.status);
                        break;
                    case 'success':
                        send_toast('Evento rimosso dai preferiti!', xmlHttp.status);
                        _this.getElementsByClassName("material-icons")[0].textContent = "favorite_border";
                        _this.onclick = function () {
                            add_favorite(this, event_id);
                        };
                        break;
                    default:
                        send_toast('Errore nella richiesta.', 404);
                        break;
                }
            }
        };
        xmlHttp.open("GET", '/favorites/' + event_id + '/remove', true);
        xmlHttp.send();
    }

    function send_toast(text, status) {
        if (status == 200) {
            M.toast({html: text, classes: 'green'});
        } else {
            M.toast({html: text, classes: 'red'});
        }
    }
</script>
