<script>
    let notifications = [];
    const NOTIFICATION_TYPES = {
        eventUpdated: 'App\\Notifications\\EventUpdated',
        soldOut : 'App\\Notifications\\SoldOut'
    };
    let notificationsWrapper   = $('#dropdown-notification');

    function addNotifications(newNotifications, target) {
        notifications = _.concat(newNotifications, notifications);
        // show only last 5 notifications
        notifications.slice(0, 5);
        showNotifications(notifications, target);
    }

    function showNotifications(notifications, target) {
        let text = '<li><a href="#!">Non hai nuove notifiche.</a></li>';
        if(notifications.length) {
            let htmlElements = notifications.map(function (notification) {
                return makeNotification(notification);
            });
            text = htmlElements.join('');
            $(target).addClass('has-notifications')
        } else {
            $(target).removeClass('has-notifications');
        }
        $(target).html(text).promise().done(function () {
            $('.dropdown-trigger').dropdown('recalculateDimensions');
        });
    }

    // Make a single notification string
    function makeNotification(notification) {
        let to = routeNotification(notification);
        let notificationText = makeNotificationText(notification);
        return '<li><a style="height: 4em;" href="' + to + '">' + notificationText + '</a></li>';
    }

    // get the notification route based on it's type
    function routeNotification(notification) {
        let to = '?read=' + notification.id;
        if(notification.type === NOTIFICATION_TYPES.eventUpdated ||
           notification.type === NOTIFICATION_TYPES.soldOut) {
            to = 'events/' + notification.data.event_id + to;
        }
        return '/' + to;
    }

    // get the notification text based on it's type
    function makeNotificationText(notification) {
        let text = '';
        let icon = '<i class="material-icons">priority_high</i>';
        let notificationText = 'Hai una nuova notifica';
        if(notification.type === NOTIFICATION_TYPES.eventUpdated) {
            const event_name = notification.data.event_name;
            icon = '<i class="material-icons">event</i>';
            notificationText = 'L\'evento <span style="font-weight: bold;">' + event_name + '</span> è stato aggiornato.';
        }
        if(notification.type === NOTIFICATION_TYPES.soldOut) {
            const event_name = notification.data.event_name;
            if(notification.data.notifiable.userable_type === 'App\\Customer') {
                icon = '<i class="material-icons">sentiment_very_dissatisfied</i>';
            } else if (notification.data.notifiable.userable_type === 'App\\Organizer') {
                icon = '<i class="material-icons">sentiment_very_satisfied</i>';
            }
            notificationText = 'L\'evento <span style="font-weight: bold;">' + event_name + '</span> è sold out.';
        }
        const date = moment.utc(notification.created_at).fromNow();
        text += icon + notificationText + '<br/><span style="font-size: small;" class="right">' + date + '</span>';
        return text;
    }
</script>
