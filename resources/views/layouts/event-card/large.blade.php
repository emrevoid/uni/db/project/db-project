<div class="card hoverable">
    <a href="/events/{{ $event->id }}" aria-label="Link all'evento {{$event->name}}">
        <div class="card-image" style="height: 100%;">
            @include('layouts.event-card.image')
        </div>
    </a>
    <div class="card-content">
        @include('layouts.event-card.description-small')
        @include('layouts.event-card.description')
        @include('layouts.event-card.remaining-tickets')
        @include('layouts.event-card.guests')
        @include('layouts.event-card.organizer')
        @include('layouts.event-card.sponsors')
    </div>
    @include('layouts.event-card.event-show-actions')
</div>
