<a href="#ticket-delete-modal" onclick="delete_ticket({{$ticket->id}})" class="btn-floating halfway-fab waves-effect waves-light modal-trigger secondary-content red">
    <i class="material-icons">delete</i>
</a>
