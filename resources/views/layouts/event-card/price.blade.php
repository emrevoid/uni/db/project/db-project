@if(isset($cart) || isset($eventshow))
    @isset($ticket->final_price)
        @if($ticket->final_price > 0)
        <div class="chip right" style="margin: -4em 0 0 0;">
            {{ number_format($ticket->final_price, 2) }} €
        </div>
        @endif
    @else
        <div class="chip right" style="margin: -4em 0 0 0;">
            GRATIS
        </div>
    @endisset
@endif
@isset($event->price)
    @isset($event->n_ticket)
        @if ($event->n_ticket_aval === 0)
            <div class="chip right" style="margin: -4em 0 0 0;">
                SOLD OUT
            </div>
        @else
            @if($event->price > 0)
                <div class="chip right" style="margin: -4em 0 0 0;">
                    {{ number_format($event->price, 2) }} €
                </div>
            @else
                <div class="chip right" style="margin: -4em 0 0 0;">
                    GRATIS
                </div>
            @endif
        @endif
    @else
        @if($event->price > 0)
            <div class="chip right" style="margin: -4em 0 0 0;">
                {{ number_format($event->price, 2) }} €
            </div>
        @else
            <div class="chip right" style="margin: -4em 0 0 0;">
                GRATIS
            </div>
        @endif
    @endisset
@endisset
