@if(\Illuminate\Support\Facades\Auth::check())
    <a id="btnAddCart" class="btn-floating btn right waves-light waves-effect
    @isset($fab)
        @if($fab === true)
            halfway-fab
        @endif
    @endisset
    @isset($event->n_ticket)
        @if ($event->n_ticket_aval === 0)
            disabled">
        @else
            " onclick="add_ticket({{$event->id}})"
            @isset($fab)
                @if($fab === true)
                    >
                @endif
            @endisset
            @isset($card_action)
                @if($card_action === true)
                    style="margin-right: 2em;">
                @endif
            @endisset
        @endif
    @else
        " onclick="add_ticket({{$event->id}})"
        @isset($fab)
            @if($fab === true)
                >
            @endif
        @endisset
        @isset($card_action)
            @if($card_action === true)
                 style="margin-right: 2em;">
            @endif
        @endisset
    @endisset
        <i class="material-icons">shopping_cart</i>
    </a>
@endif

