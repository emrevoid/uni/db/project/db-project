<div class="input-field">
    <input id="{{ $ticket->id }}" value="{{ $ticket->quantity }}" name="n_ticket" type="number" min="1" max="5">
    <label for="n_ticket">Numero di biglietti da acquistare</label>
</div>
