<div class="card-action" style="height: 5em">
    <button class="btn-flat left"><a href="{{route('events.index')}}">Indietro</a></button>
    @isset($current_user)
        @if ($current_user->userable_type === 'App\Administrator' || $event->organizer_id === Auth::id())
            <button class="btn-flat right"><a href="{{route('events.edit', $event->id)}}">Aggiorna evento</a></button>
        @endif
        @if ($current_user->userable_type === 'App\Customer')
            <form id="formCart" method="POST" action="{{ route('tickets.store') }}">
                @csrf
                <input name="event_id" type="hidden" value="{{ $event->id }}">
                @include('layouts.event-card.add-cart', ['card_action' => true])
            </form>
            @include('layouts.event-card.add-favorite', ['card_action' => true])
        @endif
    @endisset
</div>
