<form method="POST" action="{{ route('cart.update', $ticket->id) }}" id="ticket-update-form-{{$ticket->id}}">
    @csrf
    @method('PATCH')
    <button class="btn-floating ticket-update halfway-fab waves-effect waves-light secondary-content green disabled" style="margin-right: 4.5em;" type="submit" name="action">
        <i class="material-icons right">check</i>
    </button>
</form>
