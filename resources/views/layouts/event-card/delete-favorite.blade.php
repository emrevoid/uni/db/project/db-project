<form class="secondary-content" action="{{ route('favorites.destroy', $favorite->id)}}" method="post">
    @csrf
    @method('DELETE')
    <button class="btn-floating btn halfway-fab waves-effect waves-light red" type="submit">
        <i class="material-icons">delete</i>
    </button>
</form>
