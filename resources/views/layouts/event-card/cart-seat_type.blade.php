@isset($seat_types)
<div class="input-field">
    <select name="seat_type_id" form="ticket-update-form-{{$ticket->id}}">
        <option value="" disabled selected>Scegli un posto</option>
        @foreach($seat_types as $seat_type)
            @foreach($seat_type->seats as $seat)
                @if ($seat->event_id === $ticket->event_id)
                    <option
                        @isset($ticket->seat_type)
                            @if($ticket->seat_type->id === $seat->seat_type->id)
                                selected
                            @endif
                        @endisset
                        value="{{ $seat->seat_type->id }}">{{ $seat->seat_type->name }} ({{ $seat->unit_price }} €)</option>
                @endif
            @endforeach
        @endforeach
    </select>
    <label>Posto</label>
</div>
@endisset
