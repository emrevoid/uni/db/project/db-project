@isset($ticket_rates)
    <div class="input-field">
        <select name="ticket_rate_id" form="ticket-update-form-{{$ticket->id}}">
            <option value="" disabled selected>Scegli una tariffa</option>
            @foreach($ticket_rates as $ticket_rate)
            <option
                @isset($ticket->ticket_rate->id)
                    @if($ticket->ticket_rate->id === $ticket_rate->id) selected @endif
                @endisset
            value="{{ $ticket_rate->id }}">{{ $ticket_rate->name }} (sconto {{ $ticket_rate->amount * 100 }}%)</option>
            @endforeach
        </select>
        <label>Tariffa</label>
    </div>
@endisset
