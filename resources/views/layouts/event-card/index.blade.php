<div class="col s12 m4">
    <div class="card hoverable">
        <a href="/events/{{ $event->id }}" aria-label="Link all'evento {{$event->name}}">
            <div class="card-image" style="height: 100%;">
                @include('layouts.event-card.image')
                @include('layouts.event-card.add-cart', ['fab' => true])
                @include('layouts.event-card.add-favorite', ['fab' => true])
            </div>
        </a>
        <div class="card-content">
            @include('layouts.event-card.description-small')
            @isset($event->n_ticket_aval)
            <i class="tiny material-icons">confirmation_number</i>Biglietti rimanenti: {{ $event->n_ticket_aval }}
            @endisset
        </div>
    </div>
</div>
