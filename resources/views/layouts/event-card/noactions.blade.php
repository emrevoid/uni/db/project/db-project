<div class="col s12 m4">
    <div class="card hoverable">
        <a href="/events/{{ $event->id }}" aria-label="Link all'evento {{$event->name}}">
            <div class="card-image" style="height: 100%;">
                @include('layouts.event-card.image')
            </div>
        </a>
        <div class="card-content">
            @include('layouts.event-card.description-small')
        </div>
    </div>
</div>
