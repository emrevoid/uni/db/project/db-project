<i class="material-icons tiny">business</i> Sponsorizzato da:
<ul>
@foreach($event->funds as $fund)
    <li>
    {{ $fund->sponsor->name }}
        @isset($fund->sponsor_range->name)
            ({{ $fund->sponsor_range->name }} sponsor)
        @endisset
    </li>
@endforeach
</ul>
