@php ($event = $ticket->event)
<div class="col s12 m4 ticket">
    <div class="card hoverable">
        <a href="/events/{{ $event->id }}" aria-label="Link all'evento {{$event->name}}">
            <div class="card-image" style="height: 100%;">
                @include('layouts.event-card.image')
                @include('layouts.event-card.delete-ticket')
                @include('layouts.event-card.cart-ticket_update')
            </div>
        </a>
        <div class="card-content">
            @include('layouts.event-card.macrocategory')
            @include('layouts.event-card.title')
            @include('layouts.event-card.place')
            @include('layouts.event-card.datetime')
            @include('layouts.event-card.price', ['cart' => true])
{{--            @include('layouts.event-card.cart-ticket')--}}
            @include('layouts.event-card.cart-seat_type')
            @include('layouts.event-card.cart-ticket_rate')
        </div>
    </div>
</div>
