@if(\Illuminate\Support\Facades\Auth::check())
    <a id="btnAddFavorite" class="btn-floating btn waves-effect waves-light right
       @isset($fab)
       @if($fab === true)
       halfway-fab
       @endif
       @endisset
        "
    @if(isset($favorites_events_ids) && $favorites_events_ids !== [])
        @if(in_array($event->id, $favorites_events_ids, false))
            onclick="remove_favorite(this, {{$event->id}})">
                <i class="material-icons">favorite
        @else
            onclick="add_favorite(this, {{$event->id}})">
                <i class="material-icons">favorite_border
        @endif
    @else
        onclick="add_favorite(this, {{$event->id}})">
        <i class="material-icons">favorite_border
    @endif
            </i>
    </a>
@endif
