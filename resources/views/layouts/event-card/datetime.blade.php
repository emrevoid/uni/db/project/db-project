<p>
    <i class="tiny material-icons">event</i>
    @if ($event->start_date === $event->end_date)
        {{date('d/m/Y', strtotime($event->start_date))}}
    @else
        {{date('d/m/Y', strtotime($event->start_date))}} - {{date('d/m/Y', strtotime($event->end_date))}}
    @endif
    <br/><i class="tiny material-icons">access_time</i> {{date('H:m', strtotime($event->start_time))}} - {{date('H:m', strtotime($event->end_time))}}
</p>
