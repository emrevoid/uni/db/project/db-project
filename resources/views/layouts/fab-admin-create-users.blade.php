@can('isAdminOwner', Auth::user())
<div class="fixed-action-btn">
    <a class="btn-floating btn-large red">
        <i class="large material-icons">add</i>
    </a>
    <ul>
        @if(Route::has('register'))
        <li>
            <a class="btn-floating yellow darken-1" href="{{ route('register') }}"><i class="material-icons">add</i></a>
            <a href="#" class="btn-floating fab-padding mobile-fab-tip">Cliente</a>
        </li>
        @endif
        @if(Route::has('organizers.create'))
        <li>
            <a class="btn-floating green" href="{{ route('organizers.create') }}"><i class="material-icons">add</i></a>
            <a href="#" class="btn-floating fab-padding mobile-fab-tip">Organizzatore</a>
        </li>
        @endif
        @if(Route::has('administrators.create'))
        <li>
            <a class="btn-floating blue" href="{{ route('administrators.create') }}"><i class="material-icons">add</i></a>
            <a href="#" class="btn-floating fab-padding mobile-fab-tip">Amministratore</a>
        </li>
        @endif
    </ul>
</div>
@endcan
