<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            {{ config('app.name') }} è un progetto Open Source. Il codice sorgente è disponibile su <a class="grey-text text-lighten-3" href="https://gitlab.com/emrevoid/uni/web/web-project">GitLab</a> (Licenza GNU GPLv2).
            <a class="grey-text text-lighten-3 right" href="{{ route('organizers.create') }}">Organizzatori</a>
        </div>
    </div>
</footer>
