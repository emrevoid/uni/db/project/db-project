<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="{{ asset('/js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js" integrity="sha256-AdQN98MVZs44Eq2yTwtoKufhnU+uZ7v2kXnD5vqzZVo=" crossorigin="anonymous"></script>

    <script>
        window.Laravel = { csrfToken: '{{ csrf_token() }}' };
    </script>

    @if(!auth()->guest())
        <script>
            window.Laravel.userId = <?php echo auth()->user()->id; ?>;
        </script>
    @endif

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css" integrity="sha256-zvFVp826siCUuxc2KZRdlkALzLZWNb7FaJm9K1mRxW0=" crossorigin="anonymous" media="screen,projection"/>

    <style>
        html, .sidenav > li > a, a.collapsible-header, .collapsible-body > ul > li > a, .card-title, .dropdown-content > li > a, .dropdown-content > li > span, .select-dropdown, button.btn-flat > a{
            color: #414772;
        }

        #search_button:hover {
            background-color: #ff521b;
        }

        .card-content {
            color: rgba(0,0,0,0.87);
        }

        nav {
            background-color: #e88e20;
        }

        .btn, #btnAddFavorite, .pagination > li.active, .btn-large{
            background-color: #ff521b;
        }

        #btnAddFavorite {
            margin-right: 2em;
        }

        #btnAddFavorite.halfway-fab {
            margin-right: 4em;
        }

        #btnAddCart {
            background-color: #414772;
        }

        footer {
            background-color: #e88e20 !important;
        }

        .user-view .background {
            background-color: #e88e20;
        }

        i.material-icons.large {
            color: white;
        }

        /* Sticky footer */
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }

        main {
            flex: 1 0 auto;
        }

        .page-footer {
            padding-top: 0 !important;
        }

        .mobile-fab-tip {
            position: fixed;
            right: 85px;
            padding:0px 0.5rem;
            text-align: right;
            background-color: #323232;
            border-radius: 2px;
            color: #FFF;
            width:auto;
        }

        @media only screen and (min-width : 992px) {
            .fixed-action-btn {
                margin-bottom: 2em;
            }

            .fab-padding {
                margin-bottom: 0;
            }
        }

        @if(!auth()->guest())
        @if(Auth::user()->userable_type === 'App\Administrator' || Auth::user()->userable_type === 'App\Organizer')
        header, main, footer {
            padding-left: 300px;
        }

        @media only screen and (max-width : 992px) {
            header, main, footer {
                padding-left: 0;
            }
        }

        @media only screen and (min-width : 992px) {
            a.dropdown-trigger {
                margin-right: 20em;
            }
        }
        @endif
        @endif
    </style>
</head>
<body>
    @if(!auth()->guest())
    <ul id='dropdown-notification' class='dropdown-content'>
        <li><a href="#!">Non hai nuove notifiche.</a></li>
        <!--
        <li><a href="#!"><i class="material-icons">priority_high</i>I biglietti per l'evento X stanno terminando!</a></li>
        <li><a href="#!"><i class="material-icons">whatshot</i>C'è un evento che potrebbe interessarti!</a></li>
        <li><a href="#!"><i class="material-icons">event</i>La data per l'evento X è cambiata!</a></li>
        <li><a href="#!"><i class="material-icons">sentiment_very_dissatisfied</i>L'evento X è sold out!</a></li>
        -->
    </ul>
    <ul id="dropdown-user" class="dropdown-content">
        <li>
            <a href="{{ route('customers.edit', Auth::user(), Auth::user()->userable_id) }}">
                <i class="material-icons">person</i>
                Profilo
            </a>
        </li>
        <li>
            <a href="/myevents">
                <i class="material-icons">confirmation_number</i>
                Miei eventi
            </a>
        </li>
        <li>
            <a href="{{ route('orders.index') }}">
                <i class="material-icons">receipt</i>
                Ordini
            </a>
        </li>
        <li>
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                <i class="material-icons">exit_to_app</i>
                {{ __('Logout') }}
            </a>
        </li>
    </ul>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    @endif

    <header>
        @include('nav.nav-desktop')
        @include('nav.sidenav-common')
    </header>

    <main>
        <div class="container">
            @if(session()->get('success'))
                <div class="card-panel green lighten-2">
                    {{ session()->get('success') }}
                </div><br />
            @endif
            @if(session()->get('error'))
                <div class="card-panel red lighten-2">
                    {{ session()->get('error') }}
                </div>
            @endif
            @yield('content')
        </div>
    </main>

    @include('layouts.footer')

    @include('scripts.scripts-bottom')
</body>
</html>
