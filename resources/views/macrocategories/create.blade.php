@extends('layouts.app')

@section('content')
@can('isAdmin', auth()->user())
    <div class="row">
        <div class="col s12 m4 l4"></div>
        <div class="col s12 m4 l4">
            <div class="card">
                <form method="POST" action="{{ route('macrocategories.store') }}">
                    @csrf
                    <div class="card-content">
                        @if ($errors->any())
                            <div class="card-panel red lighten-1">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        <span class="card-title">Crea una macrocategoria</span>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="category_name" name="name" type="text" class="validate">
                                <label for="category_name">Nome macrocategoria</label>
                            </div>
                        </div>
                    </div>

                    <div class="card-action">
                        <button class="btn-flat"><a href="{{route('macrocategories.index')}}">Indietro</a></button>
                        <button type="submit" class="btn-flat">Crea macrocategoria</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col s12 m4 l4"></div>
    </div>
@endcan
@endsection
