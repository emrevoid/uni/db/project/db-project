@extends('layouts.app')

@section('content')
@can('isAdmin', auth()->user())
    <style>
        .collapsible-secondary {
            position: absolute;
            right: 0;
        }
        .collapsible-header {
            position: relative;
        }

        .collapsible-body {
            padding: 0;
        }

        .collapsible-body .collection .collection-item {
            padding: 10px 30px;
        }
    </style>

    <h4>Macrocategorie</h4>
    <ul class="collapsible expandable">
        @foreach($macrocategories as $macrocategory)
            <li>
                <div class="collapsible-header">
                    <div style="font-weight: bold;">{{ $macrocategory->name }}</div>
                    <a href="{{ route('macrocategories.edit', $macrocategory->id) }}" class="collapsible-secondary" style="padding-right: 2.5em;">
                        <i class="material-icons">edit</i>
                    </a>
                    <a href="#macro-delete-modal" onclick="deleteMacro({{$macrocategory->id}})" class="waves-effect waves-light modal-trigger collapsible-secondary">
                        <i class="material-icons red-text">delete</i>
                    </a>
                </div>
                <div class="collapsible-body">
                    <ul class="collection">
                        @foreach($categories as $category)
                            @if($category->macrocategory->id === $macrocategory->id)
                            <li class="collection-item">
                                {{ $category->name }}
                                <a href="#category-delete-modal" onclick="deleteCategory({{$category->id}})" class="waves-effect waves-light modal-trigger secondary-content">
                                    <i class="material-icons red-text">delete</i>
                                </a>
                                <a href="{{ route('categories.edit', $category->id) }}" class="secondary-content" style="padding-right: 0.5em;">
                                    <i class="material-icons">edit</i>
                                </a>
                            </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </li>
        @endforeach
    </ul>

    @if(Route::has('macrocategories.create') and Route::has('categories.create'))
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">add</i>
            </a>
            <ul>
                <li>
                    <a class="btn-floating yellow darken-1" href="{{ route('macrocategories.create') }}"><i class="material-icons">add</i></a>
                    <a href="#" class="btn-floating fab-padding mobile-fab-tip">Macrocategoria</a>
                </li>
                <li>
                    <a class="btn-floating green" href="{{ route('categories.create') }}"><i class="material-icons">add</i></a>
                    <a href="#" class="btn-floating fab-padding mobile-fab-tip">Categoria</a>
                </li>
            </ul>
        </div>
    @endif

    <div id="macro-delete-modal" class="modal">
        <form id="macro-delete-form" action="" method="post">
            <div class="modal-content">
                @csrf
                @method('DELETE')
                <h4>Conferma</h4>
                <p>Sei sicuro di voler cancellare la macrocategoria?</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="btn btn-flat modal-close red">No</a>
                <button type="submit" name="" class="btn btn-flat green" onclick="macroDeleteSubmit()">Si</button>
            </div>
        </form>
    </div>

    <div id="category-delete-modal" class="modal">
        <form id="category-delete-form" action="" method="post">
            <div class="modal-content">
                @csrf
                @method('DELETE')
                <h4>Conferma</h4>
                <p>Sei sicuro di voler cancellare la categoria?</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="btn btn-flat modal-close red">No</a>
                <button type="submit" name="" class="btn btn-flat green" onclick="categoryDeleteSubmit()">Si</button>
            </div>
        </form>
    </div>

    <script type="text/javascript">
        function deleteMacro(id)
        {
            var url = '{{ route("macrocategories.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#macro-delete-form").attr('action', url);
        }

        function macroDeleteSubmit()
        {
            $("#macro-delete-form").submit();
        }

        function deleteCategory(id)
        {
            var url = '{{ route("categories.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#category-delete-form").attr('action', url);
        }

        function categoryDeleteSubmit()
        {
            $("#category-delete-form").submit();
        }
    </script>

@endcan
@endsection
