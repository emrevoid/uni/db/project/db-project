@extends('layouts.app')

@section('content')
    <h1>Prossimi eventi</h1>

    <p></p>
    <!--
    <form action="/search" method="GET" role="search">
        <div class="row" style="margin-bottom: 0;">
            <div class="input-field col s10 m11">
                <input id="search" @isset($searchterm) value="{{$searchterm}}" @endisset type="search" name="searchterm" aria-label="Cerca per nome, ospite, luogo o organizzatore..." placeholder="Cerca per nome, ospite, luogo o organizzatore..." required>
                <i class="material-icons" onclick="clear_searchbar()">close</i>
            </div>
            <div class="input-field col s1">
                <button id="search_button" class="btn waves-effect waves-light" type="submit" name="action">
                    <i class="material-icons">search</i>
                </button>
            </div>
        </div>
    </form>
    -->
    <div class="row">
        <div class="input-field col s10 m11">
            <i class="material-icons prefix">filter_list</i>
            <select onchange="category_filter()" aria-label="Filtra per categoria" id="category_id" name="category_id">
                <option value="" disabled selected>   Filtra per categoria...</option>
                @foreach($macrocategories as $macrocategory)
                    <optgroup label="{{$macrocategory->name}}"></optgroup>
                    @foreach($macrocategory->categories as $category)
                        @isset($category_id)
                            @if($category->id === (int)$category_id)
                                <option value="{{ $category->id }}" selected>   {{ $category->name }}</option>
                            @else
                                <option value="{{ $category->id }}">   {{ $category->name }}</option>
                            @endif
                        @else
                            <option value="{{ $category->id }}">   {{ $category->name }}</option>
                        @endisset
                    @endforeach
                @endforeach
            </select>
        </div>
        @isset($category_id)
            <div class="input-field col s1">
            <a class="waves-effect waves-light btn red" onclick="clear_category()"><i class="material-icons">close</i></a>
        </div>
        @endisset
    </div>
    <div class="row">
        @foreach($events as $event)
            @if(\Illuminate\Support\Facades\Auth::check())
                @if(isset($favorites_events_ids) && $favorites_events_ids !== [])
                    @include('layouts.event-card.index', compact($event, $favorites_events_ids))
                @else
                    @include('layouts.event-card.index', compact($event))
                @endif
            @else
                @include('layouts.event-card.index', compact($event))
            @endif
        @endforeach
    </div>
    @if($events->total() !== 0)
        <ul class="pagination">
            @if($events->currentPage() !== 1)
                <li class="waves-effect"><a href="{{$events->previousPageUrl()}}">
            @else
                <li class="disabled waves-effect"><a href="#">
            @endif
                <i class="material-icons">chevron_left</i></a></li>
            @for ($page = 1,$pageMax = ceil($events->total()/$events->perPage()); $page <= $pageMax; $page++)
                @if($page === $events->currentPage())
                    <li class="active"><a href="#">{{$events->currentPage()}}</a></li>
                @else
                    <li class="waves-effect"><a href="{{$events->url($page)}}">{{$page}}</a></li>
                @endif
            @endfor
            @if($events->currentPage() !== $events->lastPage())
                <li class="waves-effect"><a href="{{$events->nextPageUrl()}}">
            @else
                <li class="disabled waves-effect"><a href="#">
            @endif
                <i class="material-icons">chevron_right</i></a></li>
        </ul>
    @else
        <h4 class="center">Non ci sono eventi che corrispondono ai criteri di ricerca.</h4>
    @endif
    <script>
        function getUrlVars() {
            let vars = {};
            let parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        }

        function getUrlParam(parameter, defaultvalue){
            let urlparameter = defaultvalue;
            if(window.location.href.indexOf(parameter) > -1){
                urlparameter = getUrlVars()[parameter];
            }
            return urlparameter;
        }

        function category_filter() {
            let select = document.getElementById("category_id");
            let category_id = select.options[select.selectedIndex].value;
            let searchterm = getUrlParam('searchterm','');
            if (searchterm != '') {
                window.location = "/search?searchterm=" + searchterm + "&category_id=" + category_id;
            } else {
                window.location = "/search?category_id=" + category_id;
            }
        }

        function clear_searchbar() {
            let search = document.getElementById("search");
            search.value = null;
            let select = document.getElementById("category_id");
            let category_id = select.options[select.selectedIndex].value;
            if (category_id !== '') {
                window.location = "/search?category_id=" + category_id;
            } else if (getUrlParam('searchterm','') !== '') {
                window.location = "/";
            }
        }

        function clear_category() {
            //let searchterm = document.getElementById("search").value;
            let select = document.getElementById("category_id");
            let category_id = select.options[select.selectedIndex].value;
//            if (searchterm !== '' && category_id !== '') {
            //if (category_id !== '') {
            //    window.location = "/search?searchterm=" + searchterm;
            //} else if (getUrlParam('category_id','') !== '') {
            if (getUrlParam('category_id','') !== '') {
                window.location = "/";
            }
        }
    </script>
@endsection
