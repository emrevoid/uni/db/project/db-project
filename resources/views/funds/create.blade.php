@extends('layouts.app')

@section('content')
@can('isAdmin', auth()->user())
    <div class="row">
        <div class="col s12 m4 l4"></div>
        <div class="col s12 m4 l4">
            <div class="card">
                <form method="POST" action="{{ route('funds.store') }}">
                    @csrf
                    <div class="card-content">
                        @if ($errors->any())
                            <div class="card-panel red lighten-1">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        <span class="card-title">Aggiungi una donazione</span>
                        <div class="row">
                            <div class="input-field col s12">
                                <select name="sponsor_id">
                                    <option value="" disabled selected>Scegli uno sponsor</option>
                                    @foreach($sponsors as $sponsor)
                                        <option value="{{ $sponsor->id }}">{{ $sponsor->name }}</option>
                                    @endforeach
                                </select>
                                <label>Sponsor</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <select name="event_id">
                                    <option value="" disabled selected>Scegli un evento</option>
                                    @foreach($events as $event)
                                    <option value="{{ $event->id }}">{{ $event->name }}</option>
                                    @endforeach
                                </select>
                                <label>Evento</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="fund_amount" name="amount" type="number" min="1" class="validate">
                                <label for="fund_amount">Ammontare</label>
                            </div>
                        </div>
                    </div>

                    <div class="card-action">
                        <button class="btn-flat"><a href="{{route('funds.index')}}">Indietro</a></button>
                        <button type="submit" class="btn-flat">Aggiungi donazione</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col s12 m4 l4"></div>
    </div>
@endcan
@endsection

