@extends('layouts.app')

@section('content')
    @can('isAdmin', auth()->user())
        <h4>Lista delle Donazioni</h4>
        <table class="striped responsive-table">
            <thead>
            <tr>
                <th>Fascia</th>
                <th>Sponsor</th>
                <th>Evento</th>
                <th>Importo</th>
                <th></th>
                <th></th>
            </tr>
            </thead>

            <tbody>
            @forelse($funds as $fund)
                <tr>
                    <td><div class="chip left">@isset($fund->sponsor_range->name){{ $fund->sponsor_range->name }}@else Nessuna fascia @endisset</div></td>
                    <td>{{ $fund->sponsor->name }}</td>
                    <td>{{ $fund->event->name }}</td>
                    <td>{{ $fund->amount }} €</td>
                    <td><a href="{{ route('funds.edit', $fund->id) }}"><i class="material-icons">edit</i></a></td>
                    <td>
                        <a href="#seat-delete-modal" onclick="delete_seat({{ $fund->id }})" class="waves-effect waves-light modal-trigger">
                            <i class="material-icons red-text">delete</i>
                        </a>
                    </td>
                </tr>
            @empty
                <li class="collection-item">Non sono presenti donazioni.</li>
            @endforelse
            </tbody>
        </table>

        @if(Route::has('funds.create'))
            <div class="fixed-action-btn">
                <a class="btn-floating btn-large red" href="{{ route('funds.create') }}">
                    <i class="large material-icons">add</i>
                </a>
            </div>
        @endif

        <div id="fund-delete-modal" class="modal">
            <form id="fund-delete-form" action="" method="post">
                <div class="modal-content">
                    @csrf
                    @method('DELETE')
                    <h4>Conferma</h4>
                    <p>Sei sicuro di voler cancellare la donazione?</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="btn btn-flat modal-close red">No</a>
                    <button type="submit" name="" class="btn btn-flat green" onclick="fund_delete_submit()">Si</button>
                </div>
            </form>
        </div>

        <script>
            function delete_fund(fund)
            {
                let url = '{{ route("funds.destroy", ":id") }}';
                url = url.replace(':id', fund);
                $("#fund-delete-form").attr('action', url);
            }

            function fund_delete_submit()
            {
                $("#fund-delete-form").submit();
            }
        </script>
    @endcan
@endsection
