@extends('layouts.app')

@section('content')
@can('isCustomer', auth()->user())
    <h1>Miei eventi</h1>
    <div class="row">
        <h2>Preferiti</h2>
        <div id="favorites" class="col s12">
            <div class="row">
            @if($favorites->isEmpty())
                <h3 class="center">Non hai eventi preferiti.</h3>
            @else
            @each('layouts.event-card.myevent-favorite', $favorites, 'favorite')
            @endif
            </div>
        </div>
        <h2>Prossimi</h2>
        <div id="next_events" class="col s12">
            @if(empty($next_events))
                <h3 class="center">Non parteciperai a nessun evento.</h3>
            @else
                @each('layouts.event-card.noactions', $next_events, 'event')
            @endif
        </div>
        <h2>Passati</h2>
        <div id="past_events" class="col s12">
            @if(empty($past_events))
                <h3 class="center">Non hai partecipato a nessun evento.</h3>
            @else
                @each('layouts.event-card.noactions', $past_events, 'event')
            @endif
        </div>
    </div>
@endcan
@endsection
