@extends('layouts.app')

@section('content')
    @can('isAdmin', auth()->user())
        <ul class="collection with-header">
            <li class="collection-header"><h4>Tipi posti</h4></li>
            @foreach($seat_types as $seat_type)
                <li class="collection-item">
                    <div>
                        {{ $seat_type->name }}

                        <a href="#seattype-delete-modal" onclick="delete_seattype({{ $seat_type->id }})" class="waves-effect waves-light modal-trigger secondary-content">
                            <i class="material-icons red-text">delete</i>
                        </a>
                        <a href="{{ route('seat_types.edit', $seat_type->id) }}" class="secondary-content">
                            <i class="material-icons">edit</i>
                        </a>
                    </div>
                </li>
            @endforeach
        </ul>

        @if(Route::has('seat_types.create'))
            <div class="fixed-action-btn">
                <a class="btn-floating btn-large red" href="{{ route('seat_types.create') }}">
                    <i class="large material-icons">add</i>
                </a>
            </div>
        @endif

        <div id="seattype-delete-modal" class="modal">
            <form id="seattype-delete-form" action="" method="post">
                <div class="modal-content">
                    @csrf
                    @method('DELETE')
                    <h4>Conferma</h4>
                    <p>Sei sicuro di voler cancellare la tipologia di posto?</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="btn btn-flat modal-close red">No</a>
                    <button type="submit" name="" class="btn btn-flat green" onclick="seattype_delete_submit()">Si</button>
                </div>
            </form>
        </div>

        <script>
            function delete_seattype(id)
            {
                let url = '{{ route("seat_types.destroy", ":id") }}';
                url = url.replace(':id', id);
                $("#seattype-delete-form").attr('action', url);
            }

            function seattype_delete_submit()
            {
                $("#seattype-delete-form").submit();
            }
        </script>
    @endcan
@endsection
