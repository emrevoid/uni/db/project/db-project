@extends('layouts.app')

@section('content')
    @can('isAdmin', auth()->user())
        <div class="row">
            <div class="col s12 m4 l4"></div>
            <div class="col s12 m4 l4">
                <div class="card">
                    <form method="POST" action="{{ route('seat_types.update', $seatType) }}">
                        @csrf
                        @method('PATCH')
                        <div class="card-content">
                            @if ($errors->any())
                                <div class="card-panel red lighten-1">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div><br />
                            @endif
                            <span class="card-title">Modifica tipologia posto: {{ $seatType->name }}</span>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input placeholder="{{ $seatType->name }}" id="seattype_name" name="name" type="text" value="{{ $seatType->name }}" class="validate">
                                    <label for="sponsorrange_name">Nome</label>
                                </div>
                            </div>
                        </div>

                        <div class="card-action">
                            <button class="btn-flat"><a href="{{route('seat_types.index')}}">Indietro</a></button>
                            <button type="submit" class="btn-flat">Aggiorna tipo posto</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col s12 m4 l4"></div>
        </div>
    @endcan
@endsection

