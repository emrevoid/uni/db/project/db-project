@extends('layouts.app')

@section('content')
@can('isCustomer', auth()->user())
    <h1>Carrello</h1>
    @if ($errors->any())
        <div class="card-panel red lighten-1">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif
    @if(!empty($tickets))
        <div class="row">
            @foreach($tickets as $ticket)
                @include('layouts.event-card.cart')
            @endforeach
        </div>
        <h5 id="price" class="right">Prezzo totale: {{ number_format($total_price, 2) }} €</h5>

        <div class="fixed-action-btn">
            <a href="#pay-modal" class="btn-floating btn-large modal-trigger fab-padding green">
                <i class="large material-icons">attach_money</i>
            </a>
        </div>

        <!--
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large">
                <i class="large material-icons">shopping_cart</i>
            </a>
            <ul>
                <li>
                    <a href="#cart-delete-modal" onclick="delete_cart($order->id)" class="btn-floating fab-padding modal-trigger red">
                        <i class="material-icons">delete</i>
                    </a>
                    <a href="#" class="btn-floating mobile-fab-tip">Svuota il carrello</a>
                </li>
                <li>
                    <a class="btn-floating blue fab-padding" onclick="save_cart()" ><i class="material-icons">save</i></a>
                    <a href="#" class="btn-floating mobile-fab-tip">Salva il carrello</a>
                </li>
                <li>
                    <a href="#pay-modal" onclick="pay($order->id)" class="btn-floating fab-padding modal-trigger green">
                        <i class="material-icons">attach_money</i>
                    </a>
                    <a href="#" class="btn-floating mobile-fab-tip">Conferma ordine</a>
                </li>
            </ul>
        </div>
        -->
        <div id="ticket-delete-modal" class="modal">
            <form id="ticket-delete-form" action="" method="post">
                <div class="modal-content">
                    @csrf
                    @method('DELETE')
                    <h4>Conferma</h4>
                    <p>Sei sicuro di voler rimuovere il biglietto dal carrello?</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="btn btn-flat modal-close red">No</a>
                    <button type="submit" name="" class="btn btn-flat green" onclick="ticket_delete_submit()">Si</button>
                </div>
            </form>
        </div>

        <div id="pay-modal" class="modal">
            <form id="pay-form" action="{{ route("cart.pay") }}" method="get">
                <div class="modal-content">
                    <h4>Conferma</h4>
                    <p>Sei sicuro di voler procedere con l'ordine?</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="btn btn-flat modal-close red">No</a>
                    <button type="submit" name="" class="btn btn-flat green" onclick="pay_submit()">Si</button>
                </div>
            </form>
        </div>

        <div id="cart-delete-modal" class="modal">
            <form id="cart-delete-form" action="" method="post">
                <div class="modal-content">
                    @csrf
                    @method('DELETE')
                    <h4>Conferma</h4>
                    <p>Sei sicuro di voler svuotare il carrello?</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="btn btn-flat modal-close red">No</a>
                    <button type="submit" name="" class="btn btn-flat green" onclick="cart_delete_submit()">Si</button>
                </div>
            </form>
        </div>

        <script>
            document.addEventListener('DOMContentLoaded', function() {
                let elems = document.querySelectorAll('.fixed-action-btn');
                let instances = M.FloatingActionButton.init(elems);
            });

            /*
            function save_cart() {
                let ticket_nodes = document.getElementsByName("n_ticket");
                let ticket_rates = document.getElementsByName("ticket_rate_id");
                let ticket_array = Array.from(ticket_nodes);
                let ticket_rates_array = Array.from(ticket_rates);
                let tickets = [];
                for (let i = 0; i < ticket_array.length; i++) {
                    tickets[i] = {id:ticket_array[i].id, quantity:ticket_array[i].value, ticket_rate:ticket_rates_array[i].options[ticket_rates_array[i].selectedIndex].value};
                }
                let tickets_json = JSON.stringify(tickets);
                let xmlHttp = new XMLHttpRequest();
                xmlHttp.onreadystatechange = function() {
                    if (xmlHttp.readyState == 4) {
                        if (xmlHttp.status === 404) {
                            M.toast({html: "Si sono verificati degli errori.", classes: 'red'});
                            window.location.href = window.location.href;
                        } else {
                            console.log(xmlHttp.responseText);
                            let response = JSON.parse(xmlHttp.responseText);
                            if (typeof response['error'] !== 'undefined') {
                                M.toast({html: response['error'], classes: 'red'});
                                if (response['old'] !== null) {
                                    for (let i = 0; i < response['old'].length; i++) {
                                        ticket_nodes[i].value = response['old'][i];
                                    }
                                }
                            } else {
                                M.toast({html: "Carrello salvato.", classes: 'green'});
                                let price = document.getElementById("price");
                                price.innerHTML = response['price'].toFixed(2) + ' €';
                            }
                        }
                    }
                };
                xmlHttp.open("POST", "/cart/update", true);
                xmlHttp.setRequestHeader("x-csrf-token", "{{csrf_token()}}");
                xmlHttp.send(tickets_json);
            }
            */

            function delete_ticket(id)
            {
                let url = '{{ route("cart.destroy", ":id") }}';
                url = url.replace(':id', id);
                $("#ticket-delete-form").attr('action', url);
            }

            function ticket_delete_submit()
            {
                $("#ticket-delete-form").submit();
            }

            function delete_cart(id)
            {
                let url = '{{ route("orders.destroy", ":id") }}';
                url = url.replace(':id', id);
                $("#cart-delete-form").attr('action', url);
            }

            function cart_delete_submit()
            {
                $("#cart-delete-form").submit();
            }

            function pay_submit()
            {
                $("#pay-form").submit();
            }

            $(document).ready(function() {
                function toggleUpdateButton(item) {
                    let to = !(item.closest('.card').find('select[name=\"ticket_rate_id\"]').prop('selectedIndex')
                                && item.closest('.card').find('select[name=\"seat_type_id\"]').prop('selectedIndex'));
                    item.closest(".card").find('.ticket-update').toggleClass('disabled', to);
                }

                $( "select[name=\"ticket_rate_id\"]" ).change(function() {
                    toggleUpdateButton($(this));
                });

                $( "select[name=\"seat_type_id\"]" ).change(function() {
                    toggleUpdateButton($(this));
                });
            });
        </script>
    @else
        <h5 class="center">Non ci sono biglietti nel tuo carrello.</h5>
    @endisset
@endcan
@endsection
