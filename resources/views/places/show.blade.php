@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="card-panel red lighten-1">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif
    <div class="row">
        <div class="col s12 m4 l3"></div>
        <div class="col s12 m4 l6">
            <h1>{{ $place->name }}</h1>
            <div class="card hoverable">
                <div class="card-content">
                    <span class="truncate card-title" style="font-weight: bold;">{{ $place->name }}</span>
                    <span class="truncate"><i class="tiny material-icons">place</i>{{ $place->address }}</span>
                    <span class="truncate"><i class="tiny material-icons">place</i>{{ $place->phoneNum }}</span>
                    <p><i class="tiny material-icons">message</i>{{ $place->description }}</p>
                    @foreach ($events as $event)
                        <span class="truncate"><i class="tiny material-icons">place</i>
                        <a href="/events/{{ $event->id }}" aria-label="Link all'evento {{$event->name}}">
                            {{ $event->start_date }}{{ $event->name }}
                        </a></span>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col s12 m4 l3"></div>
    </div>
@endsection
