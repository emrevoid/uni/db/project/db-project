@extends('layouts.app')

@section('content')
    @canany(['isAdminOwner', 'isAdmin'], $current_user)
        <div class="row">
            <div class="col s12 m4 l4"></div>
            <div class="col s12 m4 l4">
                <div class="card">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif

                    <form action="{{ route('places.update', $place->id) }}" method="post">
                        <div class="card-content">
                            @csrf
                            @method('PATCH')
                            <span class="card-title center-align">{{ __('Modifica Luogo') }}</span>

                            <div class="row">
                                <div class="input-field">
                                    <i class="material-icons prefix">person</i>
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $place->name }}" required autocomplete="name" autofocus>
                                    <label for="name">{{ __('Nome') }}</label>
                                    @error('name')
                                    <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field">
                                    <i class="material-icons prefix">person</i>
                                    <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ $place->address }}" required autocomplete="#address">
                                    <label for="address">{{ __('Indirizzo') }}</label>
                                    @error('address')
                                    <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field">
                                    <i class="material-icons prefix">person</i>
                                    <input id="phoneNum" type="text" class="form-control @error('phoneNum') is-invalid @enderror" name="phoneNum" value="{{ $place->phoneNum }}" required autocomplete="phoneNum">
                                    <label for="phoneNum">{{ __('Numero di Telefono') }}</label>
                                    @error('phoneNum')
                                    <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                    @enderror
                                </div>
                            </div>


                            <div class="row">
                                <div class="input-field">
                                    <i class="material-icons prefix">person</i>
                                    <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ $place->description }}" required autocomplete="#description">
                                    <label for="description">{{ __('Descrizione') }}</label>
                                    @error('description')
                                    <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                    @enderror
                                </div>
                            </div>

                            <div class="card-action center-align">
                                @csrf
                                @method('PATCH')
                                <button class="btn btn-danger" type="submit">Aggiorna Account</button>
                            </div>
                        </div>
                    </form>
                    <div class="card-action center-align">
                        <form action="{{ route('places.destroy', $current_user->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Cancella Account</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4"></div>
        </div>
    @endcanany
@endsection
