@extends('layouts.app')

@section('content')
    <h4>Lista dei Luoghi</h4>
    <table class="striped responsive-table">
        <thead>
        <tr>
            <th>Nome</th>
            <th>Indirizzo</th>
            <th>Numero di telefono</th>
            <th>Descrizione</th>
            <th></th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        @foreach($places as $place)
            <tr>
                <td><a href="/places/{{ $place->id }}" aria-label="Link al luogo {{$place->name}}">{{ $place->name }}</a></td>
                <td>{{ $place->address }}</td>
                <td>{{ $place->phoneNum }}</td>
                <td>{{ $place->description }}</td>

                @can('isAdmin', auth()->user())
                <td>
                    <a href="{{ route('places.edit', $place->id) }}">
                        <i class="material-icons">edit</i>
                    </a>
                </td>
                <td>
                    <a href="#place-delete-modal" onclick="delete_place({{ $place->id }})" class="waves-effect waves-light modal-trigger">
                        <i class="material-icons red-text">delete</i>
                    </a>
                </td>
                @endcan
            </tr>
        @endforeach
        </tbody>
    </table>

    @can('isAdmin', Auth::user())
        @if(Route::has('places.create'))
            <div class="fixed-action-btn">
                <a class="btn-floating btn-large red" href="{{ route('places.create') }}">
                    <i class="large material-icons">add</i>
                </a>
            </div>
        @endif
    @endcan

    <div id="place-delete-modal" class="modal">
        <form id="place-delete-form" action="" method="post">
            <div class="modal-content">
                @csrf
                @method('DELETE')
                <h4>Conferma</h4>
                <p>Sei sicuro di voler cancellare questo luogo?</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="btn btn-flat modal-close red">No</a>
                <button type="submit" name="" class="btn btn-flat green" onclick="place_delete_submit()">Si</button>
            </div>
        </form>
    </div>

    <script>
        function delete_place(id)
        {
            let url = '{{ route("places.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#place-delete-form").attr('action', url);
        }

        function place_delete_submit()
        {
            $("#place-delete-form").submit();
        }
    </script>

@endsection
