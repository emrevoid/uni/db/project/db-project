@extends('layouts.app')

@section('content')
    @can('isAdmin', auth()->user())
        <div class="row">
            <div class="col s12 m4 l3"></div>
            <div class="col s12 m4 l6">
                <h1 class="center-align">{{ __('Crea un luogo') }}</h1>
                <div class="card">
                    <form method="POST" action="{{ route('places.store') }}">
                        @csrf
                        <div class="card-content">
                            @if ($errors->any())
                                <div class="card-panel red lighten-1">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div><br />
                            @endif

                            <div class="row">
                                <div class="input-field">
                                    <i class="material-icons prefix">person</i>
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    <label for="name">{{ __('Nome') }}</label>
                                    @error('name')
                                    <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field">
                                    <i class="material-icons prefix">person</i>
                                    <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="#address">
                                    <label for="address">{{ __('Indirizzo') }}</label>
                                    @error('address')
                                    <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field">
                                    <i class="material-icons prefix">person</i>
                                    <input id="phoneNum" type="text" class="form-control @error('phoneNum') is-invalid @enderror" name="phoneNum" value="{{ old('phoneNum') }}" required autocomplete="phoneNum">
                                    <label for="phoneNum">{{ __('Numero di Telefono') }}</label>
                                    @error('phoneNum')
                                    <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                    @enderror
                                </div>
                            </div>


                                <div class="row">
                                    <div class="input-field">
                                        <i class="material-icons prefix">person</i>
                                        <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autocomplete="#description">
                                        <label for="description">{{ __('Descrizione') }}</label>
                                        @error('description')
                                        <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="card-action center-align">
                                <button type="submit" class="btn-flat">
                                    {{ __('Registra') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col s12 m4 l3"></div>
        </div>
    @endcan
@endsection
