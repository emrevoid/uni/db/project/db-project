@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="card-panel red lighten-1">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif
    <div class="row">
        <div class="col s12 m4 l3"></div>
        <div class="col s12 m4 l6">
            <h1>{{$event->name}}</h1>
            @include('layouts.event-card.large')
        </div>
        <div class="col s12 m4 l3"></div>
    </div>
@endsection
