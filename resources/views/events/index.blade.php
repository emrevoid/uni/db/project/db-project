@extends('layouts.app')

@section('content')
    <ul class="collection with-header">
        <li class="collection-header"><h4>Eventi</h4></li>
        @foreach($events as $event)
            <li class="collection-item">
                <div>
                    {{ $event->name }}

                    <a href="{{ route('events.show', $event->id) }}" class="secondary-content">
                        <i class="material-icons">info</i>
                    </a>
                    <a href="#event-delete-modal" onclick="delete_event({{$event->id}})" class="waves-effect waves-light modal-trigger secondary-content">
                        <i class="material-icons red-text">delete</i>
                    </a>
                    <a href="{{ route('events.edit', $event->id) }}" class="secondary-content">
                        <i class="material-icons">edit</i>
                    </a>
                </div>
            </li>
        @endforeach
    </ul>


    @if(Route::has('events.create'))
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large fab-padding red" href="{{ route('events.create') }}">
                <i class="large material-icons">add</i>
            </a>
        </div>
    @endif

    <div id="event-delete-modal" class="modal">
        <form id="event-delete-form" action="" method="post">
            <div class="modal-content">
                @csrf
                @method('DELETE')
                <h4>Conferma</h4>
                <p>Sei sicuro di voler cancellare l'evento?</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="btn btn-flat modal-close red">No</a>
                <button type="submit" name="" class="btn btn-flat green" onclick="event_delete_submit()">Si</button>
            </div>
        </form>
    </div>

    <script>
        function delete_event(id)
        {
            let url = '{{ route("events.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#event-delete-form").attr('action', url);
        }

        function event_delete_submit()
        {
            $("#event-delete-form").submit();
        }
    </script>

@endsection
