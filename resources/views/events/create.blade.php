@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="card-panel red lighten-1">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif
    <div class="row">
        <div class="col s12 m4 l3"></div>
        <div class="col s12 m4 l6">
            <h1>Crea un evento</h1>
            <div class="card">
                <form method="POST" action="{{ route('events.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-content">
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="event_name" name="name" type="text" class="validate">
                                <label for="event_name">Nome</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <textarea id="event_description" name="description" class="materialize-textarea"></textarea>
                                <label for="event_description">Descrizione</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="file-field">
                                <div class="btn">
                                    <span>Sfoglia..</span>
                                    <input type="file" name="image">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Carica la locandina dell'evento..">
                                </div>
                            </div>
                        </div>
                        <!--
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="event_price" name="price" type="text" class="validate">
                                <label for="event_price">Prezzo</label>
                                <div class="right-align">
                                    <label>
                                        <input onclick="checkbox_controller('#check_price', '#event_price')" type="checkbox" id="check_price" class="filled-in"/>
                                        <span>Gratuito</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        -->
                        <div class="row">
                            <div class="input-field col s12">
                                <select name="place_id">
                                    <option value="" disabled selected>Seleziona il luogo</option>
                                    @foreach($places as $place)
                                        <option value="{{ $place->id }}">{{ $place->name }}</option>
                                    @endforeach
                                </select>
                                <label for="event_place">Luogo</label>
                            </div>
                        </div>
                        <!--
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="event_n_ticket" name="n_ticket" type="text" class="validate">
                                <label for="event_n_ticket">Numero dei biglietti disponibili</label>
                                <div class="right-align">
                                    <label>
                                        <input onclick="checkbox_controller('#check_n_ticket', '#event_n_ticket')" type="checkbox" id="check_n_ticket" class="filled-in"/>
                                        <span>Non sono previsti biglietti</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        -->
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="event_start_date" name="start_date" type="text" class="datepicker">
                                <label for="event_start_date">Data d'inizio</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="event_end_date" name="end_date" type="text" class="datepicker">
                                <label for="event_end_date">Data di fine</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="event_start_time" name="start_time" type="text" class="timepicker">
                                <label for="event_start_time">Orario d'inizio</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="event_end_time" name="end_time" type="text" class="timepicker">
                                <label for="event_end_time">Orario di fine</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <select name="category_id">
                                    <option value="">Scegli una categoria</option>
                                    @foreach($macrocategories as $macrocategory)
                                        <optgroup label="{{$macrocategory->name}}"></optgroup>
                                        @foreach($macrocategory->categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                                <label>Categoria</label>
                            </div>
                        </div>
                        @if($current_user->userable_type === 'App\Administrator')
                            <div class="row">
                                <div class="input-field col s12">
                                    <select name="organizer_id">
                                        <option value="" disabled selected>Scegli un organizzatore</option>
                                        @foreach($organizers as $organizer)
                                            <option value="{{ $organizer->id }}">{{ $organizer->email }}</option>
                                        @endforeach
                                    </select>
                                    <label>Organizzatore</label>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="card-action">
                        <button class="btn-flat"><a href="{{route('events.index')}}">Annulla</a></button>
                        <button type="submit" class="btn-flat">Crea l'evento</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col s12 m4 l3"></div>
    </div>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            let date = document.querySelectorAll('.datepicker');
            let date_in = M.Datepicker.init(date, {format: 'yyyy-mm-dd'});
            let time = document.querySelectorAll('.timepicker');
            let time_in = M.Timepicker.init(time, {twelveHour: false});
        });

        function checkbox_controller(checkbox_id, textbox_id) {
            if ($(checkbox_id).prop('checked')) {
                $(textbox_id).prop('value', '');
                $(textbox_id).prop('disabled', true);
            } else {
                $(textbox_id).prop('disabled', false);
            }
        }
    </script>
@endsection
