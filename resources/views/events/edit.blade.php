@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col s12 m4 l3"></div>
        <div class="col s12 m4 l6">
            <div class="card">
                <form method="POST" action="{{ route('events.update', $event->id) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="card-content">
                        @if ($errors->any())
                            <div class="card-panel red lighten-1">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        <span class="card-title">Modifica evento: {{ $event->name }}</span>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="{{ $event->name }}" id="event_name" name="name" type="text" value="{{ $event->name }}" class="validate">
                                <label for="event_name">Nome</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <textarea placeholder="{{ $event->description }}" id="event_description" name="description" class="materialize-textarea">{{ $event->description }}</textarea>
                                <label for="event_description">Descrizione</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="{{ $event->start_date }}" id="event_start_date" name="start_date" type="text" value="{{ $event->start_date }}" class="datepicker">
                                <label for="event_start_date">Data d'inizio</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="{{ $event->end_date }}" id="event_end_date" name="end_date" type="text" value="{{ $event->end_date }}" class="datepicker">
                                <label for="event_end_date">Data di fine</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="{{ date('H:m', strtotime($event->start_time)) }}" id="event_start_time" name="start_time" type="text" value="{{ date('H:m', strtotime($event->start_time)) }}" class="timepicker">
                                <label for="event_start_time">Orario d'inizio</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="{{ date('H:m', strtotime($event->end_time)) }}" id="event_end_time" name="end_time" type="text" value="{{ date('H:m', strtotime($event->end_time)) }}" class="timepicker">
                                <label for="event_end_time">Orario di fine</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <select id="event_place" name="place_id" type="text" class="validate">
                                    <option value="" disabled selected>Seleziona il luogo</option>
                                    @foreach($places as $place)
                                        <option @if($event->place->id === $place->id) selected @endif value="{{ $place->id }}">{{ $place->name }}</option>
                                    @endforeach
                                </select>
                                <label for="event_place">Luogo</label>
                            </div>
                        </div>
                            <!--
                        @isset($event->n_ticket)
                            <div class="row">
                                <div class="input-field col s12">
                                    <input placeholder="{{ (int)$event->n_ticket - (int)$event->n_ticket_aval }}" id="event_n_ticket_remaining" name="n_ticket_remaining" type="text" value="{{ (int)$event->n_ticket - (int)$event->n_ticket_aval }}" class="validate">
                                    <label for="event_n_ticket_remaining">Biglietti rimanenti</label>
                                </div>
                            </div>
                        @endisset
                            -->
                        <div class="row">
                            <div class="file-field">
                                <div class="btn">
                                    <span>Sfoglia..</span>
                                    <input type="file" name="image">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Carica la locandina dell'evento.." value="{{ $event->image }}">
                                </div>
                            </div>
                            <div class="col s12">
                                <img class="responsive-img" src="{{Storage::disk('s3')->url($event->image)}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <select name="category_id">
                                    <option value="{{ $event->category->id }}">{{ $event->category->name }}</option>
                                    @foreach($macrocategories as $macrocategory)
                                        <optgroup label="{{$macrocategory->name}}"></optgroup>
                                        @foreach($macrocategory->categories as $category)
                                            <option value="{{ $category->id }}" @if($event->category->id === $category->id) selected @endif>{{ $category->name }}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                                <label>Categoria</label>
                            </div>
                        </div>
                    </div>
                    <div class="card-action">
                        <button class="btn-flat"><a href="{{route('events.index')}}">Indietro</a></button>
                        <button type="submit" class="btn-flat">Aggiorna evento</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col s12 m4 l3"></div>
    </div>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            let date = document.querySelectorAll('.datepicker');
            let date_in = M.Datepicker.init(date, {format: 'yyyy-mm-dd'});
            let time = document.querySelectorAll('.timepicker');
            let time_in = M.Timepicker.init(time, {twelveHour: false});
        });

        function checkbox_controller(checkbox_id, textbox_id) {
            if ($(checkbox_id).prop('checked')) {
                $(textbox_id).prop('value', '');
                $(textbox_id).prop('disabled', true);
            } else {
                $(textbox_id).prop('disabled', false);
            }
        }
    </script>
@endsection
