@component('mail::message')

Ciao {{ $notifiable->userable->name }},

l'evento **{{ $event->name }}** è stato aggiornato.

@component('mail::panel')

**Data**: dal {{ $event->start_date }} al {{ $event->end_date }}

**Ora**: dalle {{ $event->start_time }} alle {{ $event->end_time }}

**In**: {{ $event->place }}

**Ospiti**: {{ $event->guest }}

@endcomponent

@component('mail::button', ['url' =>  '/events/'.$event->id])
    Visualizza evento
@endcomponent

{{ config('app.name') }}
@endcomponent
