@component('mail::message')

Ciao {{ $notifiable->name}},

l'evento **{{ $event->name }}** è sold out.

{{ config('app.name') }}
@endcomponent
