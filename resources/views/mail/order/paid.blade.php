@component('mail::message')

Ciao {{ $notifiable->userable->name }},

grazie per il tuo ordine!

@component('mail::table')
| Evento        | Numero biglietti | Prezzo* |
| ------------- |:-------------:| :--------: |
@foreach($order->tickets as $ticket)
| {{ $ticket->event->name }}      |  {{ $ticket->quantity }}     | {{  $ticket->event->price }} |
@endforeach
@endcomponent

\* unitario

@component('mail::button', ['url' => '/orders/'.$order->id])
Visualizza ordine
@endcomponent

Grazie per aver scelto {{ config('app.name') }}.
@endcomponent
