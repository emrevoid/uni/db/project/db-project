@extends('layouts.app')

@section('content')
@can('isAdmin', auth()->user())
    <ul class="collection with-header">
        <li class="collection-header"><h4>Categorie</h4></li>
        @foreach($categories as $category)
            <li class="collection-item">
                <div>
                    {{ $category->name }}

                    <a href="#category-delete-modal" onclick="delete_category({{$category->id}})" class="waves-effect waves-light modal-trigger secondary-content">
                        <i class="material-icons red-text">delete</i>
                    </a>
                    <a href="{{ route('categories.edit', $category->id) }}" class="secondary-content">
                        <i class="material-icons">edit</i>
                    </a>
                </div>
            </li>
        @endforeach
    </ul>

    @if(Route::has('categories.create'))
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href="{{ route('categories.create') }}">
                <i class="large material-icons">add</i>
            </a>
        </div>
    @endif

    <div id="category-delete-modal" class="modal">
        <form id="category-delete-form" action="" method="post">
            <div class="modal-content">
                @csrf
                @method('DELETE')
                <h4>Conferma</h4>
                <p>Sei sicuro di voler cancellare la categoria?</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="btn btn-flat modal-close red">No</a>
                <button type="submit" name="" class="btn btn-flat green" onclick="category_delete_submit()">Si</button>
            </div>
        </form>
    </div>

    <script>
        function delete_category(id)
        {
            let url = '{{ route("categories.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#category-delete-form").attr('action', url);
        }

        function category_delete_submit()
        {
            $("#category-delete-form").submit();
        }
    </script>
@endcan
@endsection
