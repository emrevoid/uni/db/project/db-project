@extends('layouts.app')

@section('content')
@canany(['isAdminOwner', 'isAdmin'], $current_user)
    <div class="row">
        <div class="col s12 m4 l4"></div>
        <div class="col s12 m4 l4">
            <div class="card">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif

                <form action="{{ route('administrators.update', $current_user->id) }}" method="post">
                    <div class="card-content">
                        @csrf
                        @method('PATCH')
                        <span class="card-title center-align">{{ __('Modifica Profilo') }}</span>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">person</i>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $current_user->name }}" required autocomplete="name" autofocus>
                                <label for="name">{{ __('Nome') }}</label>
                                @error('name')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">person</i>
                                <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ $current_user->surname }}" required autocomplete="#surname">
                                <label for="surname">{{ __('Cognome') }}</label>
                                @error('surname')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">email</i>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $current_user->email }}" required autocomplete="email">
                                <label for="email">{{ __('Indirizzo E-Mail') }}</label>
                                @error('email')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">lock</i>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                <label for="password">{{ __('Password') }}</label>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">lock</i>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                <label for="password-confirm">{{ __('Conferma Password') }}</label>
                            </div>
                        </div>

                        <div class="card-action center-align">
                            @csrf
                            @method('PATCH')
                            <button class="btn btn-danger" type="submit">Aggiorna Account</button>
                        </div>
                    </div>
                </form>
                <div class="card-action center-align">
                    <form action="{{ route('administrators.destroy', $current_user->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Cancella Account</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col s12 m4 l4"></div>
    </div>
@endcanany
@endsection
