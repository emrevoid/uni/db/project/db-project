@extends('layouts.app')

@section('content')
@can('isAdmin', auth()->user())
    <ul class="collection with-header">
        <li class="collection-header"><h4>Pannello di amministrazione</h4></li>
            <div>
                <li class="collection-item">
                    <div><a href="{{ route('customers.index') }}" class="secondary-content">Utente</a></div>
                </li>
                <li class="collection-item">
                    <div><a href="{{ route('organizers.index') }}" class="secondary-content">Organizzatore</a></div>
                </li>
                <li class="collection-item">
                    <div><a href="{{ route('administrators.index') }}" class="secondary-content">Amministratore</a></div>
                </li>
            </div>
    </ul>
@endcan
@endsection
