@extends('layouts.app')

@section('content')
@can('isAdmin', auth()->user())
    <ul class="collection with-header">
        <li class="collection-header"><h4>Lista degli amministratori</h4></li>
        @foreach($users as $user)
            <li class="collection-item">
                <div>
                    {{ $user->name }}
                    {{ $user->surname }}
                    {{ $user->email }}
                    <a href="#administrator-delete-modal" onclick="delete_administrator({{$user->id}})" class="waves-effect waves-light modal-trigger secondary-content">
                        <i class="material-icons red-text">delete</i>
                    </a>
                    <a href="{{ route('customers.edit', $user->id) }}" class="secondary-content">
                        <i class="material-icons">edit</i>
                    </a>
                </div>
            </li>
        @endforeach
    </ul>
    @include('layouts.fab-admin-create-users')
@endcan

    <div id="administrator-delete-modal" class="modal">
        <form id="administrator-delete-form" action="" method="post">
            <div class="modal-content">
                @csrf
                @method('DELETE')
                <h4>Conferma</h4>
                <p>Sei sicuro di voler cancellare l'amministratore?</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="btn btn-flat modal-close red">No</a>
                <button type="submit" name="" class="btn btn-flat green" onclick="administrator_delete_submit()">Si</button>
            </div>
        </form>
    </div>

    <script>
        function delete_administrator(id)
        {
            let url = '{{ route("administrators.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#administrator-delete-form").attr('action', url);
        }

        function administrator_delete_submit()
        {
            $("#administrator-delete-form").submit();
        }
    </script>

@endsection
