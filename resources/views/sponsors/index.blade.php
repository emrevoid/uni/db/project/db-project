@extends('layouts.app')

@section('content')
    @can('isAdmin', auth()->user())
        <ul class="collection with-header">
            <li class="collection-header"><h4>Sponsors</h4></li>
            @foreach($sponsors as $sponsor)
                <li class="collection-item">
                    <div>
                        <a href="{{ route('sponsors.show', $sponsor) }}">{{ $sponsor->name }}</a>

                        <a href="#sponsor-delete-modal" onclick="delete_sponsor({{ $sponsor->id }})" class="waves-effect waves-light modal-trigger secondary-content">
                            <i class="material-icons red-text">delete</i>
                        </a>
                        <a href="{{ route('sponsors.edit', $sponsor->id) }}" class="secondary-content">
                            <i class="material-icons">edit</i>
                        </a>
                    </div>
                </li>
            @endforeach
        </ul>

        @if(Route::has('sponsors.create'))
            <div class="fixed-action-btn">
                <a class="btn-floating btn-large red" href="{{ route('sponsors.create') }}">
                    <i class="large material-icons">add</i>
                </a>
            </div>
        @endif

        <div id="sponsor-delete-modal" class="modal">
            <form id="sponsor-delete-form" action="" method="post">
                <div class="modal-content">
                    @csrf
                    @method('DELETE')
                    <h4>Conferma</h4>
                    <p>Sei sicuro di voler cancellare lo sponsor?</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="btn btn-flat modal-close red">No</a>
                    <button type="submit" name="" class="btn btn-flat green" onclick="sponsor_delete_submit()">Si</button>
                </div>
            </form>
        </div>

        <script>
            function delete_sponsor(sponsor)
            {
                let url = '{{ route("sponsors.destroy", ":id") }}';
                url = url.replace(':id', sponsor);
                $("#sponsor-delete-form").attr('action', url);
            }

            function sponsor_delete_submit()
            {
                $("#sponsor-delete-form").submit();
            }
        </script>
    @endcan
@endsection
