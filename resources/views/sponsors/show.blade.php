@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col s12 m4 l3"></div>
        <div class="col s12 m4 l6">
            <h1>{{$sponsor->name}}</h1>
            <div class="card hoverable">
                <div class="card-content">
                    <p><i class="tiny material-icons">message</i> {{ $sponsor->description }}</p>
                    Ha sponsorizzato gli eventi:
                    <ul>
                    @foreach($sponsor->funds as $fund)
                        <li>
                            <a href="{{ route('events.show', $fund->event->id) }}">{{ $fund->event->name }}</a>
                        </li>
                    @endforeach
                    </ul>
                </div>
                <div class="card-action" style="height: 5em">
                    <button class="btn-flat left"><a href="{{route('sponsors.index')}}">Indietro</a></button>
                </div>
            </div>
        </div>
        <div class="col s12 m4 l3"></div>
    </div>
@endsection
