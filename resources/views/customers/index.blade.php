@extends('layouts.app')

@section('content')
@can('isAdmin', auth()->user())
    <h4>Lista dei Clienti</h4>
    <table class="striped responsive-table">
        <thead>
        <tr>
            <th>Nome</th>
            <th>Cognome</th>
            <th>Email</th>
            <th>Sesso</th>
            <th>Data di Nascita</th>
            <th>Residenza</th>
            <th></th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->surname }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->userable->sesso }}</td>
                <td>{{ $user->userable->dataNascita }}</td>
                <td>{{ $user->userable->residenza }}</td>
                <td>
                    <a href="{{ route('customers.edit', $user->id) }}">
                        <i class="material-icons">edit</i>
                    </a>
                </td>
                <td>
                    <a href="#profile-delete-modal" onclick="delete_profile({{ $user->id }})" class="waves-effect waves-light modal-trigger">
                        <i class="material-icons red-text">delete</i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @include('layouts.fab-admin-create-users')
@endcan

    <div id="profile-delete-modal" class="modal">
        <form id="profile-delete-form" action="" method="post">
            <div class="modal-content">
                @csrf
                @method('DELETE')
                <h4>Conferma</h4>
                <p>Sei sicuro di voler cancellare il profilo?</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="btn btn-flat modal-close red">No</a>
                <button type="submit" name="" class="btn btn-flat green" onclick="profile_delete_submit()">Si</button>
            </div>
        </form>
    </div>

    <script>
        function delete_profile(id)
        {
            let url = '{{ route("customers.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#profile-delete-form").attr('action', url);
        }

        function profile_delete_submit()
        {
            $("#profile-delete-form").submit();
        }
    </script>

@endsection
