@extends('layouts.app')

@section('content')
@canany(['isAdmin','isCustomerOwner'], $current_user)
        <div class="row">
        <div class="col s12 m4 l3"></div>
            <div class="col s12 m4 l6">
            <h1>{{ __('Modifica Profilo') }}</h1>
            <div class="card">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                        </ul>
                    </div><br />
                @endif

                <form action="{{ route('customers.update', $current_user->id) }}" method="post">
                    <div class="card-content">
                        @csrf
                        @method('PATCH')
                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">person</i>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $current_user->name }}" required autocomplete="name" autofocus>
                                <label for="name">{{ __('Nome') }}</label>
                                @error('name')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">person</i>
                                <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ $current_user->surname }}" required autocomplete="#surname">
                                <label for="surname">{{ __('Cognome') }}</label>
                                @error('surname')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <p><label>
                                    <input type="radio" id="male" name="sesso" value="M" @if($customer->sesso === 'M') checked @endif><span>Maschio</span>
                                </label></p>
                                <p><label>
                                    <input type="radio" id="female" name="sesso" value="F" @if($customer->sesso === 'F') checked @endif><span>Femmina</span>
                                </label></p>
                                @error('sesso')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">date_range</i>
                                <input id="dataNascita" type="text" name="dataNascita" class="datepicker" value="{{ $customer->dataNascita }}">
                                <label for="dataNascita">Data di Nascita</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">place</i>
                                <input id="residenza" type="text" class="form-control @error('residenza') is-invalid @enderror" name="residenza" value="{{ $customer->residenza }}" required autocomplete="residenza" autofocus>
                                <label for="residenza">{{ __('Residenza') }}</label>
                                @error('residenza')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">email</i>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $current_user->email }}" required autocomplete="email">
                                <label for="email">{{ __('Indirizzo E-Mail') }}</label>
                                @error('email')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">lock</i>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                <label for="password">{{ __('Password') }}</label>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">lock</i>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                <label for="password-confirm">{{ __('Conferma password') }}</label>
                            </div>
                        </div>

                        <div class="card-action center-align">
                            @csrf
                            @method('PATCH')
                            <button class="btn btn-danger" type="submit">Aggiorna account</button>
                        </div>
                    </div>
                </form>

                <div class="card-action center-align">
                    <form action="{{ route('customers.destroy', $current_user->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Cancella account</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col s12 m4 l3"></div>
    </div>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        let date = document.querySelectorAll('.datepicker');
        let date_in = M.Datepicker.init(date, {
            format: 'yyyy-mm-dd',
            yearRange: [1900, 2020],
            defaultDate: Date.now()
        });
        let time = document.querySelectorAll('.timepicker');
        let time_in = M.Timepicker.init(time, {twelveHour: false});
    });

    function checkbox_controller(checkbox_id, textbox_id) {
        if ($(checkbox_id).prop('checked')) {
            $(textbox_id).prop('value', '');
            $(textbox_id).prop('disabled', true);
        } else {
            $(textbox_id).prop('disabled', false);
        }
    }
</script>
@endcanany
@endsection
