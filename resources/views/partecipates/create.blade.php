@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col s12 m4 l3"></div>
        <div class="col s12 m4 l6">
            <h1 class="center-align">{{ __('Aggiungi Ospite') }}</h1>
            <div class="card">
                <form method="POST" action="{{ route('partecipates.store') }}">
                    @csrf
                    <div class="card-content">
                        @if ($errors->any())
                            <div class="card-panel red lighten-1">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif

                            <div class="row">
                                <div class="input-field col s12">
                                    <select name="event_id">
                                        <option value="" disabled selected>Seleziona l'Evento</option>
                                        @foreach($events as $event)
                                            <option value="{{ $event->id }}">{{ $event->name }}</option>
                                        @endforeach
                                    </select>
                                    <label for="event_id">Evento</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <select name="guest_id">
                                        <option value="" disabled selected>Seleziona l'Ospite</option>
                                        @foreach($guests as $guest)
                                            <option value="{{ $guest->id }}">{{ $guest->name }}</option>
                                        @endforeach
                                    </select>
                                    <label for="guest_id">Ospite</label>
                                </div>
                            </div>

                        <div class="card-action center-align">
                            <button type="submit" class="btn-flat">
                                {{ __('Aggiungi') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col s12 m4 l3"></div>
    </div>
@endsection
