@extends('layouts.app')

@section('content')
    <h4>Lista dei Partecipanti</h4>
    <table class="striped responsive-table">
        <thead>
        <tr>
            <th>Evento</th>
            <th>Ospite</th>
            <th></th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        @foreach($partecipates as $partecipant)
            <tr>
                <td>
                    @isset($partecipant->event)
                    <a href="/events/{{ $partecipant->event->id }}" aria-label="Link all'ospite {{$partecipant->event->name}}">{{ $partecipant->event->name }}</a>
                    @endif
                </td>
                <td>
                    @isset($partecipant->guest)
                    <a href="/guests/{{ $partecipant->guest->id }}" aria-label="Link all'ospite {{$partecipant->guest->name}}">{{ $partecipant->guest->name }}</a>
                    @endisset
                </td>
                <td>
                    <a href="{{ route('partecipates.edit', $partecipant->id) }}">
                        <i class="material-icons">edit</i>
                    </a>
                </td>
                <td>
                    <a href="#partecipate-delete-modal" onclick="delete_partecipate({{ $partecipant->id }})" class="waves-effect waves-light modal-trigger">
                        <i class="material-icons red-text">delete</i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @include('layouts.fab-admin-create-users')

    @if(Route::has('partecipates.create'))
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href="{{ route('partecipates.create') }}">
                <i class="large material-icons">add</i>
            </a>
        </div>
    @endif

    <div id="partecipate-delete-modal" class="modal">
        <form id="partecipate-delete-form" action="" method="post">
            <div class="modal-content">
                @csrf
                @method('DELETE')
                <h4>Conferma</h4>
                <p>Sei sicuro di voler cancellare questo ospite?</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="btn btn-flat modal-close red">No</a>
                <button type="submit" name="" class="btn btn-flat green" onclick="partecipate_delete_submit()">Si</button>
            </div>
        </form>
    </div>

    <script>
        function delete_partecipate(id)
        {
            let url = '{{ route("partecipates.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#partecipate-delete-form").attr('action', url);
        }

        function partecipate_delete_submit()
        {
            $("#partecipate-delete-form").submit();
        }
    </script>

@endsection
