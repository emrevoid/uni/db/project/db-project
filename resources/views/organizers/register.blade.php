@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col s12 m4 l3"></div>
        <div class="col s12 m4 l6">
            <h1 class="center-align">{{ __('Registra la tua società') }}</h1>
            <div class="card">
                <form method="POST" action="{{ route('organizers.store') }}">
                    @csrf
                    <div class="card-content">
                        @if ($errors->any())
                            <div class="card-panel red lighten-1">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">person</i>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="#name" autofocus>
                                <label for="ragSociale">{{ __('Nome') }}</label>
                                @error('name')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">person</i>
                                <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ old('surname') }}" required autocomplete="#surname" autofocus>
                                <label for="surname">{{ __('Cognome') }}</label>
                                @error('surname')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">assignment</i>
                                <input id="ragSociale" type="text" class="form-control @error('ragSociale') is-invalid @enderror" name="ragSociale" value="{{ old('ragSociale') }}" required autocomplete="#ragSociale" autofocus>
                                <label for="ragSociale">{{ __('Ragione Sociale') }}</label>
                                @error('ragSociale')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">local_phone</i>
                                <input id="numTelefono" type="text" class="form-control @error('numTelefono') is-invalid @enderror" name="numTelefono" value="{{ old('numTelefono') }}" required autocomplete="#numTelefono" autofocus>
                                <label for="numTelefono">{{ __('Num Telefono') }}</label>
                                @error('numTelefono')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">description</i>
                                <input id="descrizione" type="text" class="form-control @error('descrizione') is-invalid @enderror" name="descrizione" value="{{ old('descrizione') }}" required autocomplete="#descrizione" autofocus>
                                <label for="descrizione">{{ __('Descrizione') }}</label>
                                @error('descrizione')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">email</i>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                <label for="email">{{ __('Indirizzo E-Mail') }}</label>
                                @error('email')
                                <span class="helper-text" data-error="{{ $message }}" data-success=""></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">lock</i>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                <label for="password">{{ __('Password') }}</label>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">lock</i>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                <label for="password-confirm">{{ __('Conferma password') }}</label>
                            </div>
                        </div>

                        <div class="card-action center-align">
                            <button type="submit" class="btn-flat">
                                {{ __('Registrati') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col s12 m4 l3"></div>
    </div>
@endsection
