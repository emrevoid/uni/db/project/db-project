@extends('layouts.app')

@section('content')
@canany(['isAdmin', 'isCustomer'], auth()->user())
    <h4>Lista degli Organizzatori</h4>
    <table class="striped responsive-table">
        <thead>
        <tr>
            <th>Nome</th>
            <th>Cognome</th>
            <th>Email</th>
            <th>Ragione Sociale</th>
            <th>Numero di telefono</th>
            <th>Descrizione</th>
            <th></th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->surname }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->userable->ragSociale }}</td>
                <td>{{ $user->userable->numTelefono }}</td>
                <td>{{ $user->userable->descrizione }}</td>
                <td>
                    <a href="{{ route('organizers.edit', $user->id) }}">
                        <i class="material-icons">edit</i>
                    </a>
                </td>
                <td>
                    <a href="#organizer-delete-modal" onclick="delete_seat({{ $user->id }})" class="waves-effect waves-light modal-trigger">
                        <i class="material-icons red-text">delete</i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@include('layouts.fab-admin-create-users')

<div id="organizer-delete-modal" class="modal">
    <form id="organizer-delete-form" action="" method="post">
        <div class="modal-content">
            @csrf
            @method('DELETE')
            <h4>Conferma</h4>
            <p>Sei sicuro di voler cancellare l'organizzatore?</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="btn btn-flat modal-close red">No</a>
            <button type="submit" name="" class="btn btn-flat green" onclick="organizer_delete_submit()">Si</button>
        </div>
    </form>
</div>

<script>
    function delete_organizer(id)
    {
        let url = '{{ route("organizers.destroy", ":id") }}';
            url = url.replace(':id', id);
        $("#organizer-delete-form").attr('action', url);
    }

    function organizer_delete_submit()
    {
        $("#organizer-delete-form").submit();
    }
</script>

@endcanany
@endsection
