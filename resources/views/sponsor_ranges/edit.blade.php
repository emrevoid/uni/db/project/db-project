@extends('layouts.app')

@section('content')
    @can('isAdmin', auth()->user())
        <div class="row">
            <div class="col s12 m4 l4"></div>
            <div class="col s12 m4 l4">
                <div class="card">
                    <form method="POST" action="{{ route('sponsor_ranges.update', $sponsorRange) }}">
                        @csrf
                        @method('PATCH')
                        <div class="card-content">
                            @if ($errors->any())
                                <div class="card-panel red lighten-1">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div><br />
                            @endif
                            <span class="card-title">Modifica fascia sponsor: {{ $sponsorRange->name }}</span>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input placeholder="{{ $sponsorRange->name }}" id="sponsorrange_name" name="name" type="text" value="{{ $sponsorRange->name }}" class="validate">
                                    <label for="sponsorrange_name">Nome</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <div class="input-field col s12">
                                        <input id="sponsorrange_amount" name="amount" type="number" min="0.00" max="1.00" step="0.01" class="validate" value="{{ $sponsorRange->amount }}">
                                        <label for="sponsorrange_amount">Percentuale di contributo (decimale)</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-action">
                            <button class="btn-flat"><a href="{{route('sponsor_ranges.index')}}">Indietro</a></button>
                            <button type="submit" class="btn-flat">Aggiorna fascia sponsor</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col s12 m4 l4"></div>
        </div>
    @endcan
@endsection

