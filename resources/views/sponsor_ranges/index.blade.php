@extends('layouts.app')

@section('content')
    @can('isAdmin', auth()->user())
        <ul class="collection with-header">
            <li class="collection-header"><h4>Fasce Sponsor</h4></li>
            @foreach($sponsor_ranges as $sponsor_range)
                <li class="collection-item">
                    <div>
                        {{ $sponsor_range->name }} ({{ $sponsor_range->amount * 100 }}% contributo)

                        <a href="#sponsorrange-delete-modal" onclick="delete_sponsorrange({{ $sponsor_range->id }})" class="waves-effect waves-light modal-trigger secondary-content">
                            <i class="material-icons red-text">delete</i>
                        </a>
                        <a href="{{ route('sponsor_ranges.edit', $sponsor_range->id) }}" class="secondary-content">
                            <i class="material-icons">edit</i>
                        </a>
                    </div>
                </li>
            @endforeach
        </ul>

        @if(Route::has('sponsor_ranges.create'))
            <div class="fixed-action-btn">
                <a class="btn-floating btn-large red" href="{{ route('sponsor_ranges.create') }}">
                    <i class="large material-icons">add</i>
                </a>
            </div>
        @endif

        <div id="sponsorrange-delete-modal" class="modal">
            <form id="sponsorrange-delete-form" action="" method="post">
                <div class="modal-content">
                    @csrf
                    @method('DELETE')
                    <h4>Conferma</h4>
                    <p>Sei sicuro di voler cancellare la fascia sponsor?</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="btn btn-flat modal-close red">No</a>
                    <button type="submit" name="" class="btn btn-flat green" onclick="sponsorrange_delete_submit()">Si</button>
                </div>
            </form>
        </div>

        <script>
            function delete_sponsorrange(sponsorrange)
            {
                let url = '{{ route("sponsor_ranges.destroy", ":id") }}';
                url = url.replace(':id', sponsorrange);
                $("#sponsorrange-delete-form").attr('action', url);
            }

            function sponsorrange_delete_submit()
            {
                $("#sponsorrange-delete-form").submit();
            }
        </script>
    @endcan
@endsection
