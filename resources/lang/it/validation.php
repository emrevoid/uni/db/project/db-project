<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Il campo :attribute deve essere accettato.',
    'active_url' => 'Il campo :attribute non è un URL valido.',
    'after' => 'Il campo :attribute deve essere una data successiva a :date.',
    'after_or_equal' => 'Il campo :attribute deve essere una data successiva o uguale a :date.',
    'alpha' => 'Il campo :attribute può contenere soltanto lettere.',
    'alpha_dash' => 'Il campo :attribute può contenere soltanto lettere, numeri, trattini e trattini bassi.',
    'alpha_num' => 'Il campo :attribute può contenere soltanto lettere e numeri.',
    'array' => 'Il campo :attribute deve essere un array.',
    'before' => 'Il campo :attribute deve essere una data precedente a :date.',
    'before_or_equal' => 'Il campo :attribute deve essere una data precedente o uguale a :date.',
    'between' => [
        'numeric' => 'Il campo :attribute deve avere un valore compreso fra :min e :max.',
        'file' => 'Il campo :attribute deve avere dimensioni comprese fra :min e :max kilobytes.',
        'string' => 'Il campo :attribute deve avere lunghezza compresa fra :min e :max caratteri.',
        'array' => 'Il campo :attribute deve avere lunghezza compresa fra :min e :max elementi.',
    ],
    'boolean' => 'Il campo :attribute deve essere vero o falso.',
    'confirmed' => 'Il campo :attribute non combacia con la conferma.',
    'date' => 'Il campo :attribute non è una data valida.',
    'date_equals' => 'Il campo :attribute deve essere una data uguale a :date.',
    'date_format' => 'Il campo :attribute non rispetta il formato :format.',
    'different' => 'I campi :attribute e :other devono essere diversi.',
    'digits' => 'Il campo :attribute deve essere lungo :digits cifre.',
    'digits_between' => 'Il campo :attribute deve avere una lunghezza compresa fra :min e :max cifre.',
    'dimensions' => 'Il campo :attribute ha dimensioni non valide.',
    'distinct' => 'Il campo :attribute ha valori duplicati.',
    'email' => 'Il campo :attribute deve essere un indirizzo email corretto.',
    'exists' => 'Il campo selezionato :attribute è invalido.',
    'file' => 'Il campo :attribute deve essere un file.',
    'filled' => 'Il campo :attribute non può essere vuoto.',
    'gt' => [
        'numeric' => 'Il campo :attribute deve avere un valore maggiore di :value.',
        'file' => 'Il campo :attribute deve avere dimensioni maggiori di :value kilobytes.',
        'string' => 'Il campo :attribute deve avere una lunghezza maggiore di :value caratteri.',
        'array' => 'Il campo :attribute deve avere più di :value elementi.',
    ],
    'gte' => [
        'numeric' => 'Il campo :attribute deve essere maggiore o uguale a :value.',
        'file' => 'Il campo :attribute deve avere dimensioni maggiori o uguali a :value kilobytes.',
        'string' => 'Il campo :attribute deve avere una lunghezza maggiore o uguale a :value caratteri.',
        'array' => 'Il campo :attribute deve avere :value elementi o più.',
    ],
    'image' => 'Il campo :attribute deve essere un file di tipo immagine.',
    'in' => 'Il valore selezionato nel campo :attribute è invalido.',
    'in_array' => 'Il contenuto del campo :attribute non esiste in :other.',
    'integer' => 'Il campo :attribute deve essere un intero.',
    'ip' => 'Il campo :attribute deve essere un indirizzo IP valido.',
    'ipv4' => 'Il campo :attribute deve essere un indirizzo IPv4 valido.',
    'ipv6' => 'Il campo :attribute deve essere un indirizzo IPv6 valido.',
    'json' => 'Il campo :attribute deve essere una stringa JSON valida.',
    'lt' => [
        'numeric' => 'Il campo :attribute deve essere minore di :value.',
        'file' => 'Il campo :attribute deve avere dimensioni minori di :value kilobytes.',
        'string' => 'Il campo :attribute deve avere una lunghezza minore di :value caratteri.',
        'array' => 'Il campo :attribute deve avere meno di :value elementi.',
    ],
    'lte' => [
        'numeric' => 'Il campo :attribute deve essere minore o uguale a :value.',
        'file' => 'Il campo :attribute deve avere dimensioni minori o uguali a :value kilobytes.',
        'string' => 'Il campo :attribute deve avere una lunghezza minore o uguale a :value caratteri.',
        'array' => 'Il campo :attribute deve avere :value elementi o meno.',
    ],
    'max' => [
        'numeric' => 'Il campo :attribute deve avere un valore non maggiore di :max.',
        'file' => 'Il campo :attribute deve avere dimensioni non maggiori di :max kilobytes.',
        'string' => 'Il campo :attribute deve avere lunghezza non maggiore di :max caratteri.',
        'array' => 'Il campo :attribute non deve avere più di :max elementi.',
    ],
    'mimes' => 'Il campo :attribute deve essere un file di tipo: :values.',
    'mimetypes' => 'Il campo :attribute deve essere un file di tipo: :values.',
    'min' => [
        'numeric' => 'Il campo :attribute deve avere un valore non minore di :max.',
        'file' => 'Il campo :attribute deve avere dimensioni non minori di :max kilobytes.',
        'string' => 'Il campo :attribute deve avere lunghezza non minore di :max caratteri.',
        'array' => 'Il campo :attribute non deve avere meno di :max elementi.',
    ],
    'not_in' => 'Il valore selezionato nel campo :attribute è invalido.',
    'not_regex' => 'Il formato campo :attribute è invalido.',
    'numeric' => 'Il campo :attribute deve essere un numero.',
    'present' => 'Il campo :attribute deve essere presente.',
    'regex' => 'Il formato del campo :attribute è invalido.',
    'required' => 'Il campo :attribute è obbligatorio.',
    'required_if' => 'Il campo :attribute è obbligatorio se :other è uguale a :value.',
    'required_unless' => 'Il campo :attribute è obbligatorio a meno che :other sia in :values.',
    'required_with' => 'Il campo :attribute è obbligatorio se :values è presente.',
    'required_with_all' => 'Il campo :attribute è obbligatorio se :values sono presenti.',
    'required_without' => 'Il campo :attribute è obbligatorio se :values non sono presenti.',
    'required_without_all' => 'Il campo :attribute è obbligatorio se nessuno di :values sono presenti.',
    'same' => 'Il campo :attribute e :other devono essere uguali.',
    'size' => [
        'numeric' => 'Il campo :attribute deve avere un valore uguale a :size.',
        'file' => 'Il campo :attribute deve avere dimensioni uguali a :size kilobytes.',
        'string' => 'Il campo :attribute deve avere lunghezza uguale a :size caratteri.',
        'array' => 'Il campo :attribute deve contenere :size elementi.',
    ],
    'starts_with' => 'Il campo :attribute deve iniziare con uno dei seguenti valori: :values',
    'string' => 'Il campo :attribute deve essere una stringa.',
    'timezone' => 'Il campo :attribute deve essere una timezone valida.',
    'unique' => 'Il campo :attribute è già stato utilizzato.',
    'uploaded' => 'Il campo :attribute ha fallito il caricamento.',
    'url' => 'Il formato del campo :attribute è invalido.',
    'uuid' => 'Il campo :attribute deve essere un UUID valido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'name' => 'Nome',
        'description' => 'Descrizione',
        'start_date' => 'Data d\'inizio',
        'end_date' => 'Data di fine',
        'start_time' => 'Orario d\'inizio',
        'end_time' => 'Orario di fine',
        'start_datetime' => 'Data e ora d\'inizio',
        'end_datetime' => 'Data e ora di fine',
        'place' => 'Luogo',
        'guest' => 'Ospiti',
        'image' => 'Immagine',
        'category_id' => 'Categoria',
        'price' => 'Prezzo',
        'n_ticket' => 'Numero dei biglietti disponibili',
        'n_ticket_remaining' => 'Numero dei biglietti rimanenti'
    ],

];
