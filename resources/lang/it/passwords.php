<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least eight characters and match the confirmation.',
    'reset' => 'La tua password è stata resettata!',
    'sent' => 'Ti abbiamo mandato per e-mail il link per resettare la tua password!',
    'token' => 'Il token di reset della password non è valido.',
    'user' => 'Non esiste un utente con la mail inserita.',

];
